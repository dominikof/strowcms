<?php

/**
 * This is the model class for table "Langs".
 *
 * The followings are the available columns in table 'Langs':
 * @property integer $id
 * @property string $short
 * @property boolean $back
 * @property boolean $front
 * @property boolean $back_def
 * @property boolean $front_def
 * @property string $img
 * @property string $name
 * @property integer $order
 */
class Langs extends CActiveRecord
{

    public function scopes()
    {
        return array(
            'front'=>array(
                'condition'=>'"front"=true',
            ),
            'back'=>array(
                'condition'=>'"back"=true',
            ),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Langs the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'Langs';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('order', 'numerical', 'integerOnly' => true),
            array('short', 'length', 'max' => 5),
            array('short', 'required'),
            array('short', 'unique'),
            array('img', 'length', 'max' => 50),
            array('name', 'length', 'max' => 20),
            array('name', 'required'),
            array('back, front, back_def, front_def', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, short, back, front, back_def, front_def, img, name, order', 'safe', 'on' => 'search'),
        );
    }

    public function frontActiveLangs(){//'front=true',
        return $this->findAll(array('condition'=>'front=:param','order'=>'"order"','params'=>array(':param'=>'true')));
//        return $this->findAll('front=true',array('order'=>'"order"'));
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'short' => Yii::t("langs", "Language short"),
            'back' => Yii::t("langs", "Use in backend"),
            'front' => Yii::t("langs", "Use in frontend"),
            'back_def' => Yii::t("langs", "Use in backend by default"),
            'front_def' => Yii::t("langs", "Use in frontend by default"),
            'img' => 'Img',
            'name' => 'Name',
            'order' => 'Order',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('short', $this->short, true);
        $criteria->compare('back', $this->back);
        $criteria->compare('front', $this->front);
        $criteria->compare('back_def', $this->back_def);
        $criteria->compare('front_def', $this->front_def);
        $criteria->compare('img', $this->img, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('order', $this->order);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }


}