<?php

/**
 * This is the model class for table "UrlManager".
 *
 * The followings are the available columns in table 'UrlManager':
 * @property integer $id
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property integer $level
 * @property integer $module_id
 * @property integer $position_id
 * @property string $translit
 * @property string $item_name
 * @property integer $lang_id
 * @property boolean $category
 *
 * The followings are the available model relations:
 * @property Modules $module
 * @property Langs $lang
 */
class UrlManager extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UrlManager the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        return array(
            'nestedSetBehavior'=>array(
                'class'=>'common.extensions.behaviors.NestedSetBehavior',
                'leftAttribute'=>'lft',
                'rightAttribute'=>'rgt',
                'levelAttribute'=>'level',
                'rootAttribute'=>'root',
                'hasManyRoots'=>true,
            ),
        );
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'UrlManager';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that 
        // will receive user inputs. 
        return array(
            array('root, lft, rgt, level, module_id, position_id, lang_id', 'numerical', 'integerOnly'=>true),
            array('translit, item_name', 'length', 'max'=>100),
            array('category', 'safe'),
            // The following rule is used by search(). 
            // Please remove those attributes that should not be searched. 
            array('id, root, lft, rgt, level, module_id, position_id, translit, item_name, lang_id, category', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related 
        // class name for the relations automatically generated below. 
        return array(
            'module' => array(self::BELONGS_TO, 'Modules', 'module_id'),
            'lang' => array(self::BELONGS_TO, 'Langs', 'lang_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'root' => 'Root',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'level' => 'Level',
            'module_id' => 'Module',
            'position_id' => 'Position',
            'translit' => 'Translit',
            'item_name' => 'Item Name',
            'lang_id' => 'Lang',
            'category' => 'Category',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that 
        // should not be searched. 

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('root',$this->root);
        $criteria->compare('lft',$this->lft);
        $criteria->compare('rgt',$this->rgt);
        $criteria->compare('level',$this->level);
        $criteria->compare('module_id',$this->module_id);
        $criteria->compare('position_id',$this->position_id);
        $criteria->compare('translit',$this->translit,true);
        $criteria->compare('item_name',$this->item_name,true);
        $criteria->compare('lang_id',$this->lang_id);
        $criteria->compare('category',$this->category);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}