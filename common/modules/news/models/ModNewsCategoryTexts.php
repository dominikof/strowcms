<?php

/**
 * This is the model class for table "ModNewsCategoryTexts".
 *
 * The followings are the available columns in table 'ModNewsCategoryTexts':
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $mtitle
 * @property string $mdescr
 * @property integer $lang_id
 *
 * The followings are the available model relations:
 * @property ModNewsCategory $category
 * @property Langs $lang
 */
class ModNewsCategoryTexts extends MultiActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ModNewsCategoryTexts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ModNewsCategoryTexts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('name', 'required'),
			array('category_id, lang_id', 'numerical', 'integerOnly'=>true),
			array('name, mtitle', 'length', 'max'=>255),
			array('mdescr,id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, category_id, name, mtitle, mdescr, lang_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, 'ModNewsCategory', 'category_id'),
			'lang' => array(self::BELONGS_TO, 'Langs', 'lang_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Category',
			'name' => Yii::t('modNews','Name of the category'),
			'mtitle' => 'Mtitle',
			'mdescr' => 'Mdescr',
			'lang_id' => 'Lang',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('mtitle',$this->mtitle,true);
		$criteria->compare('mdescr',$this->mdescr,true);
		$criteria->compare('lang_id',$this->lang_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}