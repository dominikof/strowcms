<?php

/**
 * This is the model class for table "ModNews".
 *
 * The followings are the available columns in table 'ModNews':
 * @property integer $id
 * @property string $start_date
 * @property string $end_date
 * @property boolean $visible
 * @property integer $order
 * @property integer $category_id
 * @property string $update_time
 * @property string $create_time
 *
 * The followings are the available model relations:
 * @property ModNewsCategory $category
 * @property ModNewsTexts[] $modNewsTexts
 */
class ModNews extends CActiveRecord
{


	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ModNews the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave(){
        if ($this->isNewRecord)
            $this->create_time = new CDbExpression('NOW()');

        $this->update_time = new CDbExpression('NOW()');

        return parent::beforeSave();
    }

    public function behaviors(){
        return array(
            'DateBehavior'=>array(
                'class'=>'common.extensions.behaviors.DateBehavior',
                'dateAttribute'=>array('start_date','end_date'),
                'modelObj'=>$this,
            ),
        );
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ModNews';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
        Yii::import('common.extensions.validators.DateCompare');
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, start_date, end_date', 'required'),
            array('start_date','DateCompare','compareFld'=>'end_date'),
			array('order, category_id', 'numerical', 'integerOnly'=>true),
			array('visible, update_time, create_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, start_date, end_date, visible, order, category_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, 'ModNewsCategory', 'category_id'),
			'texts' => array(self::HAS_ONE, 'ModNewsTexts', 'news_id'),
            'url' => array(self::HAS_ONE, 'UrlManager', 'position_id'),
            'images' => array(self::HAS_MANY, 'ModUploaderImg', 'position_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'start_date' =>Yii::t('modNews','Start Date'),
			'end_date' => Yii::t('modNews','End Date'),
			'visible' => 'Visible',
			'order' => 'Order',
			'category_id' => 'Category',
            'update_time' => 'Update Time',
            'create_time' => 'Create Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('visible',$this->visible);
		$criteria->compare('order',$this->order);
		$criteria->compare('category_id',$this->category_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}