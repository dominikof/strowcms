<?php

/**
 * This is the model class for table "ModNewsTexts".
 *
 * The followings are the available columns in table 'ModNewsTexts':
 * @property integer $id
 * @property integer $news_id
 * @property string $name
 * @property string $short
 * @property string $full
 * @property integer $lang_id
 * @property string $mtitle
 * @property string $mdescr
 *
 * The followings are the available model relations:
 * @property ModNews $news
 * @property Langs $lang
 */
class ModNewsTexts extends MultiActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ModNewsTexts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ModNewsTexts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('name', 'required'),
			array('news_id, lang_id', 'numerical', 'integerOnly'=>true),
			array('name, mtitle', 'length', 'max'=>255),
			array('short, full, mdescr', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, news_id, name, short, full, lang_id, mtitle, mdescr', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'news' => array(self::BELONGS_TO, 'ModNews', 'news_id'),
			'lang' => array(self::BELONGS_TO, 'Langs', 'lang_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'news_id' => 'News',
			'name' => Yii::t('modNews','News title'),
			'short' => 'Short',
			'full' => 'Full',
			'lang_id' => 'Lang',
			'mtitle' => 'Mtitle',
			'mdescr' => 'Mdescr',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('news_id',$this->news_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('short',$this->short,true);
		$criteria->compare('full',$this->full,true);
		$criteria->compare('lang_id',$this->lang_id);
		$criteria->compare('mtitle',$this->mtitle,true);
		$criteria->compare('mdescr',$this->mdescr,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}