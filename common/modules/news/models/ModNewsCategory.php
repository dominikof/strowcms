<?php

/**
 * This is the model class for table "ModNewsCategory".
 *
 * The followings are the available columns in table 'ModNewsCategory':
 * @property integer $id
 * @property boolean $visible
 * @property integer $order
 *
 * The followings are the available model relations:
 * @property ModNews[] $modNews
 * @property ModNewsCategoryTexts[] $modNewsCategoryTexts
 */
class ModNewsCategory extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ModNewsCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ModNewsCategory';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order', 'numerical', 'integerOnly'=>true),
			array('visible,id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, visible, order', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'news' => array(self::HAS_MANY, 'ModNews', 'category_id'),
			'categoryTexts' => array(self::HAS_ONE, 'ModNewsCategoryTexts', 'category_id'),
            'url' => array(self::HAS_ONE, 'UrlManager', 'position_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'visible' => 'Visible',
			'order' => 'Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('visible',$this->visible);
		$criteria->compare('order',$this->order);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}