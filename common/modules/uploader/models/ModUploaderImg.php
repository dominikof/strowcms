<?php

/**
 * This is the model class for table "ModUploaderImg".
 *
 * The followings are the available columns in table 'ModUploaderImg':
 * @property integer $id
 * @property integer $module_id
 * @property integer $position_id
 * @property string $file_name
 * @property boolean $visible
 * @property integer $order
 * @property string $source
 */
class ModUploaderImg extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ModUploaderImg the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'ModUploaderImg';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that 
        // will receive user inputs. 
        return array(
            array('module_id, position_id, order', 'numerical', 'integerOnly'=>true),
            array('file_name', 'length', 'max'=>135),
            array('source', 'length', 'max'=>255),
            array('visible', 'safe'),
            // The following rule is used by search(). 
            // Please remove those attributes that should not be searched. 
            array('id, module_id, position_id, file_name, visible, order, source', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related 
        // class name for the relations automatically generated below. 
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'module_id' => 'Module',
            'position_id' => 'Position',
            'file_name' => 'File Name',
            'visible' => 'Visible',
            'order' => 'Order',
            'source' => 'Source',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that 
        // should not be searched. 

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('module_id',$this->module_id);
        $criteria->compare('position_id',$this->position_id);
        $criteria->compare('file_name',$this->file_name,true);
        $criteria->compare('visible',$this->visible);
        $criteria->compare('order',$this->order);
        $criteria->compare('source',$this->source,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}