<?php
/**
 * User: Panarin S.S.
 * Date: 08.01.13
 * Time: 22:57
 */
class DateCompare extends CValidator
{

    public $compareFld;

    protected function validateAttribute($object,$attribute){
        $start_date=DateTime::createFromFormat(Yii::app()->params['dateTimeFormat'],$object->$attribute);
        $end_date=DateTime::createFromFormat(Yii::app()->params['dateTimeFormat'],$object->{$this->compareFld});
        if($end_date instanceof DateTime)
        $diff=$start_date->diff($end_date);
        if(isset($diff) && $diff->invert!=0) {
            $message=$this->message!==null?$this->message:Yii::t('main','{attribute} must be greater ' . $diff->format('Y-m-d H:i:s'));
            $this->addError($object,$attribute,$message);
        }
        else if(!isset($diff))
        {
            $message=$this->message!==null?$this->message:Yii::t('main','{attribute} bad format');
            $this->addError($object,$attribute,$message);
        }
        return;
    }
}
