<?php
/**
 * User: Panarin S.S.
 * Date: 10.12.12
 * Time: 16:03
 * Cms component for working with url data (save,add,remove, erc.)
 */
class MultiLanguageUrlManager
{


    /**
     * generate and save url in the pages hierarchy
     * @param $module_id
     * @param $position_id
     * @param $multiTextsArr
     * @param $urlMakeField
     * @param $parent_id
     */
    public function saveUrl($module_id, $position_id, $multiTextsArr, $urlMakeField, $parent_id, $category = false, $parent_category = false)
    {

        $UrlManager = new UrlManager();

        $langsArr = Langs::model()->front()->findAll();
        foreach ($langsArr as $lang) {
            $rootModule = $UrlManager->roots()->find('"module_id"=:module_id AND "lang_id"=:lang_id',
                array(':module_id' => $module_id, ':lang_id' => $lang->id));
            if (empty($rootModule->id)) {
                $rootModule = $this->createRoot($module_id, $lang->id);
            }
            $urlModel = $UrlManager->find('"module_id"=:module_id AND "position_id"=:position_id AND "lang_id"=:lang_id AND "category"=:category',
                array(':module_id' => $module_id, ':position_id' => $position_id, ':lang_id' => $lang->id, ":category" => MYCHtml::strbool($category)));
            if ($parent_id == 0) {
                if (empty($urlModel->id)) {
                    if (!$this->createUrlNode($module_id, $position_id, $rootModule, $multiTextsArr, $urlMakeField, $lang->id, $category)) return false;
                } else {
                    $parent = $urlModel->parent()->find('"module_id"=:module_id AND "lang_id"=:lang_id',
                        array(':module_id' => $module_id, ':lang_id' => $lang->id));
                    $moved=false;
                    if (!$parent->isRoot()) {
                        $moved=true;
                        if (!$urlModel->moveAsLast($rootModule)) return false;
                    }
                    if (!$this->updateUrlNode($urlModel, $multiTextsArr, $urlMakeField, $rootModule, $module_id, $position_id, $lang->id,$moved)) return false;
                }
            } else {
                $urlModelParent = $UrlManager->find('"module_id"=:module_id AND "position_id"=:position_id AND "lang_id"=:lang_id AND "category"=:category',
                    array(':module_id' => $module_id, ':position_id' => $parent_id, ':lang_id' => $lang->id, ":category" => MYCHtml::strbool($parent_category)));
                /**Тут нужна перегенерациия иерархии урлов для страници и языка который добавился потом но нету урлов для родительских страниц. Сейчас же мы считаем что все родительские урлы созданы*/
                if (empty($urlModelParent->id)) continue;
                if (empty($urlModel->id)) {
                    if (!$this->createUrlNode($module_id, $position_id, $urlModelParent, $multiTextsArr, $urlMakeField, $lang->id, $category)) return false;
                } else {
                    $parent = $urlModel->parent()->find('"module_id"=:module_id AND "lang_id"=:lang_id AND "category"=:category',
                        array(':module_id' => $module_id, ':lang_id' => $lang->id, ":category" => MYCHtml::strbool($parent_category)));
                    $moved=false;
                    if ($parent->position_id != $parent_id) {
                        $moved=true;
                        if (!$urlModel->moveAsLast($urlModelParent)) return false;
                    }
                    if (!$this->updateUrlNode($urlModel, $multiTextsArr, $urlMakeField, $urlModelParent, $module_id, $position_id, $lang->id,$moved)) return false;
                }
            }
        }

        return true;
    }

    public function getUrlMultiLangRecord($module_id, $position_id, $category = false, &$textsModel)
    {
        $UrlManager = new UrlManager();
        $langsArr = Langs::model()->front()->findAll();
        foreach ($langsArr as $lang) {
            $textsModel->translit[$lang->id] = '';
        }
        if($position_id!==NULL){
            $urlModel = $UrlManager->findAll('"module_id"=:module_id AND "position_id"=:position_id AND "category"=:category',
                array(':module_id' => $module_id, ':position_id' => $position_id, ":category" => MYCHtml::strbool($category)));
            foreach ($urlModel as $row) {
                $textsModel->translit[$row->lang_id] = $row->translit;
            }
        }



    }

    public function deleteUrlNode($module_id, $position_id, $category = false)
    {
        if (is_array($position_id) AND count($position_id) > 0) {
            $urlModel = new UrlManager;
            $positions_id_str = implode(',', $position_id);
            $urlModel->deleteAll('"module_id"=:module_id AND "category"=:category AND "position_id" IN (' . $positions_id_str . ')',
                array(':module_id' => $module_id, ':category' => MYCHtml::strbool($category)));
        } elseif (!is_array($position_id) AND !empty($position_id)) {
            $urlModel = new UrlManager;
            $urlModel->deleteAll('"module_id"=:module_id AND "category"=:category AND "position_id"=:position_id',
                array(':module_id' => $module_id, ':position_id' => $position_id, ':category' => MYCHtml::strbool($category)));
        }

    }

    /**
     * Create new url node
     * @param $module_id
     * @param $position_id
     * @param $urlModelParent
     * @param $multiTextsArr
     * @param $urlMakeField
     * @param $lang_id
     * @return bool
     */
    private function createUrlNode($module_id, $position_id, $urlModelParent, $multiTextsArr, $urlMakeField, $lang_id, $category)
    {
        $translits = Yii::app()->getParams('translits');
        $urlModelNew = new UrlManager;
        $urlModelNew->module_id = $module_id;
        $urlModelNew->position_id = $position_id;
        $urlModelNew->lang_id = $lang_id;
        $urlModelNew->category = $category;
        if (is_array($translits) AND $translits[$lang_id]['changeUrl'] AND !empty($translits[$lang_id]['translit']))
            $translit = $translits[$lang_id]['translit'];
        else
            $translit = $this->makeTranslit($multiTextsArr[$lang_id][$urlMakeField]);
        $urlModelNew->translit = $this->checkInUnikTranslit($module_id, $lang_id, $translit, $position_id, $urlModelParent, $category);
        $urlModelNew->item_name = $multiTextsArr[$lang_id][$urlMakeField];
        if (!$urlModelNew->prependTo($urlModelParent)) return false;
        return true;
    }

    private function updateUrlNode($urlModel, $multiTextsArr, $urlMakeField, $urlModelParent, $module_id, $position_id, $lang_id,$moved)
    {
        $translits = Yii::app()->request->getParam('translits');
        if (is_array($translits)) {
            if ($translits[$lang_id]['changeUrl'] AND !empty($translits[$lang_id]['translit']))
                $translit = $translits[$lang_id]['translit'];
            elseif (isset($translits[$lang_id]['regenerate']))
                $translit = $this->makeTranslit($multiTextsArr[$lang_id][$urlMakeField]);

        }
        if($moved)
            $translit = $urlModel->translit;
        if(!empty($translit))
            $urlModel->translit = $this->checkInUnikTranslit($module_id, $lang_id, $translit, $position_id, $urlModelParent);

        $urlModel->item_name = $multiTextsArr[$lang_id][$urlMakeField];
        if (!$urlModel->saveNode()) return false;
        return true;
    }

    /**
     * Create root node for current module and lang
     */
    private function createRoot($module_id, $lang_id)
    {
        $rootModule = new UrlManager;
        $rootModule->module_id = $module_id;
        $rootModule->lang_id = $lang_id;
        $rootModule->saveNode();
        return $rootModule;
    }

    /**
     * factory to create class SUrlManager
     * @return MultiLanguageUrlManager
     */
    public static function factory()
    {
        return new MultiLanguageUrlManager();
    }

    /**
     * Check if generated transit are unick in level which it must be saved
     * @param $module_id
     * @param $lang_id
     * @param $translit
     * @param $position_id
     * @param $parent_node
     * @return mixed
     */
    protected function checkInUnikTranslit($module_id, $lang_id, $translit, $position_id, $parent_node, $category = false)
    {
        $childrens = $parent_node->children()->findAll('"module_id"=:module_id AND "position_id"!=:position_id AND "translit"=:translit AND "lang_id"=:lang_id AND "category"=:category',
            array(':module_id' => $module_id, ':lang_id' => $lang_id, ':translit' => $translit, ':position_id' => $position_id, ':category' => MYCHtml::strbool($category)));
        if (count($childrens) > 0) {
            $translit .= '-' . $position_id;
            return $this->checkInUnikTranslit($module_id, $lang_id, $translit, $position_id, $parent_node, $category);
        } else {
            return $translit;
        }
    }

    /**
     * Translit text from cyrillic to latin letters.
     * @static
     * @param string $text the text being translit.
     * @return string
     */
    protected function makeTranslit($text, $toLowCase = TRUE)
    {
        $matrix = array(
            "й" => "i", "ц" => "c", "у" => "u", "к" => "k", "е" => "e", "н" => "n",
            "г" => "g", "ш" => "sh", "щ" => "shch", "з" => "z", "х" => "h", "ъ" => "",
            "ф" => "f", "ы" => "y", "в" => "v", "а" => "a", "п" => "p", "р" => "r",
            "о" => "o", "л" => "l", "д" => "d", "ж" => "zh", "э" => "e", "ё" => "e",
            "я" => "ya", "ч" => "ch", "с" => "s", "м" => "m", "и" => "i", "т" => "t",
            "ь" => "", "б" => "b", "ю" => "yu",
            "Й" => "I", "Ц" => "C", "У" => "U", "К" => "K", "Е" => "E", "Н" => "N",
            "Г" => "G", "Ш" => "SH", "Щ" => "SHCH", "З" => "Z", "Х" => "X", "Ъ" => "",
            "Ф" => "F", "Ы" => "Y", "В" => "V", "А" => "A", "П" => "P", "Р" => "R",
            "О" => "O", "Л" => "L", "Д" => "D", "Ж" => "ZH", "Э" => "E", "Ё" => "E",
            "Я" => "YA", "Ч" => "CH", "С" => "S", "М" => "M", "И" => "I", "Т" => "T",
            "Ь" => "", "Б" => "B", "Ю" => "YU",
            "«" => "", "»" => "", " " => "-",

            "\"" => "", "\." => "", "–" => "-", "\," => "", "\(" => "", "\)" => "",
            "\?" => "", "\!" => "", "\:" => "",

            '#' => '', '№' => '', ' - ' => '-', '/' => '-', '  ' => '-',
        );

        // Enforce the maximum component length
        $maxlength = 100;
        $text = implode(array_slice(explode('<br>', wordwrap(trim(strip_tags(html_entity_decode($text))), $maxlength, '<br>', false)), 0, 1));
        //$text = substr(, 0, $maxlength);

        foreach ($matrix as $from => $to)
            $text = mb_eregi_replace($from, $to, $text);

// Optionally convert to lower case.
        if ($toLowCase) {
            $text = strtolower($text);
        }

        return $text;
    }

    public function getTranslitsForItem($module_id, $item_id)
    {
        $translits = UrlManager::model()->findAll('"module_id"=:module_id AND "position_id"=:position_id', array(':module_id' => $module_id, ':position_id' => $item_id));
        foreach ($translits as $row) {
            foreach ($this->metaData->columns as $attr_name => $column) {
//                if($column->type!='string') continue;
                $obj->{$attr_name}[$row['lang_id']] = $row->{$attr_name};
            }
        }
        $langs = Langs::model()->frontActiveLangs();
        foreach ($langs as $lang) {
            foreach ($this->metaData->columns as $attr_name => $column) {
                if (!isset($obj->{$attr_name}[$lang->id]))
                    $obj->{$attr_name}[$lang->id] = '';
            }
        }

        return $obj;
    }
}
