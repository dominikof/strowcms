<?php
/**
 * User: Panarin S.S.
 * Date: 23.02.13
 * Time: 11:05
 */
class DateBehavior extends CActiveRecordBehavior
{

    public $dateAttribute;
    public $modelObj;

    public function afterFind($event){
        parent::afterFind($event);
        foreach ($this->dateAttribute as $dateAttr) {
            $date=DateTime::createFromFormat(Yii::app()->params['dateTimeDatabaseFormat'],$this->modelObj->$dateAttr);
            if($date instanceof DateTime)
               $this->modelObj->$dateAttr=$date->format(Yii::app()->params['dateTimeFormat']);
        }
    }
}
