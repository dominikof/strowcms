<?php
/**
 * User: Panarin S.S
 * Date: 11.09.12
 * Time: 22:14
 */
class Simage
{
    private $home_path = '';
    private $valid_types = array("gif" => '', "GIF" => '', "jpg" => '', "JPG" => '', "png" => '', "PNG" => '', "jpeg" => '', "JPEG" => '');
    public $error = array();
    private $model;
    private $field;
    private $resize = 1024;

    function __construct($home_path = NULL,&$model,$fld='image',$resize = 1024)
    {
        $this->model=$model;
        $this->field=$fld;
        if (!empty($home_path))
            $this->home_path = $_SERVER["DOCUMENT_ROOT"] . $home_path;
        else
            $this->home_path = $_SERVER["DOCUMENT_ROOT"] . '/media/images/uploads/';

    }

    public function saveImage()
    {
        if(isset($_FILES[$this->field])){
            $file_name=$this->uploadImage($_FILES[$this->field]);
            if($file_name==false) return false;
            $this->remove($this->home_path.$this->model->{$this->field});
            $this->model->{$this->field}=$file_name;
            $this->model->update();
        }
        return true;
    }

    private function createDirectory()
    {

    }

    private function getFileExt($filename)
    {
        $arr = explode(".", $filename);
        $count = count($arr);
        return $arr[$count - 1];
    }

    private function generateUnikFileName()
    {
        // Генерируем случайное название
        $symbols = '0123456789abcdefghijklmnopqrstuvwxyz';
        return md5(substr(str_shuffle($symbols), 0, 16) . microtime());
    }

    private function uploadImage($file_arr)
    {
        $ext = $this->getFileExt($file_arr['name']);
        if (!isset($this->valid_types[$ext])) {
            $this->error[] = Yii::t("main", "Error adding pictures. This file type is not allowed to upload.");
            return false;
        }

        $filename = $this->generateUnikFileName();
        // Изменение размера и загрузка изображения
        $im = Yii::app()->image->load($file_arr['tmp_name']);

        if ($im->width > $this->resize) {
            $im->resize($this->resize,$this->resize);
        }
        if (!$im->save("$this->home_path$filename.$ext"))
            $this->error[] = Yii::t("main", "Error saving image");

        return "$filename.$ext";
    }

    public function uploadImages($file_arr, $resize = 1024)
    {

    }

    public function setResize($resize){
        $this->resize=$resize;
        return $this;
    }

    static function factory($path_to_save = NULL,&$model,$fld='image',$resize = 1024){
        return new Simage($path_to_save,$model,$fld,$resize);
    }

    static function remove($path){
        $pathRoot=$_SERVER["DOCUMENT_ROOT"].$path;
        if(file_exists($path) AND is_file($path))
            unlink($path);
        elseif(file_exists($pathRoot) AND is_file($pathRoot))
            unlink($pathRoot);
    }
}
