<?php
/**
 * User: Panarin S.S.
 * Date: 05.12.12
 * Time: 8:44
 *
 * Doing resize image using image exstansion.
 * Using this when you need resize image and save it once with other name. In second just checking if is name
 */
class  ImageResizer
{

    private static function getFileNameByMaster($master, $width, $height)
    {
        $masterName = '';
        if ($master == 4)
            $masterName = 'resize_width_' . $width . '_';
        elseif ($master == 3)
            $masterName = 'resize_height_' . $height . '_';
        elseif ($master == 1)
            $masterName = 'resize_exact_' . $width . 'x' . $height . '_';
        else
            $masterName = 'resize_auto_' . $width . 'x' . $height . '_';

        return $masterName;
    }

    /**
     * @param $path - path to the image (with DOCUMENT_ROOT)
     * @param $width - width of the image for resize
     * @param $height - height of the image for resize
     * @param null $master - of: Image::NONE=1, Image::AUTO=2, Image::WIDTH=4, Image::HEIGHT=3 (witch parameter of image use for resizing)
     */
    public static function resize($path, $width, $height, $master = NULL)
    {
        $document_root=$_SERVER['DOCUMENT_ROOT'];
        $masterName=ImageResizer::getFileNameByMaster($master,$width, $height);
        $resized_filename = pathinfo($path, PATHINFO_DIRNAME) .'/'.$masterName . pathinfo($path, PATHINFO_BASENAME);

        if(file_exists($document_root.$path) AND !file_exists($document_root.$resized_filename)){
            $fullPath=$document_root.$path;
            $Image= Yii::app()->image->load($fullPath);
            $Image->resize($width, $height, $master)->save($document_root.$resized_filename);
        }

        return $resized_filename;
    }
}
