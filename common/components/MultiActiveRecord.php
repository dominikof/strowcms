<?php
/**
 * User: Panarin S.S.
 * Date: 18.11.12
 * Time: 20:34
 */
class MultiActiveRecord extends CActiveRecord
{
    public $attributesM;
    public $translit;

    public function multiSave($id,$foreignKey){
        $langs=Langs::model()->frontActiveLangs();
        foreach ($langs as $lang) {
//            $this->unsetAttributes();
            $tmp=$this->attributesM[$lang->id];
            $tmp[$foreignKey]=$id;
            $tmp['lang_id']=$lang->id;
            $obj=$this->find('"'.$foreignKey.'"=:item_id AND "lang_id"=:lang_id',array(":item_id"=>$id,":lang_id"=>$lang->id));

            $className=get_class($this);
            if(empty($obj->id)) $obj = new $className();

            $obj->attributes=$tmp;
            if(!$obj->validate() OR !$obj->save()){
                $this->addErrors($obj->errors);
                return false;
            }
        }

        return true;
    }
//
//    private function setErrors($obj){
//        foreach ($obj->errors as $error) {
//            $this->setErrors()
//        }
//
//    }

    public function getDataForEdit($id,$foreignKey){
        $modelTexts = $this->findAll('"'.$foreignKey.'"=:foreignKey',array(':foreignKey'=>$id));
        foreach ($modelTexts as $row) {
            foreach ($this->metaData->columns as $attr_name=>$column) {
//                if($column->type!='string') continue;
                $obj->{$attr_name}[$row['lang_id']]=$row->{$attr_name};
            }
        }
        $langs=Langs::model()->frontActiveLangs();
        foreach ($langs as $lang) {
            foreach ($this->metaData->columns as $attr_name=>$column) {
                if(!isset( $obj->{$attr_name}[$lang->id]))
                $obj->{$attr_name}[$lang->id]='';
            }
        }

        return $obj;

    }
}
