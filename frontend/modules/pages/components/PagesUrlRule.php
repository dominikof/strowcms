<?php
/**
 * User: Panarin S.S.
 * Date: 24.02.13
 * Time: 15:45
 */
class PagesUrlRule extends CBaseUrlRule
{

    public function createUrl($manager, $route, $params, $ampersand)
    {
        $pathsMap = Yii::app()->getModule('pages')->getPathsMap();

        if ($route === 'pages/page/index' && isset($params['id'], $pathsMap[$params['id']]))
            return $pathsMap[$params['id']] . $manager->urlSuffix;
        else
            return false;
    }

    public function parseUrl($manager, $request, $pathInfo, $rawPathInfo)
    {
        $pathsMap = Yii::app()->getModule('pages')->getPathsMap();
        $id = array_search($pathInfo, $pathsMap);

        if ($id === false)
            return false;

        $_GET['id'] = $id;
        return 'pages/page/index';
    }
}
