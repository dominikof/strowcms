<?php

class PageController extends Controller
{
    protected $perPage;
    protected $moduleId;


    protected function beforeAction($action){
        $moduleObj=Yii::app()->getModule('pages');
        $this->moduleId=$moduleObj->moduleId;
        return parent::beforeAction($action);
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
//            array('allow', // allow admin user to perform 'admin' and 'delete' actions
//                'actions' => array('*'),
//                'users' => array('root'),
//            ),
//            array('deny', // deny all users
//                'users' => array('*'),
//            ),
        );
    }


    /**
     * Lists all models.
     */
    public function actionIndex($id)
    {
        $ModPagesFront=new ModPagesFront();
        $data=$ModPagesFront->getPage($id);
        if ($data === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        $this->render('index',array('data'=>$data));
    }


}
