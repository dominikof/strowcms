<?php

class PagesModule extends CWebModule
{

    public $defaultController="Page";
    public $moduleId;
    public $mainPageId=163;
    public $cacheId = 'pagesPathsMap';

    public $cacheClearArr=array();

	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'pages.models.*',
			'pages.components.*',
			'common.modules.pages.models.*',
            'common.modules.uploader.models.*',
            'application.modules.uploader.models.*',
		));



        //        if (Yii::app()->cache->get($this->cacheId) === false)


        if(empty($this->moduleId))
            $this->moduleId=Modules::model()->getModuleIdByName('pages');

        $this->cacheId = 'pagesPathsMap_'.lang_id;

        if (Yii::app()->cache->get($this->cacheId) === false)
            $this->updatePathsMap();
        //$this->cacheClearArr=include '/common/modules/pages/CacheIds.php';

	}


    /**
     * Возвращает карту путей из кеша.
     * @return mixed
     */
    public function getPathsMap()
    {
        $pathsMap = Yii::app()->cache->get($this->cacheId);
        return $pathsMap === false ? $this->generatePathsMap() : $pathsMap;
    }

    /**
     * Сохраняет в кеш актуальную на момент вызова карту путей.
     * @return void
     */
    public function updatePathsMap()
    {
        $dependesy=new CDbCacheDependency('SELECT MAX("update_time") FROM "'.ModPages::$tableName.'"');
        Yii::app()->cache->set($this->cacheId, $this->generatePathsMap(),3600,$dependesy);
    }

    /**
     * Генерация карты страниц.
     * @return array ID узла => путь до узла
     */
    public function generatePathsMap()
    {
        $nodes=UrlManager::model()->findAll(
            array(
                'condition'=>
                '"module_id"='.$this->moduleId.' AND "lang_id"='.lang_id.'',
                'order'=>'"t"."lft"'
            ),
            array(':module_id'=>$this->moduleId,':lang_id'=>lang_id)
        );
        $pathsMap = array();
        $depths = array();
        foreach ($nodes as $node)
        {
            if ($node['level'] > 2){
                $path = $depths[$node['level'] - 1];
            }else
                $path = '';
            if(empty($node['translit'])) continue;
            $path .= $node['translit'];
            $depths[$node['level']] = $path . '/';
            $pathsMap[$node['position_id']] = $path;
        }
        return $pathsMap;
    }
}
