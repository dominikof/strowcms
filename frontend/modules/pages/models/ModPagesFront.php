<?php
/**
 * User: Panarin S.S.
 * Date: 28.02.13
 * Time: 21:39
 */
class ModPagesFront extends ModPages
{
    /**
     * get page date by id
     * @param $id
     * @return CActiveRecord
     */
    public function getPage($id)
    {
        $data = $this->model()->with(array(
            'texts' => array(
                'joinType' => 'LEFT JOIN',
                'on' => '"texts"."lang_id"=' . lang_id . '',
            ),
        ))->find(array('condition' => '"t"."id"=:id AND "t"."publish"=:publish','params'=>array(':id'=>$id,':publish'=>'true')));

        return $data;
    }

}
