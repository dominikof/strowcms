<?php

class AppStartConfigureBehavior extends CBehavior
{

	/**
	 * @return array the behavior events.
	 */
	public function events()
	{
        $arr=array_merge(parent::events(), array(
            'onBeginRequest'=>'setDefLanguage',
        ));
        return $arr;
	}

	public function init()
	{
//		if (!isset($this->defaultLanguage))
//			$this->defaultLanguage = $this->owner->sourceLanguage;
	}

    public function setDefLanguage(){
        $this->generateLangsParams();

        $route=Yii::app()->getRequest()->getPathInfo();
        $langsArr=Yii::app()->params['langs'];

        $lang_=NULL;
        $lang=NULL;
        $lang_id=NULL;
        if(strlen($route)>2){
            if($route[2]=='/')
                $lang_=$route[0].$route[1];
        }elseif(strlen($route)==2){
            $lang_=$route;
        }
        foreach ($langsArr['front_langs'] as $f_lan) {
            if($langsArr['data'][$f_lan]['short']==$lang_){
                $lang=$lang_;
                $lang_id=$f_lan;
                break;
            }
        }
        if(empty($lang)){
            $lang=$langsArr['data'][$langsArr['front_def']]['short'];
            $lang_id=$langsArr['front_def'];
        }

        Yii::app()->setLanguage($lang);

        if(!defined('lang_id')) define('lang_id',$lang_id);
        if(!defined('lang_short')) define('lang_short',$lang);
        if(!defined('lang_url')){
            if($lang!=$langsArr['data'][$langsArr['front_def']]['short']){
                define('lang_url',$lang.'/');
            }else  define('lang_url','/');
        }
    }

    /**
     * Get language data and generate array 'langs' in params array
     * using:
     * Yii::app()->params['langs']['data'] - array of languages data;
     * Yii::app()->params['langs']['front']- id of front language
     * Yii::app()->params['langs']['back'] - id of back language
     * Yii::app()->params['langs']['back_def'] - id of back default language
     * Yii::app()->params['langs']['front_def'] - id of front default language
     * Yii::app()->params['langs']['front_langs'] -  array of id's front languages
     */
    private function generateLangsParams(){
        $langs=Langs::model()->findAll();
        $langsArr=array();
        $langsArr['front_langs']=array();
        foreach ($langs as $lang) {
            $langsArr['data'][$lang['id']]=$lang;
            if($lang->back===true) $langsArr['back']= $lang->id;
            if($lang->front===true){
                $langsArr['front']= $lang->id;
                $langsArr['front_langs'][]=$lang->id;
            }
            if($lang->back_def===true) $langsArr['back_def']= $lang->id;
            if($lang->front_def===true) $langsArr['front_def']= $lang->id;
        }

        Yii::app()->params['langs']=$langsArr;
    }

}
