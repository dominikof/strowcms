--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.1
-- Dumped by pg_dump version 9.2.0
-- Started on 2013-03-16 22:39:32

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 201 (class 3079 OID 11727)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2166 (class 0 OID 0)
-- Dependencies: 201
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 170 (class 1259 OID 16385)
-- Name: AuthAssignment; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "AuthAssignment" (
    itemname character varying(64) NOT NULL,
    userid character varying(64) NOT NULL,
    bizrule text,
    data text
);


ALTER TABLE public."AuthAssignment" OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 16391)
-- Name: AuthItem; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "AuthItem" (
    name character varying(64) NOT NULL,
    type integer NOT NULL,
    description text,
    bizrule text,
    data text
);


ALTER TABLE public."AuthItem" OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 16397)
-- Name: AuthItemChild; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "AuthItemChild" (
    parent character varying(64) NOT NULL,
    child character varying(64) NOT NULL
);


ALTER TABLE public."AuthItemChild" OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 16400)
-- Name: auto_id_langs; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auto_id_langs
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auto_id_langs OWNER TO postgres;

--
-- TOC entry 2167 (class 0 OID 0)
-- Dependencies: 173
-- Name: auto_id_langs; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auto_id_langs', 8, true);


--
-- TOC entry 174 (class 1259 OID 16402)
-- Name: Langs; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "Langs" (
    id integer DEFAULT nextval('auto_id_langs'::regclass) NOT NULL,
    short character varying(5),
    back boolean,
    front boolean,
    back_def boolean,
    front_def boolean,
    img character varying(50),
    name character varying(20),
    "order" integer
);


ALTER TABLE public."Langs" OWNER TO postgres;

--
-- TOC entry 2168 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN "Langs".id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "Langs".id IS 'id языка';


--
-- TOC entry 2169 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN "Langs".short; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "Langs".short IS 'Сокращение языка';


--
-- TOC entry 2170 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN "Langs".back; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "Langs".back IS 'Использовать язык на админ части';


--
-- TOC entry 2171 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN "Langs".front; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "Langs".front IS 'Использовать язык на сайте';


--
-- TOC entry 2172 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN "Langs".back_def; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "Langs".back_def IS 'Язык админ части по умолчанию';


--
-- TOC entry 2173 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN "Langs".front_def; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "Langs".front_def IS 'Язык сайта по умолчанию';


--
-- TOC entry 2174 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN "Langs".img; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "Langs".img IS 'Изображение';


--
-- TOC entry 197 (class 1259 OID 73794)
-- Name: auto_id_news; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auto_id_news
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auto_id_news OWNER TO postgres;

--
-- TOC entry 2175 (class 0 OID 0)
-- Dependencies: 197
-- Name: auto_id_news; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auto_id_news', 20, true);


--
-- TOC entry 191 (class 1259 OID 57410)
-- Name: ModNews; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "ModNews" (
    id integer DEFAULT nextval('auto_id_news'::regclass) NOT NULL,
    start_date timestamp(6) without time zone,
    end_date timestamp(6) without time zone,
    visible boolean DEFAULT true,
    "order" integer,
    category_id integer NOT NULL,
    update_time timestamp without time zone,
    create_time timestamp without time zone
);
ALTER TABLE ONLY "ModNews" ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY "ModNews" ALTER COLUMN start_date SET STATISTICS 0;
ALTER TABLE ONLY "ModNews" ALTER COLUMN end_date SET STATISTICS 0;
ALTER TABLE ONLY "ModNews" ALTER COLUMN visible SET STATISTICS 0;
ALTER TABLE ONLY "ModNews" ALTER COLUMN create_time SET STATISTICS 100;


ALTER TABLE public."ModNews" OWNER TO postgres;

--
-- TOC entry 2176 (class 0 OID 0)
-- Dependencies: 191
-- Name: TABLE "ModNews"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "ModNews" IS 'Таблица новости у которой айди - это айди новости во всех зависимых таблицах';


--
-- TOC entry 2177 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN "ModNews".start_date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModNews".start_date IS 'Начальная дата новости';


--
-- TOC entry 2178 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN "ModNews".end_date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModNews".end_date IS 'Конечная дата новости';


--
-- TOC entry 2179 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN "ModNews"."order"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModNews"."order" IS 'Порядок';


--
-- TOC entry 195 (class 1259 OID 65602)
-- Name: auto_id_news_categories; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auto_id_news_categories
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auto_id_news_categories OWNER TO postgres;

--
-- TOC entry 2180 (class 0 OID 0)
-- Dependencies: 195
-- Name: auto_id_news_categories; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auto_id_news_categories', 22, true);


--
-- TOC entry 193 (class 1259 OID 57438)
-- Name: ModNewsCategory; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "ModNewsCategory" (
    id integer DEFAULT nextval('auto_id_news_categories'::regclass) NOT NULL,
    visible boolean,
    "order" integer
);
ALTER TABLE ONLY "ModNewsCategory" ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY "ModNewsCategory" ALTER COLUMN visible SET STATISTICS 0;
ALTER TABLE ONLY "ModNewsCategory" ALTER COLUMN "order" SET STATISTICS 0;


ALTER TABLE public."ModNewsCategory" OWNER TO postgres;

--
-- TOC entry 2181 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN "ModNewsCategory".id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModNewsCategory".id IS 'id - Категории новости';


--
-- TOC entry 196 (class 1259 OID 65606)
-- Name: auto_id_news_categories_texts; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auto_id_news_categories_texts
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auto_id_news_categories_texts OWNER TO postgres;

--
-- TOC entry 2182 (class 0 OID 0)
-- Dependencies: 196
-- Name: auto_id_news_categories_texts; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auto_id_news_categories_texts', 17, true);


--
-- TOC entry 194 (class 1259 OID 57443)
-- Name: ModNewsCategoryTexts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "ModNewsCategoryTexts" (
    id integer DEFAULT nextval('auto_id_news_categories_texts'::regclass) NOT NULL,
    category_id integer,
    name character varying(255),
    mtitle character varying(255),
    mdescr text,
    lang_id integer
);
ALTER TABLE ONLY "ModNewsCategoryTexts" ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY "ModNewsCategoryTexts" ALTER COLUMN category_id SET STATISTICS 0;
ALTER TABLE ONLY "ModNewsCategoryTexts" ALTER COLUMN name SET STATISTICS 0;
ALTER TABLE ONLY "ModNewsCategoryTexts" ALTER COLUMN mtitle SET STATISTICS 0;
ALTER TABLE ONLY "ModNewsCategoryTexts" ALTER COLUMN mdescr SET STATISTICS 0;
ALTER TABLE ONLY "ModNewsCategoryTexts" ALTER COLUMN lang_id SET STATISTICS 0;


ALTER TABLE public."ModNewsCategoryTexts" OWNER TO postgres;

--
-- TOC entry 2183 (class 0 OID 0)
-- Dependencies: 194
-- Name: COLUMN "ModNewsCategoryTexts".category_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModNewsCategoryTexts".category_id IS 'id - категории';


--
-- TOC entry 2184 (class 0 OID 0)
-- Dependencies: 194
-- Name: COLUMN "ModNewsCategoryTexts".name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModNewsCategoryTexts".name IS 'имя категории';


--
-- TOC entry 2185 (class 0 OID 0)
-- Dependencies: 194
-- Name: COLUMN "ModNewsCategoryTexts".mtitle; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModNewsCategoryTexts".mtitle IS 'мета тайтл для категории новостей';


--
-- TOC entry 2186 (class 0 OID 0)
-- Dependencies: 194
-- Name: COLUMN "ModNewsCategoryTexts".mdescr; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModNewsCategoryTexts".mdescr IS 'мета дескрипшен для категории новостей';


--
-- TOC entry 2187 (class 0 OID 0)
-- Dependencies: 194
-- Name: COLUMN "ModNewsCategoryTexts".lang_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModNewsCategoryTexts".lang_id IS 'id - языка';


--
-- TOC entry 198 (class 1259 OID 73796)
-- Name: auto_id_news_texts; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auto_id_news_texts
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auto_id_news_texts OWNER TO postgres;

--
-- TOC entry 2188 (class 0 OID 0)
-- Dependencies: 198
-- Name: auto_id_news_texts; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auto_id_news_texts', 13, true);


--
-- TOC entry 192 (class 1259 OID 57416)
-- Name: ModNewsTexts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "ModNewsTexts" (
    id integer DEFAULT nextval('auto_id_news_texts'::regclass) NOT NULL,
    news_id integer,
    name character varying(255),
    short text,
    "full" text,
    lang_id integer,
    mtitle character varying(255),
    mdescr text
);
ALTER TABLE ONLY "ModNewsTexts" ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY "ModNewsTexts" ALTER COLUMN news_id SET STATISTICS 0;
ALTER TABLE ONLY "ModNewsTexts" ALTER COLUMN name SET STATISTICS 0;
ALTER TABLE ONLY "ModNewsTexts" ALTER COLUMN short SET STATISTICS 0;
ALTER TABLE ONLY "ModNewsTexts" ALTER COLUMN "full" SET STATISTICS 0;
ALTER TABLE ONLY "ModNewsTexts" ALTER COLUMN lang_id SET STATISTICS 0;


ALTER TABLE public."ModNewsTexts" OWNER TO postgres;

--
-- TOC entry 2189 (class 0 OID 0)
-- Dependencies: 192
-- Name: TABLE "ModNewsTexts"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "ModNewsTexts" IS 'Мультиязычные тексты для новости';


--
-- TOC entry 2190 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN "ModNewsTexts".news_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModNewsTexts".news_id IS 'id новости';


--
-- TOC entry 2191 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN "ModNewsTexts".name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModNewsTexts".name IS 'Заголовок новости';


--
-- TOC entry 2192 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN "ModNewsTexts".short; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModNewsTexts".short IS 'Короткое описание новости';


--
-- TOC entry 2193 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN "ModNewsTexts"."full"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModNewsTexts"."full" IS 'Полное описание новости';


--
-- TOC entry 2194 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN "ModNewsTexts".lang_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModNewsTexts".lang_id IS 'id языка';


--
-- TOC entry 2195 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN "ModNewsTexts".mtitle; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModNewsTexts".mtitle IS 'тайтл для новости';


--
-- TOC entry 2196 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN "ModNewsTexts".mdescr; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModNewsTexts".mdescr IS 'мета описание для новости';


--
-- TOC entry 175 (class 1259 OID 16406)
-- Name: auto_id_pages; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auto_id_pages
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auto_id_pages OWNER TO postgres;

--
-- TOC entry 2197 (class 0 OID 0)
-- Dependencies: 175
-- Name: auto_id_pages; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auto_id_pages', 202, true);


--
-- TOC entry 176 (class 1259 OID 16408)
-- Name: ModPages; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "ModPages" (
    id integer DEFAULT nextval('auto_id_pages'::regclass) NOT NULL,
    visible boolean DEFAULT false,
    "order" integer,
    publish boolean DEFAULT false,
    parent_id integer NOT NULL,
    update_time timestamp without time zone,
    create_time timestamp without time zone
);
ALTER TABLE ONLY "ModPages" ALTER COLUMN update_time SET STATISTICS 100;
ALTER TABLE ONLY "ModPages" ALTER COLUMN create_time SET STATISTICS 100;


ALTER TABLE public."ModPages" OWNER TO postgres;

--
-- TOC entry 2198 (class 0 OID 0)
-- Dependencies: 176
-- Name: TABLE "ModPages"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "ModPages" IS 'Статические страницы';


--
-- TOC entry 183 (class 1259 OID 24576)
-- Name: auto_id_modpagestxt; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auto_id_modpagestxt
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auto_id_modpagestxt OWNER TO postgres;

--
-- TOC entry 2199 (class 0 OID 0)
-- Dependencies: 183
-- Name: auto_id_modpagestxt; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auto_id_modpagestxt', 167, true);


--
-- TOC entry 177 (class 1259 OID 16412)
-- Name: ModPagesTexts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "ModPagesTexts" (
    page_id integer NOT NULL,
    name character varying(255) NOT NULL,
    content text,
    mtitle character varying(255),
    mdescr character varying(255),
    lang_id integer,
    id integer DEFAULT nextval('auto_id_modpagestxt'::regclass) NOT NULL
);


ALTER TABLE public."ModPagesTexts" OWNER TO postgres;

--
-- TOC entry 2200 (class 0 OID 0)
-- Dependencies: 177
-- Name: TABLE "ModPagesTexts"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "ModPagesTexts" IS 'Тексты к статическим страницам';


--
-- TOC entry 187 (class 1259 OID 49165)
-- Name: auto_id_uploaderimg; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auto_id_uploaderimg
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auto_id_uploaderimg OWNER TO postgres;

--
-- TOC entry 2201 (class 0 OID 0)
-- Dependencies: 187
-- Name: auto_id_uploaderimg; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auto_id_uploaderimg', 153, true);


--
-- TOC entry 184 (class 1259 OID 32777)
-- Name: ModUploaderImg; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "ModUploaderImg" (
    id integer DEFAULT nextval('auto_id_uploaderimg'::regclass) NOT NULL,
    module_id integer,
    position_id integer,
    file_name character varying(135),
    visible boolean DEFAULT true,
    "order" integer,
    source character varying(255),
    form_id integer DEFAULT 1
);


ALTER TABLE public."ModUploaderImg" OWNER TO postgres;

--
-- TOC entry 2202 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN "ModUploaderImg".module_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModUploaderImg".module_id IS 'id - модуля которому принадлежит итем';


--
-- TOC entry 2203 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN "ModUploaderImg".position_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModUploaderImg".position_id IS 'id - позиции на которою установлено изображение';


--
-- TOC entry 2204 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN "ModUploaderImg".file_name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModUploaderImg".file_name IS 'имя файла изображения';


--
-- TOC entry 2205 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN "ModUploaderImg".visible; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModUploaderImg".visible IS 'видимость';


--
-- TOC entry 2206 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN "ModUploaderImg"."order"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModUploaderImg"."order" IS 'порядок';


--
-- TOC entry 2207 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN "ModUploaderImg".source; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "ModUploaderImg".source IS 'Путь к картинке в файловой системе';


--
-- TOC entry 186 (class 1259 OID 40976)
-- Name: auto_id_modules; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auto_id_modules
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auto_id_modules OWNER TO postgres;

--
-- TOC entry 2208 (class 0 OID 0)
-- Dependencies: 186
-- Name: auto_id_modules; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auto_id_modules', 3, true);


--
-- TOC entry 185 (class 1259 OID 40969)
-- Name: Modules; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "Modules" (
    id integer DEFAULT nextval('auto_id_modules'::regclass) NOT NULL,
    name character varying(30) NOT NULL,
    descr character varying(255)
);


ALTER TABLE public."Modules" OWNER TO postgres;

--
-- TOC entry 2209 (class 0 OID 0)
-- Dependencies: 185
-- Name: TABLE "Modules"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "Modules" IS 'Модули цмс';


--
-- TOC entry 189 (class 1259 OID 57394)
-- Name: auto_id_urlmanager; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auto_id_urlmanager
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auto_id_urlmanager OWNER TO postgres;

--
-- TOC entry 2210 (class 0 OID 0)
-- Dependencies: 189
-- Name: auto_id_urlmanager; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auto_id_urlmanager', 112, true);


--
-- TOC entry 188 (class 1259 OID 57370)
-- Name: UrlManager; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "UrlManager" (
    id integer DEFAULT nextval('auto_id_urlmanager'::regclass) NOT NULL,
    root integer,
    lft integer,
    rgt integer,
    level integer,
    module_id integer,
    position_id integer,
    translit character varying(100),
    item_name character varying(100),
    lang_id integer,
    category boolean DEFAULT false
);
ALTER TABLE ONLY "UrlManager" ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY "UrlManager" ALTER COLUMN root SET STATISTICS 0;
ALTER TABLE ONLY "UrlManager" ALTER COLUMN lft SET STATISTICS 0;
ALTER TABLE ONLY "UrlManager" ALTER COLUMN rgt SET STATISTICS 0;
ALTER TABLE ONLY "UrlManager" ALTER COLUMN level SET STATISTICS 0;
ALTER TABLE ONLY "UrlManager" ALTER COLUMN module_id SET STATISTICS 0;
ALTER TABLE ONLY "UrlManager" ALTER COLUMN position_id SET STATISTICS 0;
ALTER TABLE ONLY "UrlManager" ALTER COLUMN translit SET STATISTICS 0;
ALTER TABLE ONLY "UrlManager" ALTER COLUMN item_name SET STATISTICS 0;


ALTER TABLE public."UrlManager" OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 16424)
-- Name: auto_id_users; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auto_id_users
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auto_id_users OWNER TO postgres;

--
-- TOC entry 2211 (class 0 OID 0)
-- Dependencies: 179
-- Name: auto_id_users; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auto_id_users', 19, true);


--
-- TOC entry 180 (class 1259 OID 16426)
-- Name: Users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "Users" (
    id integer DEFAULT nextval('auto_id_users'::regclass) NOT NULL,
    username character varying(45) DEFAULT NULL::character varying,
    password character varying(255) DEFAULT NULL::character varying,
    salt character varying(255) DEFAULT NULL::character varying,
    password_strategy character varying(50) DEFAULT NULL::character varying,
    requires_new_password smallint[],
    email character varying(255) DEFAULT NULL::character varying,
    login_attempts integer,
    login_time integer,
    login_ip character varying(30) DEFAULT NULL::character varying,
    validation_key character varying(255) DEFAULT NULL::character varying,
    create_id integer,
    create_time integer,
    update_id integer,
    update_time integer,
    register_date timestamp without time zone DEFAULT now()
);


ALTER TABLE public."Users" OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 90194)
-- Name: UsersData; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "UsersData" (
    user_id integer NOT NULL,
    name character varying(50),
    second_name character varying(50),
    about text
);


ALTER TABLE public."UsersData" OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 16418)
-- Name: auto_id_groups; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auto_id_groups
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auto_id_groups OWNER TO postgres;

--
-- TOC entry 2212 (class 0 OID 0)
-- Dependencies: 178
-- Name: auto_id_groups; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auto_id_groups', 8, true);


--
-- TOC entry 190 (class 1259 OID 57397)
-- Name: auto_id_sourcemessage; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auto_id_sourcemessage
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auto_id_sourcemessage OWNER TO postgres;

--
-- TOC entry 2213 (class 0 OID 0)
-- Dependencies: 190
-- Name: auto_id_sourcemessage; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auto_id_sourcemessage', 74, true);


--
-- TOC entry 200 (class 1259 OID 106596)
-- Name: cache; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cache (
    id character(128) NOT NULL,
    expire integer,
    value bytea
);


ALTER TABLE public.cache OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 16440)
-- Name: message; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE message (
    id integer NOT NULL,
    language character varying(16) NOT NULL,
    translation text,
    lang_id integer
);


ALTER TABLE public.message OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 16446)
-- Name: sourcemessage; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sourcemessage (
    id integer DEFAULT nextval('auto_id_sourcemessage'::regclass) NOT NULL,
    category character varying(32),
    message text
);


ALTER TABLE public.sourcemessage OWNER TO postgres;

--
-- TOC entry 2141 (class 0 OID 16385)
-- Dependencies: 170
-- Data for Name: AuthAssignment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "AuthAssignment" (itemname, userid, bizrule, data) FROM stdin;
root	4	\N	N;
admin	4	\N	N;
\.


--
-- TOC entry 2142 (class 0 OID 16391)
-- Dependencies: 171
-- Data for Name: AuthItem; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "AuthItem" (name, type, description, bizrule, data) FROM stdin;
admin	2	Login to admin panel	\N	N;
root	2	Super administrator	\N	N;
user	2	Just logged user	\N	N;
\.


--
-- TOC entry 2143 (class 0 OID 16397)
-- Dependencies: 172
-- Data for Name: AuthItemChild; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "AuthItemChild" (parent, child) FROM stdin;
\.


--
-- TOC entry 2144 (class 0 OID 16402)
-- Dependencies: 174
-- Data for Name: Langs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "Langs" (id, short, back, front, back_def, front_def, img, name, "order") FROM stdin;
2	ru	t	t	t	t	9863183a489fc769bc282de5cc7d6d4d.png	Rus	0
3	en	\N	\N	\N	\N	4b16455e77d5cc047f357706c826d19f.png	Eng	1
4	ua	\N	\N	\N	\N	6291cd046b3274ab7cd7972a9f33f5c9.png	Ukr	2
\.


--
-- TOC entry 2153 (class 0 OID 57410)
-- Dependencies: 191
-- Data for Name: ModNews; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "ModNews" (id, start_date, end_date, visible, "order", category_id, update_time, create_time) FROM stdin;
4	2013-01-01 12:00:00	2013-01-31 12:00:00	t	0	15	\N	\N
19	2013-01-02 12:00:00	2013-01-31 12:00:00	t	3	15	\N	\N
20	2013-02-06 12:00:00	2013-02-28 12:00:00	t	4	15	\N	\N
17	2013-02-06 12:00:00	2013-02-28 12:00:00	t	2	15	\N	\N
15	2013-02-19 12:02:00	2013-02-28 12:02:00	f	1	15	2013-02-24 20:42:55.939	\N
\.


--
-- TOC entry 2155 (class 0 OID 57438)
-- Dependencies: 193
-- Data for Name: ModNewsCategory; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "ModNewsCategory" (id, visible, "order") FROM stdin;
15	f	1
\.


--
-- TOC entry 2156 (class 0 OID 57443)
-- Dependencies: 194
-- Data for Name: ModNewsCategoryTexts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "ModNewsCategoryTexts" (id, category_id, name, mtitle, mdescr, lang_id) FROM stdin;
11	15	Новости компании			2
\.


--
-- TOC entry 2154 (class 0 OID 57416)
-- Dependencies: 192
-- Data for Name: ModNewsTexts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "ModNewsTexts" (id, news_id, name, short, "full", lang_id, mtitle, mdescr) FROM stdin;
1	4	Новость 1	<p>іфаіваіва</p>	<p>ваппвпвап</p>	2		
12	19	asdasd			2	asda	dasd
13	20	sfsdfsdf			2		
11	17	sfsdf			2		
9	15	Новость 3			2		
\.


--
-- TOC entry 2145 (class 0 OID 16408)
-- Dependencies: 176
-- Data for Name: ModPages; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "ModPages" (id, visible, "order", publish, parent_id, update_time, create_time) FROM stdin;
192	f	8	f	0	\N	\N
190	f	9	t	0	2013-02-28 20:47:19.47	\N
163	f	0	t	0	2013-03-01 05:39:01.128	\N
197	f	1	f	0	2013-03-02 11:23:20.548	2013-03-02 11:23:20.548
201	f	2	f	0	2013-03-02 12:26:17.777	2013-03-02 12:26:17.777
196	f	3	f	0	2013-03-02 11:23:13.808	2013-03-02 11:23:13.808
193	f	4	f	0	\N	\N
198	f	5	f	0	2013-03-02 11:23:25.788	2013-03-02 11:23:25.788
200	f	6	f	0	2013-03-02 11:23:40.149	2013-03-02 11:23:40.149
194	f	7	t	0	\N	\N
195	f	8	f	0	2013-02-24 20:40:14.931	2013-02-24 20:40:14.931
199	f	9	f	0	2013-03-02 11:23:33.141	2013-03-02 11:23:33.141
181	f	1	t	163	2013-02-28 20:59:07.863	\N
202	f	12	f	0	2013-03-04 20:27:20.431	2013-03-04 20:27:20.431
\.


--
-- TOC entry 2146 (class 0 OID 16412)
-- Dependencies: 177
-- Data for Name: ModPagesTexts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "ModPagesTexts" (page_id, name, content, mtitle, mdescr, lang_id, id) FROM stdin;
190	asd		\N	\N	2	155
181	о нас		\N	\N	2	147
196	fd		\N	\N	2	161
197	Z		\N	\N	2	162
198	ew		\N	\N	2	163
199	tr		\N	\N	2	164
200	qw		\N	\N	2	165
201	sdf		\N	\N	2	166
202	cgv		\N	\N	2	167
192	qwe		\N	\N	2	157
193	zx		\N	\N	2	158
194	newpage		\N	\N	2	159
195	zxc		\N	\N	2	160
163	Главная	<p><em>Выше мы вызываем метод&nbsp;<code>find</code>&nbsp;через&nbsp;<code>Post::model()</code>. Запомните, что статический метод&nbsp;<code>model()</code>обязателен для каждого AR-класса. Этот метод возвращает экземпляр AR, используемый для доступа к методам уровня класса (что-то схожее со статическими методами класса) в контексте объекта.</em></p>\r\n<p>Если метод&nbsp;<code>find</code>&nbsp;находит строку, соответствующую условиям запроса, он возвращает экземпляр класса<code>Post</code>, свойства которого содержат значения соответствующих полей строки таблицы. Далее мы можем использовать загруженные значения аналогично обычным свойствам объектов, например,&nbsp;<code>echo $post-&gt;title;</code>.</p>\r\n<p>В случае если в базе нет данных, соответствующих условиям запроса, метод&nbsp;<code>find</code>&nbsp;вернет значение null.</p>\r\n<p>Параметры&nbsp;<code>$condition</code>&nbsp;и&nbsp;<code>$params</code>&nbsp;используются для уточнения запроса. В данном случае&nbsp;<code>$condition</code>может быть строкой, соответствующей оператору&nbsp;<code>WHERE</code>&nbsp;в SQL-выражении, а&nbsp;<code>$params</code>&nbsp;&mdash; массивом параметров, значения которых должны быть привязаны к маркерам, указанным в&nbsp;<code>$condition</code>. Например:</p>			2	129
\.


--
-- TOC entry 2150 (class 0 OID 32777)
-- Dependencies: 184
-- Data for Name: ModUploaderImg; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "ModUploaderImg" (id, module_id, position_id, file_name, visible, "order", source, form_id) FROM stdin;
150	1	163	06_11_2009_0660475001257506000_paperblue.jpg	t	4	/media/images/uploads/mod_pages/163/17c94380e40e1f9533baa49bd232c5d9.jpg	1
151	1	163	06_11_2009_0664114001257506000_paperblue.jpg	t	5	/media/images/uploads/mod_pages/163/6c3a45c607f5fcb5e5f83012e1373f8a.jpg	1
152	1	163	06_11_2009_0667740001257506000_paperblue.jpg	t	6	/media/images/uploads/mod_pages/163/ab03e63a4ab07b33416bba2665833c63.jpg	1
126	2	4	06_11_2009_0649347001257506000_paperblue.jpg	t	0	/media/images/uploads/mod_news/4/53b12d34a2950e8842a17fa3789eda5b.jpg	1
125	2	4	06_11_2009_0006003001257506001_paperblue.jpg	t	1	/media/images/uploads/mod_news/4/4bd3d5625e2d8ce834186d93a08f157c.jpg	1
127	2	4	06_11_2009_0655915001257506000_paperblue.jpg	t	2	/media/images/uploads/mod_news/4/3a67580be45299764128bb3dd2b40603.jpg	1
137	2	4	06_11_2009_0684847001257506000_paperblue.jpg	t	3	/media/images/uploads/mod_news/4/80b668dc88e10c9a5fbaee965fa4509f.jpg	1
153	2	19	iPadBackgroundTexture-grey.png	t	1	/media/images/uploads/mod_news/19/bfb6120cb70d47407d4bc254cc76ab78.png	1
135	2	15	06_11_2009_0660475001257506000_paperblue.jpg	t	1	/media/images/uploads/mod_news/15/643ec9ac53b1e568da1699eda3ea5149.jpg	1
\.


--
-- TOC entry 2151 (class 0 OID 40969)
-- Dependencies: 185
-- Data for Name: Modules; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "Modules" (id, name, descr) FROM stdin;
1	pages	Статические страници
2	news	Новости
3	users	Модуль пользователей
\.


--
-- TOC entry 2152 (class 0 OID 57370)
-- Dependencies: 188
-- Data for Name: UrlManager; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "UrlManager" (id, root, lft, rgt, level, module_id, position_id, translit, item_name, lang_id, category) FROM stdin;
50	50	1	62	1	2	\N	\N	\N	2	f
61	50	18	41	2	2	15	novosti-kompanii	Новости компании	2	t
67	50	39	40	3	2	4	novost-1	Новость 1	2	f
97	50	21	22	3	2	19	asdasd	asdasd	2	f
98	50	19	20	3	2	20	sfsdfsdf	sfsdfsdf	2	f
23	23	1	86	1	1	\N	\N	\N	2	f
102	23	22	23	2	1	192	qwe	qwe	2	f
103	23	20	21	2	1	193	zx	zx	2	f
104	23	18	19	2	1	194	newpage	newpage	2	f
105	23	16	17	2	1	195	zxc	zxc	2	f
100	23	26	27	2	1	190	asd	asd	2	f
87	23	73	74	3	1	181	o-nas	о нас	2	f
27	23	62	75	2	1	163	glavnaya	Главная	2	f
106	23	14	15	2	1	196	fd	fd	2	f
107	23	12	13	2	1	197	z	Z	2	f
108	23	10	11	2	1	198	ew	ew	2	f
109	23	8	9	2	1	199	tr	tr	2	f
110	23	6	7	2	1	200	qw	qw	2	f
111	23	4	5	2	1	201	sdf	sdf	2	f
112	23	2	3	2	1	202	cgv	cgv	2	f
78	50	5	6	3	2	12	sdfsd	sdfsd	2	f
63	50	14	15	2	2	17			2	t
77	50	7	8	3	2	11	asdsd	asdsd	2	f
94	50	23	24	3	2	17	sfsdf	sfsdf	2	f
81	50	27	28	3	2	15	novost-3	Новость 3	2	f
\.


--
-- TOC entry 2147 (class 0 OID 16426)
-- Dependencies: 180
-- Data for Name: Users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "Users" (id, username, password, salt, password_strategy, requires_new_password, email, login_attempts, login_time, login_ip, validation_key, create_id, create_time, update_id, update_time, register_date) FROM stdin;
4	root	$2a$14$qggPk4tCs/HhaYKUCvG8Y./cUbnuLlg/KtVhLlP6/42sOvXOGiMsq	$2a$14$qggPk4tCs/HhaYKUCvG8YA	bcrypt	\N	dominikof@gmail.com	39	\N	\N	6ac95298e6eadfb4b5285f3066f0bc56	\N	\N	\N	\N	\N
18	test	$2a$14$QZSu5VOCYgSNJ866kSqx1uVkbpVEY3iKLPKq19SDkL8AzasN8Jfsu	$2a$14$QZSu5VOCYgSNJ866kSqx1w	bcrypt	\N	test@test.com	\N	\N	\N	\N	\N	\N	\N	\N	\N
19	sre	$2a$14$o/zX5Ap8JjXzqmuFRxWbiOIHiK3IYruN/Yc1FKepXffURTXy18Pi2	$2a$14$o/zX5Ap8JjXzqmuFRxWbiQ	bcrypt	\N	ssd@sd.csd	\N	\N	\N	\N	\N	\N	\N	\N	2013-02-10 12:58:13.58
\.


--
-- TOC entry 2157 (class 0 OID 90194)
-- Dependencies: 199
-- Data for Name: UsersData; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "UsersData" (user_id, name, second_name, about) FROM stdin;
18			
19			
\.


--
-- TOC entry 2158 (class 0 OID 106596)
-- Dependencies: 200
-- Data for Name: cache; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cache (id, expire, value) FROM stdin;
bd8f91a6de8a1ed4d8de5e099ba69450                                                                                                	0	\\x613a323a7b693a303b613a323a7b693a303b613a393a7b693a303b4f3a383a224355726c52756c65223a31363a7b733a393a2275726c537566666978223b4e3b733a31333a226361736553656e736974697665223b4e3b733a31333a2264656661756c74506172616d73223b613a303a7b7d733a31303a226d6174636856616c7565223b4e3b733a343a2276657262223b4e3b733a31313a2270617273696e674f6e6c79223b623a303b733a353a22726f757465223b733a393a226d696e2f7365727665223b733a31303a227265666572656e636573223b613a303a7b7d733a31323a22726f7574655061747465726e223b4e3b733a373a227061747465726e223b733a33353a222f5e6d696e5c2f283f503c673e5c772b295c2f283f503c6c6d3e5c642b295c2f242f75223b733a383a2274656d706c617465223b733a31323a226d696e2f3c673e2f3c6c6d3e223b733a363a22706172616d73223b613a323a7b733a313a2267223b733a333a225c772b223b733a323a226c6d223b733a333a225c642b223b7d733a363a22617070656e64223b623a303b733a31313a22686173486f7374496e666f223b623a303b733a31343a220043436f6d706f6e656e74005f65223b4e3b733a31343a220043436f6d706f6e656e74005f6d223b4e3b7d693a313b4f3a383a224355726c52756c65223a31363a7b733a393a2275726c537566666978223b4e3b733a31333a226361736553656e736974697665223b4e3b733a31333a2264656661756c74506172616d73223b613a303a7b7d733a31303a226d6174636856616c7565223b4e3b733a343a2276657262223b4e3b733a31313a2270617273696e674f6e6c79223b623a303b733a353a22726f757465223b733a303a22223b733a31303a227265666572656e636573223b613a303a7b7d733a31323a22726f7574655061747465726e223b4e3b733a373a227061747465726e223b733a32383a222f5e283f503c6c616e67756167653e285c777b327d29295c2f242f75223b733a383a2274656d706c617465223b733a31303a223c6c616e67756167653e223b733a363a22706172616d73223b613a313a7b733a383a226c616e6775616765223b733a373a22285c777b327d29223b7d733a363a22617070656e64223b623a303b733a31313a22686173486f7374496e666f223b623a303b733a31343a220043436f6d706f6e656e74005f65223b4e3b733a31343a220043436f6d706f6e656e74005f6d223b4e3b7d693a323b4f3a383a224355726c52756c65223a31363a7b733a393a2275726c537566666978223b4e3b733a31333a226361736553656e736974697665223b4e3b733a31333a2264656661756c74506172616d73223b613a303a7b7d733a31303a226d6174636856616c7565223b4e3b733a343a2276657262223b4e3b733a31313a2270617273696e674f6e6c79223b623a303b733a353a22726f757465223b733a383a223c6d6f64756c653e223b733a31303a227265666572656e636573223b613a313a7b733a363a226d6f64756c65223b733a383a223c6d6f64756c653e223b7d733a31323a22726f7574655061747465726e223b733a32303a222f5e283f503c6d6f64756c653e5c772b29242f75223b733a373a227061747465726e223b733a34353a222f5e283f503c6c616e67756167653e285c777b327d29295c2f283f503c6d6f64756c653e5c772b295c2f242f75223b733a383a2274656d706c617465223b733a31393a223c6c616e67756167653e2f3c6d6f64756c653e223b733a363a22706172616d73223b613a313a7b733a383a226c616e6775616765223b733a373a22285c777b327d29223b7d733a363a22617070656e64223b623a303b733a31313a22686173486f7374496e666f223b623a303b733a31343a220043436f6d706f6e656e74005f65223b4e3b733a31343a220043436f6d706f6e656e74005f6d223b4e3b7d693a333b4f3a383a224355726c52756c65223a31363a7b733a393a2275726c537566666978223b4e3b733a31333a226361736553656e736974697665223b4e3b733a31333a2264656661756c74506172616d73223b613a303a7b7d733a31303a226d6174636856616c7565223b4e3b733a343a2276657262223b4e3b733a31313a2270617273696e674f6e6c79223b623a303b733a353a22726f757465223b733a32313a223c6d6f64756c653e2f3c636f6e74726f6c6c65723e223b733a31303a227265666572656e636573223b613a323a7b733a363a226d6f64756c65223b733a383a223c6d6f64756c653e223b733a31303a22636f6e74726f6c6c6572223b733a31323a223c636f6e74726f6c6c65723e223b7d733a31323a22726f7574655061747465726e223b733a34313a222f5e283f503c6d6f64756c653e5c772b295c2f283f503c636f6e74726f6c6c65723e5c772b29242f75223b733a373a227061747465726e223b733a36363a222f5e283f503c6c616e67756167653e285c777b327d29295c2f283f503c6d6f64756c653e5c772b295c2f283f503c636f6e74726f6c6c65723e5c772b295c2f242f75223b733a383a2274656d706c617465223b733a33323a223c6c616e67756167653e2f3c6d6f64756c653e2f3c636f6e74726f6c6c65723e223b733a363a22706172616d73223b613a313a7b733a383a226c616e6775616765223b733a373a22285c777b327d29223b7d733a363a22617070656e64223b623a303b733a31313a22686173486f7374496e666f223b623a303b733a31343a220043436f6d706f6e656e74005f65223b4e3b733a31343a220043436f6d706f6e656e74005f6d223b4e3b7d693a343b4f3a383a224355726c52756c65223a31363a7b733a393a2275726c537566666978223b4e3b733a31333a226361736553656e736974697665223b4e3b733a31333a2264656661756c74506172616d73223b613a303a7b7d733a31303a226d6174636856616c7565223b4e3b733a343a2276657262223b4e3b733a31313a2270617273696e674f6e6c79223b623a303b733a353a22726f757465223b733a33303a223c6d6f64756c653e2f3c636f6e74726f6c6c65723e2f3c616374696f6e3e223b733a31303a227265666572656e636573223b613a333a7b733a363a226d6f64756c65223b733a383a223c6d6f64756c653e223b733a31303a22636f6e74726f6c6c6572223b733a31323a223c636f6e74726f6c6c65723e223b733a363a22616374696f6e223b733a383a223c616374696f6e3e223b7d733a31323a22726f7574655061747465726e223b733a35383a222f5e283f503c6d6f64756c653e5c772b295c2f283f503c636f6e74726f6c6c65723e5c772b295c2f283f503c616374696f6e3e5c772b29242f75223b733a373a227061747465726e223b733a39363a222f5e283f503c6c616e67756167653e285c777b327d29295c2f283f503c6d6f64756c653e5c772b295c2f283f503c636f6e74726f6c6c65723e5c772b295c2f283f503c616374696f6e3e5c772b295c2f283f503c69643e5c642b295c2f242f75223b733a383a2274656d706c617465223b733a34363a223c6c616e67756167653e2f3c6d6f64756c653e2f3c636f6e74726f6c6c65723e2f3c616374696f6e3e2f3c69643e223b733a363a22706172616d73223b613a323a7b733a383a226c616e6775616765223b733a373a22285c777b327d29223b733a323a226964223b733a333a225c642b223b7d733a363a22617070656e64223b623a303b733a31313a22686173486f7374496e666f223b623a303b733a31343a220043436f6d706f6e656e74005f65223b4e3b733a31343a220043436f6d706f6e656e74005f6d223b4e3b7d693a353b4f3a383a224355726c52756c65223a31363a7b733a393a2275726c537566666978223b4e3b733a31333a226361736553656e736974697665223b4e3b733a31333a2264656661756c74506172616d73223b613a303a7b7d733a31303a226d6174636856616c7565223b4e3b733a343a2276657262223b4e3b733a31313a2270617273696e674f6e6c79223b623a303b733a353a22726f757465223b733a33303a223c6d6f64756c653e2f3c636f6e74726f6c6c65723e2f3c616374696f6e3e223b733a31303a227265666572656e636573223b613a333a7b733a363a226d6f64756c65223b733a383a223c6d6f64756c653e223b733a31303a22636f6e74726f6c6c6572223b733a31323a223c636f6e74726f6c6c65723e223b733a363a22616374696f6e223b733a383a223c616374696f6e3e223b7d733a31323a22726f7574655061747465726e223b733a35383a222f5e283f503c6d6f64756c653e5c772b295c2f283f503c636f6e74726f6c6c65723e5c772b295c2f283f503c616374696f6e3e5c772b29242f75223b733a373a227061747465726e223b733a38323a222f5e283f503c6c616e67756167653e285c777b327d29295c2f283f503c6d6f64756c653e5c772b295c2f283f503c636f6e74726f6c6c65723e5c772b295c2f283f503c616374696f6e3e5c772b295c2f2f75223b733a383a2274656d706c617465223b733a34313a223c6c616e67756167653e2f3c6d6f64756c653e2f3c636f6e74726f6c6c65723e2f3c616374696f6e3e223b733a363a22706172616d73223b613a313a7b733a383a226c616e6775616765223b733a373a22285c777b327d29223b7d733a363a22617070656e64223b623a313b733a31313a22686173486f7374496e666f223b623a303b733a31343a220043436f6d706f6e656e74005f65223b4e3b733a31343a220043436f6d706f6e656e74005f6d223b4e3b7d693a363b4f3a383a224355726c52756c65223a31363a7b733a393a2275726c537566666978223b4e3b733a31333a226361736553656e736974697665223b4e3b733a31333a2264656661756c74506172616d73223b613a303a7b7d733a31303a226d6174636856616c7565223b4e3b733a343a2276657262223b4e3b733a31313a2270617273696e674f6e6c79223b623a303b733a353a22726f757465223b733a32313a223c636f6e74726f6c6c65723e2f3c616374696f6e3e223b733a31303a227265666572656e636573223b613a323a7b733a31303a22636f6e74726f6c6c6572223b733a31323a223c636f6e74726f6c6c65723e223b733a363a22616374696f6e223b733a383a223c616374696f6e3e223b7d733a31323a22726f7574655061747465726e223b733a34313a222f5e283f503c636f6e74726f6c6c65723e5c772b295c2f283f503c616374696f6e3e5c772b29242f75223b733a373a227061747465726e223b733a37393a222f5e283f503c6c616e67756167653e285c777b327d29295c2f283f503c636f6e74726f6c6c65723e5c772b295c2f283f503c616374696f6e3e5c772b295c2f283f503c69643e5c642b295c2f242f75223b733a383a2274656d706c617465223b733a33373a223c6c616e67756167653e2f3c636f6e74726f6c6c65723e2f3c616374696f6e3e2f3c69643e223b733a363a22706172616d73223b613a323a7b733a383a226c616e6775616765223b733a373a22285c777b327d29223b733a323a226964223b733a333a225c642b223b7d733a363a22617070656e64223b623a303b733a31313a22686173486f7374496e666f223b623a303b733a31343a220043436f6d706f6e656e74005f65223b4e3b733a31343a220043436f6d706f6e656e74005f6d223b4e3b7d693a373b4f3a383a224355726c52756c65223a31363a7b733a393a2275726c537566666978223b4e3b733a31333a226361736553656e736974697665223b4e3b733a31333a2264656661756c74506172616d73223b613a303a7b7d733a31303a226d6174636856616c7565223b4e3b733a343a2276657262223b4e3b733a31313a2270617273696e674f6e6c79223b623a303b733a353a22726f757465223b733a33303a223c6d6f64756c653e2f3c636f6e74726f6c6c65723e2f3c616374696f6e3e223b733a31303a227265666572656e636573223b613a333a7b733a363a226d6f64756c65223b733a383a223c6d6f64756c653e223b733a31303a22636f6e74726f6c6c6572223b733a31323a223c636f6e74726f6c6c65723e223b733a363a22616374696f6e223b733a383a223c616374696f6e3e223b7d733a31323a22726f7574655061747465726e223b733a35383a222f5e283f503c6d6f64756c653e5c772b295c2f283f503c636f6e74726f6c6c65723e5c772b295c2f283f503c616374696f6e3e5c772b29242f75223b733a373a227061747465726e223b733a37333a222f5e283f503c6d6f64756c653e5c772b295c2f283f503c636f6e74726f6c6c65723e5c772b295c2f283f503c616374696f6e3e5c772b295c2f283f503c69643e5c642b295c2f242f75223b733a383a2274656d706c617465223b733a33353a223c6d6f64756c653e2f3c636f6e74726f6c6c65723e2f3c616374696f6e3e2f3c69643e223b733a363a22706172616d73223b613a313a7b733a323a226964223b733a333a225c642b223b7d733a363a22617070656e64223b623a303b733a31313a22686173486f7374496e666f223b623a303b733a31343a220043436f6d706f6e656e74005f65223b4e3b733a31343a220043436f6d706f6e656e74005f6d223b4e3b7d693a383b4f3a383a224355726c52756c65223a31363a7b733a393a2275726c537566666978223b4e3b733a31333a226361736553656e736974697665223b4e3b733a31333a2264656661756c74506172616d73223b613a303a7b7d733a31303a226d6174636856616c7565223b4e3b733a343a2276657262223b4e3b733a31313a2270617273696e674f6e6c79223b623a303b733a353a22726f757465223b733a32313a223c636f6e74726f6c6c65723e2f3c616374696f6e3e223b733a31303a227265666572656e636573223b613a323a7b733a31303a22636f6e74726f6c6c6572223b733a31323a223c636f6e74726f6c6c65723e223b733a363a22616374696f6e223b733a383a223c616374696f6e3e223b7d733a31323a22726f7574655061747465726e223b733a34313a222f5e283f503c636f6e74726f6c6c65723e5c772b295c2f283f503c616374696f6e3e5c772b29242f75223b733a373a227061747465726e223b733a35363a222f5e283f503c636f6e74726f6c6c65723e5c772b295c2f283f503c616374696f6e3e5c772b295c2f283f503c69643e5c642b295c2f242f75223b733a383a2274656d706c617465223b733a32363a223c636f6e74726f6c6c65723e2f3c616374696f6e3e2f3c69643e223b733a363a22706172616d73223b613a313a7b733a323a226964223b733a333a225c642b223b7d733a363a22617070656e64223b623a303b733a31313a22686173486f7374496e666f223b623a303b733a31343a220043436f6d706f6e656e74005f65223b4e3b733a31343a220043436f6d706f6e656e74005f6d223b4e3b7d7d693a313b733a33323a223231376265623136306465383466396634373531353834613631316562313638223b7d693a313b4e3b7d
\.


--
-- TOC entry 2148 (class 0 OID 16440)
-- Dependencies: 181
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY message (id, language, translation, lang_id) FROM stdin;
15	ru	Перевод	2
16	ru	Действия	2
9	ru	Категория	2
11	ru	Перевод	2
10	ru	Исходный текст	2
14	ru	Текст	2
14	en	Texts	3
17	ru	Заголовок новости / url	2
18	ru	Категория новости	2
19	ru	Статус	2
22	ru	Добавить	2
23	ru	Удалить	2
24	ru	Удалить	2
25	ru	Редактировать	2
26	ru	Редактировать	2
27	ru	Словарь	2
28	ru	Изображение	2
8	ru	Создание нового мультиязычного текста	2
30	ru	Соответствия	2
31	ru	Доступ запрещен.	2
32	ru	Добавить	2
33	ru	Добавить потомка	2
35	ru	Предки	2
36	ru	Вы уверены, что хотите удалить данный элемент?	2
37	ru	Назначить	2
38	ru	Назначить права	2
39	ru	Назначенные элементы	2
40	ru	Бизнес правило	2
41	ru	Отменить	2
42	ru	Создать	2
43	ru	Удалить	2
44	ru	Данные	2
45	ru	Потомки	2
46	ru	Описание	2
47	ru	Редактировать	2
48	ru	Неверный запрос.	2
49	ru	Элемент не существует.	2
50	ru	Элементы	2
51	ru	Элемент назначен данному пользователю	2
53	ru	Не найдено назначений	2
54	ru	Не найдено {type}	2
55	ru	Страница не найдена	2
56	ru	Права	2
57	ru	Права, предоставляемые элементом	2
58	ru	Права, которые наследует данный элемент	2
59	ru	Удалить	2
60	ru	Отнять	2
61	ru	Сохранить	2
62	ru	Выбрать элемент	2
63	ru	Системное название	2
64	ru	Системное название не может быть изменено после создания.	2
65	ru	Данный элемент не имеет предков.	2
66	ru	Данный элемент не имеет потомков.	2
67	ru	Данный пользователь не имеет прав.	2
68	ru	Тип	2
69	ru	Пользователь	2
70	ru	Просмотреть	2
71	ru	операция|операции	2
72	ru	роль|роли	2
73	ru	задание|задания	2
52	ru	Новая(ое) {type}	2
34	ru	Добавить	2
\.


--
-- TOC entry 2149 (class 0 OID 16446)
-- Dependencies: 182
-- Data for Name: sourcemessage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY sourcemessage (id, category, message) FROM stdin;
15	texts	Translate to current language
16	main	Action
9	texts	Category
11	texts	Translates
10	texts	Source text
14	texts	Original text
17	modNews	New title / url
18	modNews	News category
19	modNews	Status
22	main	Add
23	main	Remove
24	main	Delete
25	main	Edit
26	main	Edit
27	texts	Dictionary
28	main	Image
8	texts	Create new multilanguage text translate
30	AuthModule.main	Assignments
31	AuthModule.main	Access denied
32	AuthModule.main	Add
33	AuthModule.main	Add child
35	AuthModule.main	Ancestors
36	AuthModule.main	Are you sure you want to delete this item?
37	AuthModule.main	Assign
38	AuthModule.main	Assign permission
39	AuthModule.main	Assigned items
40	AuthModule.main	Business rule
41	AuthModule.main	Cancel
42	AuthModule.main	Create
43	AuthModule.main	Delete
44	AuthModule.main	Data
45	AuthModule.main	Descendants
46	AuthModule.main	Description
47	AuthModule.main	Edit
48	AuthModule.main	Invalid request.
49	AuthModule.main	Item does not exist.
50	AuthModule.main	Items
51	AuthModule.main	Items assigned to this user
53	AuthModule.main	No assignments found.
54	AuthModule.main	No {type} found.
55	AuthModule.main	Page not found.
56	AuthModule.main	Permissions
57	AuthModule.main	Permissions granted by this item
58	AuthModule.main	Permissions that inherit this item
59	AuthModule.main	Remove
60	AuthModule.main	Revoke
61	AuthModule.main	Save
62	AuthModule.main	Select item
63	AuthModule.main	System name
64	AuthModule.main	System name cannot be changed after creation.
65	AuthModule.main	This item does not have any ancestors.
66	AuthModule.main	This item does not have any descendants.
67	AuthModule.main	This user does not have any assignments.
68	AuthModule.main	Type
69	AuthModule.main	User
70	AuthModule.main	View
71	AuthModule.main	operation|operations
72	AuthModule.main	role|roles
73	AuthModule.main	task|tasks
52	AuthModule.main	New {type}
34	AuthModule.main	Add {type}
\.


--
-- TOC entry 2047 (class 2606 OID 16453)
-- Name: AuthAssignment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AuthAssignment"
    ADD CONSTRAINT "AuthAssignment_pkey" PRIMARY KEY (itemname, userid);


--
-- TOC entry 2051 (class 2606 OID 16455)
-- Name: AuthItemChild_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AuthItemChild"
    ADD CONSTRAINT "AuthItemChild_pkey" PRIMARY KEY (parent, child);


--
-- TOC entry 2049 (class 2606 OID 16457)
-- Name: AuthItem_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AuthItem"
    ADD CONSTRAINT "AuthItem_pkey" PRIMARY KEY (name);


--
-- TOC entry 2053 (class 2606 OID 16459)
-- Name: Langs_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Langs"
    ADD CONSTRAINT "Langs_id_key" UNIQUE (id);


--
-- TOC entry 2059 (class 2606 OID 16461)
-- Name: Langs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Langs"
    ADD CONSTRAINT "Langs_pkey" PRIMARY KEY (id);


--
-- TOC entry 2061 (class 2606 OID 16463)
-- Name: Langs_short_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Langs"
    ADD CONSTRAINT "Langs_short_key" UNIQUE (short);


--
-- TOC entry 2121 (class 2606 OID 57450)
-- Name: ModNewsCategoryTexts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ModNewsCategoryTexts"
    ADD CONSTRAINT "ModNewsCategoryTexts_pkey" PRIMARY KEY (id);


--
-- TOC entry 2117 (class 2606 OID 57442)
-- Name: ModNewsCategory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ModNewsCategory"
    ADD CONSTRAINT "ModNewsCategory_pkey" PRIMARY KEY (id);


--
-- TOC entry 2113 (class 2606 OID 57423)
-- Name: ModNewsTexts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ModNewsTexts"
    ADD CONSTRAINT "ModNewsTexts_pkey" PRIMARY KEY (id);


--
-- TOC entry 2109 (class 2606 OID 57415)
-- Name: ModNews_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ModNews"
    ADD CONSTRAINT "ModNews_pkey" PRIMARY KEY (id);


--
-- TOC entry 2070 (class 2606 OID 24584)
-- Name: ModPagesTexts_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ModPagesTexts"
    ADD CONSTRAINT "ModPagesTexts_id_key" UNIQUE (id);


--
-- TOC entry 2072 (class 2606 OID 24582)
-- Name: ModPagesTexts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ModPagesTexts"
    ADD CONSTRAINT "ModPagesTexts_pkey" PRIMARY KEY (id);


--
-- TOC entry 2063 (class 2606 OID 16465)
-- Name: ModPages_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ModPages"
    ADD CONSTRAINT "ModPages_id_key" UNIQUE (id);


--
-- TOC entry 2065 (class 2606 OID 16469)
-- Name: ModPages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ModPages"
    ADD CONSTRAINT "ModPages_pkey" PRIMARY KEY (id);


--
-- TOC entry 2087 (class 2606 OID 49163)
-- Name: ModUploaderImg_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ModUploaderImg"
    ADD CONSTRAINT "ModUploaderImg_id_key" UNIQUE (id);


--
-- TOC entry 2089 (class 2606 OID 32782)
-- Name: ModUploaderImg_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ModUploaderImg"
    ADD CONSTRAINT "ModUploaderImg_pkey" PRIMARY KEY (id);


--
-- TOC entry 2092 (class 2606 OID 40975)
-- Name: Modules_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Modules"
    ADD CONSTRAINT "Modules_name_key" UNIQUE (name);


--
-- TOC entry 2094 (class 2606 OID 40973)
-- Name: Modules_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Modules"
    ADD CONSTRAINT "Modules_pkey" PRIMARY KEY (id);


--
-- TOC entry 2105 (class 2606 OID 57374)
-- Name: UrlManager_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "UrlManager"
    ADD CONSTRAINT "UrlManager_pkey" PRIMARY KEY (id);


--
-- TOC entry 2123 (class 2606 OID 90210)
-- Name: UsersData_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "UsersData"
    ADD CONSTRAINT "UsersData_pkey" PRIMARY KEY (user_id);


--
-- TOC entry 2125 (class 2606 OID 106603)
-- Name: cache_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cache
    ADD CONSTRAINT cache_pkey PRIMARY KEY (id);


--
-- TOC entry 2083 (class 2606 OID 16475)
-- Name: message_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id, language);


--
-- TOC entry 2085 (class 2606 OID 16477)
-- Name: sourcemessage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sourcemessage
    ADD CONSTRAINT sourcemessage_pkey PRIMARY KEY (id);


--
-- TOC entry 2077 (class 2606 OID 16479)
-- Name: user_idx_email01; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Users"
    ADD CONSTRAINT user_idx_email01 UNIQUE (email);


--
-- TOC entry 2079 (class 2606 OID 16481)
-- Name: user_idx_username01; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Users"
    ADD CONSTRAINT user_idx_username01 UNIQUE (username);


--
-- TOC entry 2081 (class 2606 OID 16483)
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Users"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 2054 (class 1259 OID 49168)
-- Name: Langs_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "Langs_idx" ON "Langs" USING btree (front);


--
-- TOC entry 2055 (class 1259 OID 49169)
-- Name: Langs_idx1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "Langs_idx1" ON "Langs" USING btree (back);


--
-- TOC entry 2056 (class 1259 OID 49171)
-- Name: Langs_idx2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "Langs_idx2" ON "Langs" USING btree (back_def);


--
-- TOC entry 2057 (class 1259 OID 49172)
-- Name: Langs_idx3; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "Langs_idx3" ON "Langs" USING btree (front_def);


--
-- TOC entry 2118 (class 1259 OID 57478)
-- Name: ModNewsCategoryTexts_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "ModNewsCategoryTexts_idx" ON "ModNewsCategoryTexts" USING btree (category_id);


--
-- TOC entry 2119 (class 1259 OID 57479)
-- Name: ModNewsCategoryTexts_idx1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "ModNewsCategoryTexts_idx1" ON "ModNewsCategoryTexts" USING btree (lang_id);


--
-- TOC entry 2114 (class 1259 OID 57476)
-- Name: ModNewsCategory_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "ModNewsCategory_idx" ON "ModNewsCategory" USING btree (visible);


--
-- TOC entry 2115 (class 1259 OID 57477)
-- Name: ModNewsCategory_idx1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "ModNewsCategory_idx1" ON "ModNewsCategory" USING btree ("order");


--
-- TOC entry 2110 (class 1259 OID 57436)
-- Name: ModNewsTexts_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "ModNewsTexts_idx" ON "ModNewsTexts" USING btree (news_id);


--
-- TOC entry 2111 (class 1259 OID 57437)
-- Name: ModNewsTexts_idx1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "ModNewsTexts_idx1" ON "ModNewsTexts" USING btree (lang_id);


--
-- TOC entry 2106 (class 1259 OID 57424)
-- Name: ModNews_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "ModNews_idx" ON "ModNews" USING btree (visible);


--
-- TOC entry 2107 (class 1259 OID 57425)
-- Name: ModNews_idx1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "ModNews_idx1" ON "ModNews" USING btree ("order");


--
-- TOC entry 2095 (class 1259 OID 57375)
-- Name: UrlManager_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "UrlManager_idx" ON "UrlManager" USING btree (root);


--
-- TOC entry 2096 (class 1259 OID 57376)
-- Name: UrlManager_idx1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "UrlManager_idx1" ON "UrlManager" USING btree (lft);


--
-- TOC entry 2097 (class 1259 OID 57377)
-- Name: UrlManager_idx2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "UrlManager_idx2" ON "UrlManager" USING btree (rgt);


--
-- TOC entry 2098 (class 1259 OID 57378)
-- Name: UrlManager_idx3; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "UrlManager_idx3" ON "UrlManager" USING btree (level);


--
-- TOC entry 2099 (class 1259 OID 57379)
-- Name: UrlManager_idx4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "UrlManager_idx4" ON "UrlManager" USING btree (module_id);


--
-- TOC entry 2100 (class 1259 OID 57380)
-- Name: UrlManager_idx5; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "UrlManager_idx5" ON "UrlManager" USING btree (position_id);


--
-- TOC entry 2101 (class 1259 OID 57382)
-- Name: UrlManager_idx6; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "UrlManager_idx6" ON "UrlManager" USING btree (translit);


--
-- TOC entry 2102 (class 1259 OID 57383)
-- Name: UrlManager_idx7; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "UrlManager_idx7" ON "UrlManager" USING btree (lang_id);


--
-- TOC entry 2103 (class 1259 OID 57481)
-- Name: UrlManager_idx8; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "UrlManager_idx8" ON "UrlManager" USING btree (category);


--
-- TOC entry 2073 (class 1259 OID 24579)
-- Name: id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX id ON "ModPagesTexts" USING btree (id);


--
-- TOC entry 2090 (class 1259 OID 49161)
-- Name: id_unik; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX id_unik ON "ModUploaderImg" USING btree (id);


--
-- TOC entry 2074 (class 1259 OID 16484)
-- Name: lang_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lang_id ON "ModPagesTexts" USING btree (lang_id);


--
-- TOC entry 2066 (class 1259 OID 16486)
-- Name: order; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "order" ON "ModPages" USING btree ("order");


--
-- TOC entry 2075 (class 1259 OID 16487)
-- Name: page_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX page_id ON "ModPagesTexts" USING btree (page_id);


--
-- TOC entry 2067 (class 1259 OID 16488)
-- Name: publish; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX publish ON "ModPages" USING btree (publish);


--
-- TOC entry 2068 (class 1259 OID 16490)
-- Name: visible; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visible ON "ModPages" USING btree (visible);


--
-- TOC entry 2126 (class 2606 OID 16491)
-- Name: AuthAssignment_itemname_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "AuthAssignment"
    ADD CONSTRAINT "AuthAssignment_itemname_fkey" FOREIGN KEY (itemname) REFERENCES "AuthItem"(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2127 (class 2606 OID 16496)
-- Name: AuthItemChild_child_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "AuthItemChild"
    ADD CONSTRAINT "AuthItemChild_child_fkey" FOREIGN KEY (child) REFERENCES "AuthItem"(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2128 (class 2606 OID 16501)
-- Name: AuthItemChild_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "AuthItemChild"
    ADD CONSTRAINT "AuthItemChild_parent_fkey" FOREIGN KEY (parent) REFERENCES "AuthItem"(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2138 (class 2606 OID 57466)
-- Name: ModNewsCategoryTexts_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "ModNewsCategoryTexts"
    ADD CONSTRAINT "ModNewsCategoryTexts_fk" FOREIGN KEY (category_id) REFERENCES "ModNewsCategory"(id) ON DELETE CASCADE;


--
-- TOC entry 2139 (class 2606 OID 57471)
-- Name: ModNewsCategoryTexts_fk1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "ModNewsCategoryTexts"
    ADD CONSTRAINT "ModNewsCategoryTexts_fk1" FOREIGN KEY (lang_id) REFERENCES "Langs"(id) ON DELETE CASCADE;


--
-- TOC entry 2136 (class 2606 OID 57456)
-- Name: ModNewsTexts_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "ModNewsTexts"
    ADD CONSTRAINT "ModNewsTexts_fk" FOREIGN KEY (news_id) REFERENCES "ModNews"(id) ON DELETE CASCADE;


--
-- TOC entry 2137 (class 2606 OID 57461)
-- Name: ModNewsTexts_fk1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "ModNewsTexts"
    ADD CONSTRAINT "ModNewsTexts_fk1" FOREIGN KEY (lang_id) REFERENCES "Langs"(id) ON DELETE CASCADE;


--
-- TOC entry 2135 (class 2606 OID 57451)
-- Name: ModNews_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "ModNews"
    ADD CONSTRAINT "ModNews_fk" FOREIGN KEY (category_id) REFERENCES "ModNewsCategory"(id) ON DELETE CASCADE;


--
-- TOC entry 2129 (class 2606 OID 16506)
-- Name: ModPagesTexts_lang_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "ModPagesTexts"
    ADD CONSTRAINT "ModPagesTexts_lang_id_fkey" FOREIGN KEY (lang_id) REFERENCES "Langs"(id) ON DELETE CASCADE;


--
-- TOC entry 2130 (class 2606 OID 16511)
-- Name: ModPagesTexts_page_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "ModPagesTexts"
    ADD CONSTRAINT "ModPagesTexts_page_id_fkey" FOREIGN KEY (page_id) REFERENCES "ModPages"(id) ON DELETE CASCADE;


--
-- TOC entry 2133 (class 2606 OID 57384)
-- Name: UrlManager_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "UrlManager"
    ADD CONSTRAINT "UrlManager_fk" FOREIGN KEY (module_id) REFERENCES "Modules"(id) ON DELETE CASCADE;


--
-- TOC entry 2134 (class 2606 OID 57389)
-- Name: UrlManager_fk1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "UrlManager"
    ADD CONSTRAINT "UrlManager_fk1" FOREIGN KEY (lang_id) REFERENCES "Langs"(id) ON DELETE CASCADE;


--
-- TOC entry 2140 (class 2606 OID 90202)
-- Name: UsersData_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "UsersData"
    ADD CONSTRAINT "UsersData_fk" FOREIGN KEY (user_id) REFERENCES "Users"(id) ON DELETE CASCADE;


--
-- TOC entry 2131 (class 2606 OID 16521)
-- Name: fk_message_sourcemessage; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY message
    ADD CONSTRAINT fk_message_sourcemessage FOREIGN KEY (id) REFERENCES sourcemessage(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 2132 (class 2606 OID 57405)
-- Name: message_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_fk FOREIGN KEY (lang_id) REFERENCES "Langs"(id) ON DELETE CASCADE;


--
-- TOC entry 2165 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2013-03-16 22:39:34

--
-- PostgreSQL database dump complete
--

