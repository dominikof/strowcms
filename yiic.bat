@echo off

@setlocal

set BIN_PATH=%~dp0

if "%PHP_COMMAND%" == "" set PHP_COMMAND=C:\OpenSerer\modules\php\PHP-5.3.17\php.exe

%PHP_COMMAND% "%BIN_PATH%console\yiic.php" %*

@endlocal
