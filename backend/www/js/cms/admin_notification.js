admin_notification = {

    showNotyError:function (texts) {
        for (key in texts) {
            noty({
                text:texts[key],
                type:'error',
                layout: 'topRight',
                dismissQueue:false
            });
        }
    },

    showNotySuccess:function (msg) {
        noty({
            text:msg,
            layout: 'topRight',
            type:'success',
            dismissQueue:false,
            timeout:3000
        });

    },

    showNotyAnswer:function (msgtxt, oktxt, canceltxt,callback) {
        noty({
            text:msgtxt,
            layout: 'topRight',
            buttons:[
                {
                    addClass:'btn btn-primary',
                    text:oktxt,
                    onClick:function ($noty) {
                        $noty.close();
                        callback();
                    }
                },
                {
                    addClass:'btn btn-danger',
                    text:canceltxt,
                    onClick:function ($noty) {
                        $noty.close();
                    }
                }
            ]
        });
    }
}