$(document).ready(function(){
    $("#modalSubmitBtnId").click(function(e){
        e.preventDefault();
        url=$(this).attr('href');
        cmsAjaxSubmit(url,"#modalEditFormId",beforeSubmitStandart,editModalSuccess);
    });
    twBootstrapJsInit();
});

function editModalSuccess(responseText){
    if(ajaxResponse(responseText)){
        init_modal_edit_link();
        init_modal_add_link();
        init_modal_del_link();
        makeSortable();
        admin_modal.closeModalDialog();
    }
}