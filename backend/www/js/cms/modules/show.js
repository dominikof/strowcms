$(document).ready(function () {
    init_modal_edit_link();
    init_modal_add_link();
    init_modal_del_link();
    makeSortable();
    init_actions();
});


function init_modal_edit_link(func) {
    if (func)
        $(".edit-content").click(func);
    else {
        $(".edit-content").click(function (event) {
            event.preventDefault();
            url = $(this).attr('href');
            cmsAjaxSend(url, "POST", '', beforeSubmitStandart, ajaxResponse);
        });
    }
}
function init_modal_add_link(func) {
    if (func)
        $(".add-content").click(func);
    else {
        $(".add-content").click(function (event) {
            event.preventDefault();
            url = $(this).attr('href');
            cmsAjaxSend(url, "POST", '', beforeSubmitStandart, ajaxResponse);
        });
    }
}

function init_modal_del_link(func) {
    if (func)
        $(".del-btn").click(func);
    else {
        $(".del-btn").click(function (event) {
            event.preventDefault();
            url = $(this).attr('href');
            oktext= $(this).attr('oktext');
            notext= $(this).attr('notext');
            msgtext= $(this).attr('msgtext');
            admin_notification.showNotyAnswer(msgtext,oktext,notext,function(){
                cmsAjaxSend(url,"POST", $('input[type="checkbox"][name*="check"]:checked').serialize(), beforeSubmitStandart, function(responseText){
                    ajaxResponse(responseText);
                    init_modal_edit_link();
                    init_modal_add_link();
                    init_modal_del_link();
                });
            });
        });
    }
}

function init_actions(){
    $("#actionBtnId").click(function(){
            var selectedOption=$("#actionSelectId option:selected");
            var url=selectedOption.data('action-url');
            var data=$(this).closest('form').serialize();
            cmsAjaxSend(url, 'POST',data,beforeSubmitStandart,ajaxResponse);
    });
    actions['goToPage']=function(params){
        window.location.href = $("#paginationPageId"+params.page).attr('href');
    }
}

function makeSortable(){
    $('.sortable-table').sortable({
        items:             "tr.tr-sortable",
        tolerance:         "pointer",
        handle:            ".move_zone",
        scrollSensitivity: 40,
        opacity:           0.7,

        helper: function(event, ui){

            //width hack
//            if($(ui[0]).find('input[type="checkbox"][name*="check"]').attr('checked')!='checked'){
//                $('input[type="checkbox"][name*="check"]').attr('checked', false);
//            }

            ui.children().each(function() {
                $(this).width($(this).width());
            });
            //group sorting
            if($('input[type="checkbox"][name*="check"]:checked').size()<1) return ui;
            var helper = $('<tr/>');
            $('input[type="checkbox"][name*="check"]:checked').each(function(){
                var item = $(this).closest('tr.tr-sortable');
                helper.height(helper.height()+item.innerHeight());
                if(item[0]!=ui[0]) {
                    helper.append(item.clone());
                    $(this).closest('tr.tr-sortable').remove();
                }
                else {
                    helper.append(ui.clone());
                    item.find('input[type="checkbox"][name*="check"]').attr('checked', false);
                }
            });
            return helper;
        },
        remove:function(event, ui){
            this.cancel();
        },
        beforeStop:function(event, ui){
            if(ui.helper.children('tr.tr-sortable').size()>0){
                ui.helper.children('tr.tr-sortable').each(function(){
                    $(this).insertBefore(ui.item);
                });
                ui.item.remove();
            }
        },
        start:function(event, ui){
            ui.placeholder.height(ui.helper.height());
        },
        update:function(event, ui)
        {
            url=$(".sortable-table").attr('data-order-url');
            cmsAjaxSend(url, "POST",$('input[type="hidden"][name*="order"]').serialize(),beforeSubmitStandart,endAjaxAnimation);
            $(".sortable-table input[name*='check']").attr('checked', false);
        }
    });

    $(".item-dropable").droppable({
        activeClass: "btn-success",
        hoverClass: "btn-info",
        tolerance: "pointer",
        drop: function( event, ui ) {
            var pageUrl=$(this).attr('href');
            var orderDataObj=ui.draggable.find('input[type=hidden]');
            var orderDataStr=orderDataObj.attr('name')+'='+orderDataObj.val();
            var urlParams=orderDataStr+'&dropToPage=1';
            if(pageUrl.indexOf('?')+1)
                urlParams='&'+urlParams;
            else
                urlParams+='?'+urlParams;
            alert(urlParams);
//            window.location.href=urlParams;
        }
    });
}