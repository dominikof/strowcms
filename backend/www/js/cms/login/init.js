$(document).ready(function(){

    $("#LoginForm_username").focus(function() {

        $(this).parent(".input-prepend").addClass("input-prepend-focus");

    });

    $("#LoginForm_username").focusout(function() {

        $(this).parent(".input-prepend").removeClass("input-prepend-focus");

    });

    $("#LoginForm_password").focus(function() {

        $(this).parent(".input-prepend").addClass("input-prepend-focus");

    });

    $("#LoginForm_password").focusout(function() {

        $(this).parent(".input-prepend").removeClass("input-prepend-focus");

    });

    actions = {
        active_form_validation:function(params){
            for(msg in params)
                admin_notification.showNotyError(params[msg]);
        },
        make_login:function(params){
            $("#login-form").unbind();
            $("#login-form").submit();
        }
    }



    $("#login-form").submit(function(e){
       e.preventDefault();
       cmsAjaxSend('/admin/login/','POST',$("#login-form").serialize()+"&ajax=login-form",beforeSubmitStandart,ajaxResponse);
    });

    /* ---------- Acivate Functions ---------- */
    $("input:checkbox, input:radio, input:file").not('[data-no-uniform="true"],#uniform-is-ajax').uniform();

});