$(document).ready(function () {

//    twBootstrapJsInit();


    widthFunctions();

    initFormAjaxValidation();

    $('.btn-minimize').click(function(e){
        e.preventDefault();
        var $target = $(this).parent().parent().next('.box-content');
        if($target.is(':visible')) $('i',$(this)).removeClass('icon-chevron-up').addClass('icon-chevron-down');
        else 					   $('i',$(this)).removeClass('icon-chevron-down').addClass('icon-chevron-up');
        $target.slideToggle();
    });

    initFilters();
    template_functions();
});

function initFilters(){
    $(".filter").keyup(function(event){
        if(event.which==13){
            sendFilters();
        }
    });
    $('.filter-select').change(function(){
        sendFilters();
    });
}

function sendFilters(){
    var requestData;
    var input=$(this);
    $(".filter").each(function(){
        requestData+='&'+$(this).attr('name')+'='+$(this).val();
    });
    var filterUrl=$("#filterUrlId").val();

    cmsAjaxSend(filterUrl,'post',requestData,beforeSubmitStandart,function(response){
        ajaxResponse(response);
        initFilters();
    });
}


function initFormAjaxValidation(){
    $("form.ajax").submit(function(e){
        e.preventDefault();
        url = $(this).attr('action');
        cmsAjaxSend(url,'POST',$(this).serialize()+"&ajax=1&formId="+$(this).attr('id')+"fromActionId="+$("#fromActionId").val(),beforeSubmitStandart,ajaxResponse);
    });
}




function twBootstrapJsInit(){
    $('#settingsTabId a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('i[rel=tooltip],span[rel=tooltip],a[rel=tooltip]').tooltip();


}



/* ---------- Page width functions ---------- */

$(window).bind("resize", widthFunctions);

function widthFunctions( e ) {
    var winHeight = $(window).height();
    var winWidth = $(window).width();

    if (winHeight) {

        $("#content").css("min-height",winHeight);

    }

    if (winWidth < 980 && winWidth > 767) {

        if($(".main-menu-span").hasClass("span2")) {

            $(".main-menu-span").removeClass("span2");
            $(".main-menu-span").addClass("span1");

        }

        if($("#content").hasClass("span10")) {

            $("#content").removeClass("span10");
            $("#content").addClass("span11");

        }


        $("a").each(function(){

            if($(this).hasClass("quick-button-small span1")) {

                $(this).removeClass("quick-button-small span1");
                $(this).addClass("quick-button span2 changed");

            }

        });

        $(".circleStatsItem").each(function() {

            var getOnTablet = $(this).parent().attr('onTablet');
            var getOnDesktop = $(this).parent().attr('onDesktop');

            if (getOnTablet) {

                $(this).parent().removeClass(getOnDesktop);
                $(this).parent().addClass(getOnTablet);

            }

        });

        $(".box").each(function(){

            var getOnTablet = $(this).attr('onTablet');
            var getOnDesktop = $(this).attr('onDesktop');

            if (getOnTablet) {

                $(this).removeClass(getOnDesktop);
                $(this).addClass(getOnTablet);

            }

        });

    } else {

        if($(".main-menu-span").hasClass("span1")) {

            $(".main-menu-span").removeClass("span1");
            $(".main-menu-span").addClass("span2");

        }

        if($("#content").hasClass("span11")) {

            $("#content").removeClass("span11");
            $("#content").addClass("span10");

        }

        $("a").each(function(){

            if($(this).hasClass("quick-button span2 changed")) {

                $(this).removeClass("quick-button span2 changed");
                $(this).addClass("quick-button-small span1");

            }

        });

        $(".circleStatsItem").each(function() {

            var getOnTablet = $(this).parent().attr('onTablet');
            var getOnDesktop = $(this).parent().attr('onDesktop');

            if (getOnTablet) {

                $(this).parent().removeClass(getOnTablet);
                $(this).parent().addClass(getOnDesktop);

            }

        });

        $(".box").each(function(){

            var getOnTablet = $(this).attr('onTablet');
            var getOnDesktop = $(this).attr('onDesktop');

            if (getOnTablet) {

                $(this).removeClass(getOnTablet);
                $(this).addClass(getOnDesktop);

            }

        });

    }

}

function template_functions(){
    $('.dropmenu').click(function(e){

        e.preventDefault();

        $(this).parent().find('ul').slideToggle();

    });

    $('ul.main-menu li a').each(function(){

        if($(this).hasClass('submenu')) {

            if($($(this))[0].href==String(window.location)) {
                $(this).addClass('active');
                $(this).parent().parent().parent().addClass('active');
                $(this).parent().parent().slideToggle();
            }
        } else {

            if($($(this))[0].href==String(window.location)) {

                $(this).parent().addClass('active');

            }
        }
    });
}