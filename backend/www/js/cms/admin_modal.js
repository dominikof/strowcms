/**
 * admin_modal_ajax
 * @author Panarin Sergey
 * @type {Object}
 */
admin_modal = {
    showModalDialog:function (html) {
        if (!$("div").is("#modalContentId"))
            $("body").append("<div id='modalContentId' class='modal hide fade'></div>");
        $("#modalContentId").html(html).modal({
            backdrop:true
        });
    },
    closeModalDialog:function(){
        $(".close").trigger('click');
    }
}