




function cmsAjaxSend(url,type,data,beforeSubmit,success){
    $.ajax({
        url:url,
        type:type,
        data:data,
        beforeSend:beforeSubmit,
        success:success
    });
}

function cmsAjaxSubmit(url,form,beforeSubmit,success){
    $(form).ajaxSubmit({
        url:url,
        success:success,
        beforeSubmit:beforeSubmit
    });
}

function beforeSubmitStandart(formData, jqForm, options){
    $("#cmsAjaxLoaderId").addClass('cms-ajax-loader-show');
}

function endAjaxAnimation(){
    $("#cmsAjaxLoaderId").removeClass('cms-ajax-loader-show');
}

function ajaxResponse(responseText) {
    endAjaxAnimation();
    try {
        var json = $.parseJSON(responseText);
        var retval=actions[json.action](json.params);
        if(retval) return retval;
        if (!json.result) {
            return false;
        }
            return true;

    } catch (e) {
        alert("Возникла ошибка. Попробуйте ещё раз или обратитесь к администрации.<br/>" + e.message+"<br/>"+responseText);
//        bootbox.alert("Возникла ошибка. Попробуйте ещё раз или обратитесь к администрации.<br/>" + e.message+"<br/>"+responseText);
    }
}
var actions = {
    noty_error:function(params){
        if(params)
            admin_notification.showNotyError(params);
        else
            admin_notification.showNotyError(Array['Возникла неопределённая ошибка']);
    },
    noty_success:function(params){
        if(params.msg)
          admin_notification.showNotySuccess(params.msg)
        actions.past_to_div(params);
    },
    past_to_div:function(params){
        if(params.div && params.content){
            $("#"+params.div).html(params.content);
        }
    },
    return_html:function(params){
        if(params.return_html){
            return params.return_html;
        }
    },
    modal_form:function(params){
        admin_modal.showModalDialog(params.content);
    },
    redirect:function(params){
        window.location.href=params.url;
    }

}
// Сортировка списка
