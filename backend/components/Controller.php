<?php
/**
 * Controller.php
 *
 * @author: Panarin S.S.
 * Date: 7/23/12
 * Time: 12:55 AM
 */
class Controller extends CController {

	public $breadcrumbs = array();
	public $menu = array();
    public $box_title = "Home";
    public $form_title;
    public $leftMenu;

    public function __construct($id,$module=null){
        parent::__construct($id,$module);

        $lang=Yii::app()->request->getParam('language');
        $lang_back_def=Langs::model()->find('"back_def"=true');
        if(empty($lang)){
            $lang=$lang_back_def->short;
            if(empty($lang))
                $lang='ru';
        }
        Yii::app()->setLanguage($lang);

        $idLang=Langs::model()->find('"short"=:short',array(':short'=>Yii::app()->getLanguage()))->id;
        if(!empty($idLang) AND !defined('lang_id')) define('lang_id',$idLang);
        if(!defined('lang_short')) define('lang_short',$lang);
        if(!defined('lang_url')){
            if($lang!=$lang_back_def->short){
                define('lang_url',$lang.'/');
            }else  define('lang_url','/');
        }
    }

    protected function beforeAction($action){

        if(!Yii::app()->user->checkAccess('root'))
            $this->redirect('/admin/login/');
        Yii::app()->clientScript->registerScriptFile('jquery-ui.min.js');
        Yii::app()->clientScript->registerScriptFile('jquery.js');

        return true;
    }

    /**
     * Выбросить ошибку и прекратить выполнение
     * @param string $params текст ошибки или масив с параметрами для обработки на фронтенде
     */
    protected function throwError($params, $action = 'noty_error')
    {
        if (is_array($params))
            $dat = array('result' => false, 'action'=>$action, 'params' =>  $params);
        else
            $dat = array('result' => false, 'action'=>$action, 'params' => array($params));

//        header('Content-type: text/x-json');
        echo json_encode($dat);
        Yii::app()->end();
    }

    /**
     * Завершить аджакс запрос успешно
     * @param string|array $params что то вернуть?
     * Если $info строка то в результирующий json будет добавлен info
     * Если $info массив или объект то он будет конвертироваться в json как есть
     */
    protected function throwSuccess($params = '',$action='noty_success')
    {
        $dat = array('result' => true, 'action'=>$action, 'params' => array());
        if (is_array($params)) {
            $dat['params'] = $params;
//            die(json_encode($dat));
        }
        if (is_string($params) && strlen($params))
            $dat['params'] = $params;

//        header('Content-type: text/x-json');
        echo json_encode($dat);
        Yii::app()->end();
    }
}
