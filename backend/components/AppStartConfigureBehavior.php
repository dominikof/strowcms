<?php

class AppStartConfigureBehavior extends CBehavior
{

	/**
	 * @return array the behavior events.
	 */
	public function events()
	{
        $arr=array_merge(parent::events(), array(
            'onBeginRequest'=>'registerScripts',
        ));
        return $arr;
	}

	public function init()
	{
		if (!isset($this->defaultLanguage))
			$this->defaultLanguage = $this->owner->sourceLanguage;
	}

    public function registerScripts(){
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile('admin.css',CClientScript::POS_HEAD);
        $cs->registerScriptFile('jquery.js',CClientScript::POS_HEAD);
        $cs->registerScriptFile('jquery-ui.min.js',CClientScript::POS_END);
        $cs->registerScriptFile('admin_modal.js',CClientScript::POS_END);
        $cs->registerScriptFile('admin_notification.js',CClientScript::POS_END);
        $cs->registerScriptFile('admin_js_lib.js',CClientScript::POS_END);
        $cs->registerScriptFile('/admin/js/cms/init.js',CClientScript::POS_END);
        $cs->registerScriptFile('jquery.noty.js',CClientScript::POS_END);
        $cs->registerScriptFile('top.js',CClientScript::POS_END);
        $cs->registerScriptFile('topRight.js',CClientScript::POS_END);
        $cs->registerScriptFile('default.js',CClientScript::POS_END);
        $cs->registerScriptFile('JqueryFormPlugin.js',CClientScript::POS_END);
        $cs->registerCssFile('jquery-ui.css',CClientScript::POS_END);
    }

}
