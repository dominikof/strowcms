<?php
/**
 * User: Panarin S.S.
 * Date: 29.11.12
 * Time: 21:52
 *
 * Form to upload images in admin for any module.
 */
class UploadForm extends CWidget
{

    public $moduleId=0;
    public $positionId=0;
    public $formId=1;

    public function run(){
        Yii::import( "uploader.extensions.xupload.models.XUploadForm" );
        $model = new XUploadForm;
        CWidget::render('_form_tab_img',array('model'=>$model,'moduleId'=>$this->moduleId,'positionId'=>$this->positionId,'formId'=>$this->formId));
    }
}
