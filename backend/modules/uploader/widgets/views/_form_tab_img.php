<div style="margin-left: 35px">
<?php

$this->widget( 'uploader.extensions.xupload.XUpload', array(
        'url' => Yii::app( )->createUrl( "/uploader/upload/uploadfile?moduleId=".$moduleId."&positionId=".$positionId."&formId=".$formId),
        //our XUploadForm
        'model' => $model,
        //We set this for the widget to be able to target our own form
        'htmlOptions' => array('id'=>'modalEditFormId'),
//        'htmlOptions' => array('id'=>'uploaderForm'.$formId),
        'attribute' => 'file',
        'multiple' => true,
        //Note that we are using a custom view for our widget
        //Thats becase the default widget includes the 'form'
        //which we don't want here
        'formView' => 'uploader.widgets.views.form',
        'autoUpload' => true,
    )
);
?>
</div>