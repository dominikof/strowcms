<?php
/**
 * User: Panargin S.S.
 * Date: 04.12.12
 * Time: 22:22
 *
 * Show gallery for UploaderImages widget
 */
?>
<?php if(count($data)>0):?>
<div class="show-btn-panel">
    <input onclick="$('#tableImageId<?php echo $form_id?> input[name*=\'check\']').attr('checked', this.checked);" type="checkbox" value="1" name="selectAll"
           id="selectAll">
    <a href='<?php echo Yii::app()->baseUrl?>/uploader/upload/remove/'
       class="btn btn-danger delete-all-selected-image"
       oktext="<?php echo Yii::t('uploader', 'delete selected')?>"
       notext="<?php echo Yii::t('uploader', 'cancel')?>"
       msgtext="<?php echo Yii::t('uploader', 'Do you really want delete all selected images')?>"
    >
        <i class="icon-trash icon-white"></i>
        <span>Delete</span>
    </a>

</div>

<table id='tableImageId<?php echo $form_id?>' class="table table-striped table-images sortable-image-table" data-order-url="<?php echo Yii::app()->baseUrl?>/uploader/upload/order/?form_id=<?php echo $form_id?>">
    <thead>

    </thead>
    <tbody>
    <?php foreach ($data as $row) : ?>

    <tr class="tr-sortable">
        <td class="text-center table-check-column">
            <div class="move_zone">
                <i class="icon-list"></i>
            </div>
            <?php
            echo Chtml::hiddenField('order[]', $row->id);
            echo Chtml::checkBox('check[]', false, array('value' => $row->id));

            ?>

        </td>
        <td><img class="thumbnail" src="<?php echo ImageResizer::resize($row->source, 100, 100)?>" alt="<?php echo $row->file_name?>">
        </td>
        <td><?php echo $row->source ?></td>
        <td>
            <a href="<?php echo Yii::app()->baseUrl?>/uploader/upload/delete/<?php echo $row->id?>"
               class="btn btn-danger del-image-btn"
               oktext="<?php echo Yii::t('uploader', 'delete')?>"
               notext="<?php echo Yii::t('uploader', 'cancel')?>"
               msgtext="<?php echo Yii::t('uploader', 'Do you really want delete this image')?>"
                    >
                <i class="icon-trash icon-white"></i>
            </a>
        </td>

    </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php endif;?>