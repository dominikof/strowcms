<?php
/**
 * User: Panargin S.S.
 * Date: 04.12.12
 * Time: 22:13
 */
class UploadedImages extends CWidget{

    public $module_id;
    public $position_id;
    public $form_id=1;

    public function run(){
        $model=new ModUploaderAdmin();
        Yii::app() -> clientScript -> registerScriptFile(
            CHtml::asset(Yii::app()->basePath . '/modules/uploader/assets/uploadImageEdit.js'),
            CClientScript::POS_END
        );
        $data=$model->findAll(
            array('condition'=>'"module_id"=:module_id AND "position_id"=:position_id AND form_id=:form_id',
                'order'=>'"order"',
                'params'=> array(':module_id'=>$this->module_id,':position_id'=>$this->position_id,':form_id'=>$this->form_id)
            )
        );
        CWidget::render('uploaded_images',array('data'=>$data,'module_id'=>$this->module_id,'position_id'=>$this->position_id,'form_id'=>$this->form_id));
    }

}