<?php

class UploadController extends Controller
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('uploadfile','delete','remove','order'),
                'users' => array('root'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    private function deleteOldFiles($path){
        $dir = @opendir($path);
        while (($file = readdir($dir)) !== false) {
            // текущее время
            if ($file != "." && $file != ".." && $file != ".htaccess") {
                $time_sec = time();
                // время изменения файла
                $time_file = filemtime($path . $file);
                // тепрь узнаем сколько прошло времени (в секундах)
                $time = $time_sec - $time_file;
                if ($time > 1800)
                    unlink($path . $file);
            }
        }
    }

    /**
     * make upload files to tmp directory through xupload extension
     * @throws CHttpException
     *
     */
    public function actionUploadfile()
	{

        Yii::import( "uploader.extensions.xupload.models.XUploadForm" );
        //Here we define the paths where the files will be stored temporarily
        $path = $_SERVER['DOCUMENT_ROOT']."/media/images/tmp/";
        $publicPath = "/media/images/tmp/";
        $this->deleteOldFiles($path);
        //This is for IE which doens't handle 'Content-type: application/json' correctly
        header( 'Vary: Accept' );
        if( isset( $_SERVER['HTTP_ACCEPT'] )
            && (strpos( $_SERVER['HTTP_ACCEPT'], 'application/json' ) !== false) ) {
            header( 'Content-type: application/json' );
        } else {
            header( 'Content-type: text/plain' );
        }

        $moduleId=Yii::app()->request->getParam('moduleId');
        $positionId=Yii::app()->request->getParam('positionId');
        $formId=Yii::app()->request->getParam('formId');

        //Here we check if we are deleting and uploaded file
        if( isset( $_GET["_method"] ) ) {
            if( $_GET["_method"] == "delete" ) {
                if( $_GET["file"][0] !== '.' ) {
                    $file = $path.$_GET["file"];
                    if( is_file( $file ) ) {
                        unlink( $file );
                    }
                }
                echo json_encode( true );
            }
        } else {
            $modelAdmin=new ModUploaderAdmin();
            $model = new XUploadForm;
            $model->file = CUploadedFile::getInstance( $model, 'file' );
            //We check that the file was successfully uploaded
            if( $model->file !== null ) {
                //Grab some data
                $model->mime_type = $model->file->getType( );
                $model->size = $model->file->getSize( );
                $model->name = $model->file->getName( );
                //(optional) Generate a random name for our file
                $filename = md5( Yii::app( )->user->id.microtime( ).$model->name);
                $ext=$model->file->getExtensionName( );
                $filename .= ".".$ext;
                if( $model->validate( ) AND $modelAdmin->checkExtensionImage($ext)) {
                    //Move our file to our temporary dir
                    $model->file->saveAs( $path.$filename );
                    chmod( $path.$filename, 0777 );
                    //here you can also generate the image versions you need
                    //using something like PHPThumb


                    //Now we need to save this path to the user's session
                    if( Yii::app( )->user->hasState( "images[$moduleId][$positionId][$formId]" ) ) {
                        $userImages = Yii::app( )->user->getState( "images[$moduleId][$positionId][$formId]" );
                    } else {
                        $userImages = array();
                    }
                    $userImages[] = array(
                        "path" => $path.$filename,
                        //the same file or a thumb version that you generated
                        "thumb" => $path.$filename,
                        "filename" => $filename,
                        'size' => $model->size,
                        'mime' => $model->mime_type,
                        'name' => $model->name,
                    );
                    Yii::app()->user->setState( "images[$moduleId][$positionId][$formId]", $userImages );

                    //Now we need to tell our widget that the upload was succesfull
                    //We do so, using the json structure defined in
                    // https://github.com/blueimp/jQuery-File-Upload/wiki/Setup
                    echo json_encode( array( array(
                        "name" => $model->name,
                        "type" => $model->mime_type,
                        "size" => $model->size,
                        "url" => $publicPath.$filename,
                        "thumbnail_url" => $publicPath."$filename",
                        "delete_url" => $this->createUrl( "Uploadfile", array(
                            "_method" => "delete",
                            "file" => $filename
                        ) ),
                        "delete_type" => "POST"
                    ) ) );
                } else {
                    //If the upload failed for some reason we log some data and let the widget know
                    $uploaderErro=$modelAdmin->getError('file');
                    echo json_encode( array(
                        array( "error" => (!empty($uploaderErro))?$uploaderErro:$model->getErrors( 'file' ),
                        ) ) );
                    Yii::log( "XUploadAction: ".CVarDumper::dumpAsString( $model->getErrors( ) ),
                        CLogger::LEVEL_ERROR, "xupload.actions.XUploadAction"
                    );
                }
            } else {
                throw new CHttpException( 500, "Could not upload file" );
            }
        }
	}


    public function actionDelete($id){
        $modelImage=$this->loadModel($id);
        $uploaderAdminModel=new ModUploaderAdmin();
        if($uploaderAdminModel->removeImage($modelImage)){
            $this->throwSuccess(array('msg'=>Yii::t('uploader','Image successful delete'),"delOk"=>true),'del_image_action');
        }else{
            $this->throwError(array(Yii::t('uploader',"Can't delete image.")));
        }
    }

    public function actionOrder(){
        $orderArr=Yii::app()->request->getPost('order');
        if(is_array($orderArr)){
            $uploaderAdminModel=new ModUploaderAdmin();
            $uploaderAdminModel->saveOrder($orderArr);
        }
    }

    public function actionRemove(){
        $checkArr=Yii::app()->request->getPost('check');
        if(is_array($checkArr)){
            $uploaderAdminModel=new ModUploaderAdmin();
            $imagesToDelete=$uploaderAdminModel->findAll('"id" IN ('.implode(',',$checkArr).')');
            $deletedArr=array();
            $notDeleted=array('53');
            foreach ($imagesToDelete as $imgDel) {
                if($uploaderAdminModel->removeImage($imgDel)){
                    $deletedArr[]=$imgDel->id;
                }else{
                    $notDeleted[]=$imgDel->id;
                }
            }
            $this->throwSuccess(array('msg'=>Yii::t('uploader','Images successful delete'),"deleteErrMsg"=>Yii::t('uploader','Delete image error'),"deleted"=>$deletedArr,"notDeleted"=>$notDeleted),'remove_images_action');
        }
    }

    public function loadModel($id){
        $model = ModUploaderImg::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }



}