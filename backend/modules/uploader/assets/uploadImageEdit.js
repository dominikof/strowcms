/**
 * User: Panarin S.S.
 * Date: 05.12.12
 * Time: 11:06
 */
$(document).ready(function(){
    init_image_del_link();
    make_image_sortable();
    init_remove_images();
});

function init_image_del_link(func){
    if (func)
        $(".del-image-btn").click(func);
    else {
        $(".del-image-btn").click(function (event) {
            event.preventDefault();
            url = $(this).attr('href');
            oktext= $(this).attr('oktext');
            notext= $(this).attr('notext');
            msgtext= $(this).attr('msgtext');
            self = $(this);
            admin_notification.showNotyAnswer(msgtext,oktext,notext,function(){
                cmsAjaxSend(url, "POST","",beforeSubmitStandart,function(responseText){
                    if(ajaxResponse(responseText)){
                        self.parents('tr').fadeTo('fast',0,function(){
                            $(this).remove();
                        });
                    }
                });
            });
        });
    }
}

function init_remove_images(){
    $(".delete-all-selected-image").click(function (event) {
        event.preventDefault();
        url = $(this).attr('href');
        oktext= $(this).attr('oktext');
        notext= $(this).attr('notext');
        msgtext= $(this).attr('msgtext');
        admin_notification.showNotyAnswer(msgtext,oktext,notext,function(){
            cmsAjaxSend(url, "POST",$('input[type="checkbox"][name*="check"]:checked').serialize(),beforeSubmitStandart,function(responseText){
                ajaxResponse(responseText);
            });
        });
    });
}

actions.del_image_action=function(params){
    if(params.delOk){
        admin_notification.showNotySuccess(params.msg);
        return true;
    }else{
        admin_notification.showNotyError(params);
        return true;
    }
}
actions.remove_images_action=function(params){
    admin_notification.showNotySuccess(params.msg);
    for(key in params.deleted){
        $('input[type="checkbox"][name*="check"][value="'+params.deleted[key]+'"]').parents('tr').fadeTo('fast',0,function(){
            $(this).remove();
        });
    }
    for(key in params.notDeleted){
        $('input[type="checkbox"][name*="check"][value="'+params.notDeleted[key]+'"]')
            .parents('tr')
            .children('td')
            .eq(2)
            .append('<br/><span class="label label-important">'+params.deleteErrMsg+'</span>')
    }
}


function make_image_sortable(){
    $('.sortable-image-table').sortable({
        items:             "tr.tr-sortable",
        tolerance:         "pointer",
        handle:            ".move_zone",
        scrollSensitivity: 40,
        opacity:           0.7,

        helper: function(event, ui){

            //width hack
            if($(ui[0]).find('input[type="checkbox"][name*="check"]').attr('checked')!='checked'){
                $('input[type="checkbox"][name*="check"]').attr('checked', false);
            }

            ui.children().each(function() {
                $(this).width($(this).width());
            });
            //group sorting

            if($('input[type="checkbox"][name*="check"]:checked').size()<1) return ui;
            var helper = $('<tr/>');
            $('input[type="checkbox"][name*="check"]:checked').each(function(){
                var item = $(this).closest('tr.tr-sortable');
                helper.height(helper.height()+item.innerHeight());
                if(item[0]!=ui[0]) {
                    helper.append(item.clone());
                    $(this).closest('tr.tr-sortable').remove();
                }
                else {
                    helper.append(ui.clone());
                    item.find('input[type="checkbox"][name*="check"]').attr('checked', false);
                }
            });
            return helper;
        },
        remove:function(event, ui){
            this.cancel();
        },
        beforeStop:function(event, ui){
            if(ui.helper.children('tr.tr-sortable').size()>0){
                ui.helper.children('tr.tr-sortable').each(function(){
                    $(this).insertBefore(ui.item);
                });
                ui.item.remove();
            }
        },
        start:function(event, ui){
            ui.placeholder.height(ui.helper.height());
        },
        update:function(event, ui)
        {
            url=$(".sortable-image-table").attr('data-order-url');
            cmsAjaxSend(url, "POST",$('input[type="hidden"][name*="order"]').serialize(),beforeSubmitStandart,endAjaxAnimation);
            $(".sortable-image-table input[name*='check']").attr('checked', false);
        }
    });
}