<?php
/**
 * User: Panarin S.S.
 * Date: 29.11.12
 * Time: 23:44
 * model for pages logic in backend part
 */
class ModUploaderAdmin extends ModUploaderImg{


    private $saveFolder="";
    private $saveImages=array('jpg','jpeg','gif','png');

    /**
     * set save folder
     */
    public function setSaveFolder($saveFolder){
        if(empty($saveFolder)) $saveFolder="/media/tmp/";
        if($saveFolder[strlen($saveFolder)-1]!='/') $saveFolder.='/';
        $this->saveFolder=$saveFolder;
        return $this;
    }

    public function checkExtensionImage($ext){
        foreach ($this->saveImages as $imgExt) {
            if($imgExt==$ext) return true;
        }
        $this->addError('file',Yii::t('uploader','File extension not allowed for uploading.'));
        return false;
    }

    /**
     * check folders to save and create if not exist
     */
    private function checkSaveFolderExist(){
        $webroot=$_SERVER['DOCUMENT_ROOT'];
        $saveFoldersArr=explode('/',$this->saveFolder);
        foreach ($saveFoldersArr as $folder) {
            if(empty($folder)) continue;
            $webroot.="/".$folder;
            if(!is_dir($webroot)){
                mkdir($webroot);
                chmod( $webroot, 0777 );
            }
        }

    }


    /**
     * Save uploaded files and move to save directory
     */
    public function moveAndSaveFiles($module_id,$item_id,$formId=1,$new=false){
        $this->checkSaveFolderExist();
        //If we have pending images
        if($new)
            $postionId=0;
        else
            $postionId=$item_id;
        if( Yii::app( )->user->hasState( "images[$module_id][$postionId][$formId]" ) ) {
            $userImages = Yii::app( )->user->getState( "images[$module_id][$postionId][$formId]" );
            //Resolve the final path for our images
            $pathRoot = $_SERVER['DOCUMENT_ROOT'].$this->saveFolder.$item_id.'/';
            $path = $this->saveFolder.$item_id.'/';
            //Create the folder and give permissions if it doesnt exists
            if( !is_dir( $pathRoot ) ) {
                mkdir( $pathRoot );
                chmod( $pathRoot, 0777 );
            }

            //Now lets create the corresponding models and move the files
            foreach( $userImages as $image ) {
                if( is_file( $image["path"] ) ) {
                    if( rename( $image["path"], $pathRoot.$image["filename"] ) ) {
                        chmod( $pathRoot.$image["filename"], 0777 );
                        $imgUpload=new ModUploaderImg;
//                        $imgUpload->setIsNewRecord(true);
                        $imgUpload->module_id=$module_id;
                        $order=$this->findMaxOrder($module_id,$item_id);
                        $imgUpload->order=$order+1;
                        $imgUpload->position_id=$item_id;
                        $imgUpload->file_name = $image["name"];
                        $imgUpload->source = $path.$image["filename"];
                        $imgUpload->form_id = $formId;
                        if(!$imgUpload->validate() OR !$imgUpload->save() ) {
                            //Its always good to log something
                            Yii::log( "Could not save Image:\n".CVarDumper::dumpAsString(
                                $imgUpload->getErrors( ) ), CLogger::LEVEL_ERROR );
                            //this exception will rollback the transaction
                            throw new Exception( 'Could not save Image');

                        }
                    }
                } else {
                    //You can also throw an execption here to rollback the transaction
                    Yii::log( $image["path"]." is not a file", CLogger::LEVEL_WARNING );
                }
            }
            //Clear the user's session
            Yii::app()->user->setState( 'images', null );
        }
    }

    public function findMaxOrder($module_id,$item_id){
        $maxOrder=Yii::app()->db->createCommand()
            ->select('max("order") AS "maxOrder"')
            ->from($this->tableName())
            ->where('"module_id"='.$module_id.' AND "position_id"='.$item_id.'')
            ->queryScalar();
        return $maxOrder;
    }

    public function deleteForModelAndPosition($module_id,$position_id){
        $images=$this->model()->findAll('"module_id"=:module_id AND "position_id"=:position_id',array(":module_id"=>$module_id,":position_id"=>$position_id));
        $webroot=$_SERVER['DOCUMENT_ROOT'];
        foreach ($images as $image) {
            $this->removeImage($image);
        }
        if(isset($image)){
            $webroot=$_SERVER['DOCUMENT_ROOT'];
            $filePath=pathinfo($image->source, PATHINFO_DIRNAME);
            rmdir($webroot.$filePath);
        }
    }

    public function removeImage($modelImage){
        $webroot=$_SERVER['DOCUMENT_ROOT'];

        $filePath=pathinfo($modelImage->source, PATHINFO_DIRNAME);
        $fileName=pathinfo($modelImage->source, PATHINFO_BASENAME);
        $delMask=$webroot.$filePath.'/*'.$fileName;
        if( $modelImage->delete()){
            array_map( "unlink", glob( $delMask ) );
            return true;
        }else{
            return false;
        }
    }

    public function saveOrder($arr)
    {
        $connection = Yii::app()->db;
        $form_id=Yii::app()->request->getParam('form_id');
        $transaction = $connection->beginTransaction();
        try {
            $q = "UPDATE \"" . $this->tableName() . "\" SET \"order\"=:order WHERE \"id\"=:id AND \"form_id\"=:form_id";
            $command = Yii::app()->db->createCommand($q);
            foreach ($arr as $order => $id) {
                $command->bindParam(':order', $order, PDO::PARAM_INT);
                $command->bindParam(':id', $id, PDO::PARAM_INT);
                $command->bindParam(':form_id', $form_id, PDO::PARAM_INT);
                $command->execute();
            }
            $transaction->commit();
        } catch (Exception $e) // в случае возникновения ошибки при выполнении одного из запросов выбрасывается исключение
        {
            $transaction->rollback();
        }

    }

}