<?php
/**
 * User: Panargin S.S.
 * Date: 17.11.12
 * Time: 16:27
 */

?>
<!-- start: Main Menu -->
<div class="span2 main-menu-span">
    <div class="nav-collapse sidebar-nav">
        <ul class="nav nav-tabs nav-stacked main-menu">
            <li>
                <a class="dropmenu" href="#">
                    <i class="icon-plus icon-white"></i><span class="hidden-tablet"><?php echo Yii::t('main','Add')?>...</span>
                </a>
                <ul>
                    <li><a href="<?php echo Yii::app()->createUrl('auth/role/create') ?> " class="submenu"><i class="icon-plus icon-white"></i><span class="hidden-tablet"> <?php echo Yii::t('AuthModule.main','New role');?></span></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl('auth/task/create') ?> " class="submenu"><i class="icon-plus icon-white"></i><span class="hidden-tablet"> <?php echo Yii::t('AuthModule.main','New task');?></span></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl('auth/operation/create') ?>" v><i class="icon-plus icon-white"></i><span class="hidden-tablet"> <?php echo Yii::t('AuthModule.main','New operation');?></span></a></li>
                </ul>
            </li>
            <li>
                <a class="dropmenu" href="#">
                    <i class="icon-list-alt icon-white"></i><span class="hidden-tablet"><?php echo Yii::t('main','Show')?>...</span>
                </a>
                <ul>
                    <li><a href="<?php echo Yii::app()->createUrl('auth/assignment/index') ?> " class="submenu"><i class="icon-list-alt icon-white"></i><span class="hidden-tablet"> <?php echo Yii::t('AuthModule.main','Assignments');?></span></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl('auth/role/index') ?> " class="submenu"><i class="icon-list-alt icon-white"></i><span class="hidden-tablet"> <?php echo $this->capitalize(Yii::t('AuthModule.main','role|roles',2));?></span></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl('auth/task/index') ?> " class="submenu"><i class="icon-list-alt icon-white"></i><span class="hidden-tablet"> <?php echo $this->capitalize(Yii::t('AuthModule.main','task|tasks',2));?></span></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl('auth/operation/index') ?>" class="submenu"><i class="icon-list-alt icon-white"></i><span class="hidden-tablet"> <?php echo $this->capitalize(Yii::t('AuthModule.main','operation|operations',2));?></span></a></li>
                </ul>
            </li>

            <li><a href="<?php echo Yii::app()->createUrl('/')?>"><i class="icon-home icon-white"></i><span class="hidden-tablet"> Dashboard</span></a></li>
        </ul>
    </div><!--/.well -->
</div><!--/span-->
<!-- end: Main Menu -->