<div class="box span6">
    <div class="box-header">
        <h2><i class="icon-align-justify"></i><span class="break"></span><?php echo Yii::t('modNews', 'Last news'); ?></h2>

        <div class="box-icon">
            <a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
        </div>
    </div>
    <div class="box-content">
        <table class="table">
            <thead>
            <tr>
                <th><?php echo Yii::t('modNews', 'News title'); ?></th>
                <th><?php echo Yii::t('main', 'Category'); ?></th>
                <th colspan="2" class="center"><?php echo Yii::t('main', 'Date'); ?></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data as $news): ?>
            <tr>
                <td>
                    <a href="<?php echo Yii::app()->createUrl('news/records/edit/'.$news->id)?>" title="<?php echo $news->texts['name'] ?>">
                        <?php echo $news->texts['name'] ?>
                    </a>
                </td>
                <td class="center"><?php echo $news->category->categoryTexts['name'] ?></td>
                <td class="align-right">
                    <b>
                        <?php echo Yii::t('modNews','Begin')?>:
                    </b>
                    <br/>
                    <b>
                        <?php echo Yii::t('main','End')?>:
                    </b>
                </td>
                <td >

                    <?php
                    echo $news->start_date;
                    ?>
                    <br/>
                    <?php
                    echo $news->end_date;
                    ?>
                </td>

            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>