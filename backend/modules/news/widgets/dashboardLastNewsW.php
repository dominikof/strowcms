<?php
/**
 * User: Panarin S.S.
 * Date: 10.02.13
 * Time: 17:16
 */
class dashboardLastNewsW extends CWidget
{


    public function run(){
        Yii::import('common.modules.news.models.*');

        $data=$this->getData();
        CWidget::render('dashboardLastNewsW',array('data'=>$data));
    }

    public function getData(){
        $dataProvider = new CActiveDataProvider('ModNews', array(
            'pagination'=>false,
            'criteria' => array(
                'order' => '"t"."id" DESC',
                'limit' => 4,
                'with' => array(
                    'category' => array(
                        'joinType' => 'LEFT JOIN',
                        'with'=>array(
                            'categoryTexts' => array(
                                'joinType' => 'LEFT JOIN',
                                'on' => '"categoryTexts"."lang_id"='.lang_id.'',
                            ),
                        ),
                    ),
                    'texts' => array(
                        'joinType' => 'LEFT JOIN',
                        'on' => '"texts"."lang_id"='.lang_id.'',
                    ),
                ),
                'together' => false,
            ),
        ));

        return $dataProvider->getData();
    }
}
