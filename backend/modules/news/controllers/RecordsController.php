<?php

class RecordsController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */

    protected $perPage;
    protected $moduleId;


    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('delete', 'index', 'view', 'create', 'edit', 'remove', 'edit','order','filter'),
                'users' => array('root'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    protected function beforeAction($action){
        $moduleObj=Yii::app()->getModule('news');
        $this->perPage=$moduleObj->perPage;
        $this->moduleId=$moduleObj->moduleId;

        return parent::beforeAction($action);
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/cms/modules/show.js');
        $model = new ModNewsAdmin();
        $category_id = Yii::app()->request->getParam('category', 0);
        $dataProvider = $model->getNewsByPages($this->moduleId,$category_id);
        $newsCategories=$model->getAllCategories();

        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'category_id' => $category_id,
                'newsCategories' => $newsCategories,
        ));
    }




    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {

        $model = new ModNewsAdmin();
        $modelTexts = new ModNewsTexts();
        $transaction = Yii::app()->db->beginTransaction();
        if (isset($_POST['news']) AND isset($_POST['newstexts'])) {

                $order=$model->findMaxOrder($_POST['news']['category_id'],'category_id');
                $tmp = $_POST['news'];
                $tmp['order']=$order+1;
                $model->attributes=$tmp;

                $modelTexts->attributesM = $_POST['newstexts'];

                if (!$model->validate() OR !$model->save()) {
                    $transaction->rollBack();
                    $this->throwError($model->errors);
                }

                $urlManager=new MultiLanguageUrlManager();
                if(!$urlManager->saveUrl($this->moduleId,$model->id,$_POST['newstexts'],'name',$model->category_id,false,true)){
                    $transaction->rollBack();
                    $this->throwError(Yii::t('modNews','Error while save url for news!'));
                }

                if(!$modelTexts->multiSave($model->id,'news_id')){
                    $transaction->rollBack();
                    $this->throwError($modelTexts->errors);
                }


            $ImageModule=new ModUploaderAdmin();
            $ImageModule->setSaveFolder('/media/images/uploads/mod_news/');
            $ImageModule->moveAndSaveFiles($this->moduleId,$model->id,1,true);

            $transaction->commit();
            if($_POST['btn-submitted']=='return')
                $url=Yii::app()->createUrl('news/records/edit/'.$model->id);
            else $url=Yii::app()->createUrl('news');
            $this->throwSuccess(array('url'=>$url),'redirect');
            Yii::app()->end();
        }


        $images=$this->widget('uploader.widgets.UploadForm',array('moduleId'=>$this->moduleId,'positionId'=>0,'formId'=>1),true);
        $newsCategories=$model->getAllCategories();
        $this->render('create', array(
            'model' => $model,
            'modelTexts' => $modelTexts,
            'images' => $images,
            'newsCategories' => $newsCategories,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdit($id)
    {
        $model = $this->loadModel($id);
        $modelTexts = ModNewsTexts::model()->getDataForEdit($id,'news_id');
        $urlManager=new MultiLanguageUrlManager();

        $transaction = Yii::app()->db->beginTransaction();
        if (isset($_POST['news']) OR isset($_POST['newstexts'])) {
            if(isset($_POST['news'])){
//                CVarDumper::dump($_POST['news']);die;
                $model->attributes=$_POST['news'];
            }
            $modelTextsSave=new ModNewsTexts;
            if(isset($_POST['newstexts']))
                $modelTextsSave->attributesM = $_POST['newstexts'];

            if (!$model->validate() OR !$model->save()) {
                $transaction->rollBack();
                $this->throwError($model->errors);
            }

            $urlManager=new MultiLanguageUrlManager();
            if(!$urlManager->saveUrl($this->moduleId,$model->id,$_POST['newstexts'],'name',$model->category_id,false,true)){
                $transaction->rollBack();
                $this->throwError(Yii::t('modPages','Error while save url for news!'));
            }

            if(!$modelTextsSave->multiSave($model->id,'news_id')){
                $transaction->rollBack();
                $this->throwError($modelTextsSave->errors);
            }


            $ImageModule=new ModUploaderAdmin();
            $ImageModule->setSaveFolder('/media/images/uploads/mod_news/');
            $ImageModule->moveAndSaveFiles($this->moduleId,$model->id,1);

            $transaction->commit();
            if($_POST['btn-submitted']=='return')
                $url=Yii::app()->baseUrl."/news/records/edit/".$model->id;
            else $url=Yii::app()->baseUrl."/news";
            $this->throwSuccess(array('url'=>$url),'redirect');
            Yii::app()->end();
        }
        $images=$this->widget('uploader.widgets.UploadForm',array('moduleId'=>$this->moduleId,'positionId'=>$model->id,'formId'=>1),true);
        $uploadedImages=$this->widget('uploader.widgets.UploadedImages',array('module_id'=>$this->moduleId,'position_id'=>$model->id,'form_id'=>1),true);
        $urlManager->getUrlMultiLangRecord($this->moduleId,$model->id,false,$modelTexts);
        $newsCategories=ModNewsAdmin::model('ModNewsAdmin')->getAllCategories();
        $this->render('edit', array(
            'model' => $model,
            'modelTexts' => $modelTexts,
            'images' => $images,
            'uploadedImages' => $uploadedImages,
            'newsCategories' => $newsCategories,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $modelPagesAdmin=new ModNewsAdmin();
        $modelPagesAdmin->deleteNews($id);
        $this->ajaxThrowContent();
    }

    /**
     * Remove set of items
     */
    public function actionRemove()
    {
        $newsToDelete=Yii::app()->request->getParam('check',array());
        if(count($newsToDelete)>0){
            $modelNewsAdmin=new ModNewsAdmin();
            $modelNewsAdmin->deleteNews($newsToDelete);
            $this->ajaxThrowContent();
        }

    }

    public function actionOrder(){
        $orderArr=Yii::app()->request->getPost('order');
        $modelPagesAdmin=new ModNewsAdmin();
        $modelPagesAdmin->getNewsByPages($this->moduleId);
        if(is_array($orderArr)){
            $pageAdminModel=new ModNewsAdmin();
            $pageAdminModel->saveOrder($orderArr);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = ModNewsAdmin::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * filtering news
     */
    public function actionFilter(){
        $this->ajaxThrowContent();
    }


    private function ajaxThrowContent()
    {
        $model = new ModNewsAdmin();
        $category_id = Yii::app()->request->getParam('category', 0);
        $dataProvider = $model->getNewsByPages($this->moduleId,$category_id);
        $newsCategories=$model->getAllCategories();

        $render=$this->renderPartial('index', array(
            'dataProvider' => $dataProvider,
            'category_id' => $category_id,
            'newsCategories' => $newsCategories,
        ),true);
        $this->throwSuccess(array('content' => $render, 'div' => 'mainContentID'));
    }


}
