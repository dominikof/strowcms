<?php

class CategoriesController extends Controller
{

    protected $perPage;
    protected $categoriesPerPage;
    protected $moduleId;
    

    protected function beforeAction($action){
        $moduleObj=Yii::app()->getModule('news');
        $this->perPage=$moduleObj->perPage;
        $this->categoriesPerPage=$moduleObj->categoriesPerPage;
        $this->moduleId=$moduleObj->moduleId;

        return parent::beforeAction($action);
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('delete', 'index', 'view', 'create', 'edit', 'remove', 'edit','order'),
                'users' => array('root'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/cms/modules/show.js');
        $model = new ModNewsCategoryAdmin();
        $dataProvider = $model->getCategoriesBypages($this->moduleId);
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {

        $model = new ModNewsCategoryAdmin();
        $modelTexts = new ModNewsCategoryTexts();
        $urlManager=new MultiLanguageUrlManager();

        $transaction = Yii::app()->db->beginTransaction();
        if (isset($_POST['newsCategs']) AND isset($_POST['newsCategstexts'])) {

                $order=$model->findMaxOrder();
                $tmp = $_POST['newsCategs'];
                $tmp['order']=$order+1;
                $model->attributes=$tmp;

                $modelTexts->attributesM = $_POST['newsCategstexts'];

                if (!$model->validate() OR !$model->save()) {
                    $transaction->rollBack();
                    $this->throwError($model->errors);
                }

                $urlManager=new MultiLanguageUrlManager();
                if(!$urlManager->saveUrl($this->moduleId,$model->id,$_POST['newsCategstexts'],'name',0,true)){
                    $transaction->rollBack();
                    $this->throwError(Yii::t('modNews','Error while save url for news category!'));
                }

                if(!$modelTexts->multiSave($model->id,'category_id')){
                    $transaction->rollBack();
                    $this->throwError($modelTexts->errors);
                }

            $transaction->commit();
            if($_POST['btn-submitted']=='return')
                $url=Yii::app()->createUrl('news/categories/edit/'.$model->id);
            else $url=Yii::app()->createUrl('news/categories');
            $this->throwSuccess(array('url'=>$url),'redirect');
            Yii::app()->end();
        }

        $urlManager->getUrlMultiLangRecord($this->moduleId,NULL,true,$modelTexts);
        $this->render('create', array(
            'model' => $model,
            'modelTexts' => $modelTexts,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdit($id)
    {

        $model = $this->loadModel($id);
        $modelTexts = ModNewsCategoryTexts::model()->getDataForEdit($id,'category_id');
        $urlManager=new MultiLanguageUrlManager();

        $transaction = Yii::app()->db->beginTransaction();
        if (isset($_POST['newsCategs']) OR isset($_POST['newsCategstexts'])) {
            if(isset($_POST['newsCategs'])){
                $model->attributes=$_POST['newsCategs'];
            }
            $modelTextsSave=new ModNewsCategoryTexts();
            if(isset($_POST['newsCategstexts']))
                $modelTextsSave->attributesM = $_POST['newsCategstexts'];

            if (!$model->validate() OR !$model->save()) {
                $transaction->rollBack();
                $this->throwError($model->errors);
            }

            $urlManager=new MultiLanguageUrlManager();
            if(!$urlManager->saveUrl($this->moduleId,$model->id,$_POST['newsCategstexts'],'name',0,true)){
                $transaction->rollBack();
                $this->throwError(Yii::t('modNews','Error while save url for category!'));
            }

            if(!$modelTextsSave->multiSave($model->id,'category_id')){
                $transaction->rollBack();
                $this->throwError($modelTextsSave->errors);
            }


            $transaction->commit();
            if($_POST['btn-submitted']=='return')
                $url=Yii::app()->createUrl('news/categories/edit/'.$model->id);
            else $url=Yii::app()->createUrl('news/categories');
            $this->throwSuccess(array('url'=>$url),'redirect');
            Yii::app()->end();
        }
        $urlManager->getUrlMultiLangRecord($this->moduleId,$model->id,true,$modelTexts);
        $this->render('edit', array(
            'model' => $model,
            'modelTexts' => $modelTexts,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $modelCategories=new ModNewsCategoryAdmin();
        $modelCategories->deleteCategory($id);
        $this->ajaxThrowContent();
    }

    /**
     * Remove set of items
     */
    public function actionRemove()
    {
        $categorisToDelete=Yii::app()->request->getParam('check',array());
        if(count($categorisToDelete)>0){
            $modelPagesAdmin=new ModNewsCategoryAdmin();
            $modelPagesAdmin->deleteCategory($categorisToDelete);
            $this->ajaxThrowContent();
        }

    }

    public function actionOrder(){
        $orderArr=Yii::app()->request->getPost('order');
        if(is_array($orderArr)){
            $pageAdminModel=new ModNewsCategoryAdmin();
            $pageAdminModel->saveOrder($orderArr);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = ModNewsCategory::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }


    private function ajaxThrowContent()
    {
        $model = new ModNewsCategoryAdmin();
        $this->moduleId=Modules::model()->getModuleIdByName('news');
        $dataProvider = $model->getCategoriesBypages($this->moduleId);
        $render=$this->renderPartial('index', array(
            'dataProvider' => $dataProvider,
        ),true);
        $this->throwSuccess(array('content' => $render, 'div' => 'mainContentID'));
    }


}
