<?php
/* @var $this PageController */
/* @var $model ModPages */
/* @var $model ModPagesTexts */

$this->breadcrumbs=array(
    Yii::t('modNews','News')=>Yii::app()->createUrl('news/records'),
    Yii::t('main','Edit'),
);
$this->leftMenu= $this->renderPartial('/menu/left',array(),true);

$this->box_title=Yii::t('modNews','Edit');
$this->form_title=Yii::t('modNews','Edit news ID:'.$model->id);
?>


<?php
$content= $this->renderPartial('_form',
    array(
        'model'=>$model,
        'modelTexts' => $modelTexts,
        'submitUrl'=>Yii::app()->createUrl('/news/records/edit/'.$model->id),
        'images'=>$images,
        'uploadedImages'=>$uploadedImages,
        'newsCategories' => $newsCategories,
    ),true);
$this->widget('box',array('boxTitle'=>$this->box_title,'boxContent'=>$content))
?>