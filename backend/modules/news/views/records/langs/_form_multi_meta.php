<?php
/**
 * User: domin_000
 * Date: 21.11.12
 * Time: 21:35
 */
?>
<div class="control-group">
    <label class="control-label">
        <?php echo Yii::t("modNews", "News title");?>
    </label>
    <div class="controls">
        <?php echo CHtml::textField("newstexts[$lang][mtitle]",$modelTexts->mtitle[$lang]); ?>
    </div>
</div>
<div class="control-group">
    <label class="control-label">
        <?php echo Yii::t("modNews", "News description");?>
    </label>
    <div class="controls">
        <?php echo CHtml::textArea("newstexts[$lang][mdescr]",$modelTexts->mdescr[$lang],array('class'=>'description-text-area')); ?>
    </div>
</div>