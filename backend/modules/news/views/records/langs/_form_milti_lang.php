<?php
/**
 * User: Panarin S.S.
 * Date: 18.11.12
 * Time: 14:54
 */
?>

<?php $this->widget('translitField',array('translit'=>$modelTexts->translit[$lang],'lang_id'=>$lang)); ?>
<div class="control-group">
    <label class="control-label">
        <?php echo Yii::t("modNews", "News title");?>
    </label>
    <div class="controls">
        <?php echo CHtml::textField("newstexts[$lang][name]",$modelTexts->name[$lang]); ?>
    </div>
</div>

<div class="control-group">
    <label class="control-label">
        <?php echo Yii::t("modNews", "News short");?>
    </label>
    <div class="controls">
        <?php $this->widget('tinymce', array('fld_name' => "newstexts[$lang][short]", 'lang' => 'en','content'=>$modelTexts->short[$lang])); ?>
    </div>
</div>
<div class="control-group">
    <label class="control-label">
        <?php echo Yii::t("modNews", "News full");?>
    </label>
    <div class="controls">
        <?php $this->widget('tinymce', array('fld_name' => "newstexts[$lang][full]", 'lang' => 'en','content'=>$modelTexts->full[$lang])); ?>
    </div>
</div>

