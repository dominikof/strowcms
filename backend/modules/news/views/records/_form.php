<?php
/* @var $this PageController */
/* @var $model ModPages */
/* @var $model ModPagesTexts */
?>
<legend>
    <?php
    echo $this->form_title;
    ?>
</legend>
<div class="inner-form-body">
    <?php
    echo CHtml::form($submitUrl, "POST",
        array('class' => 'form-horizontal ajax', 'id' => 'modalEditFormId', 'enctype' => 'multipart/form-data'));
    echo CHtml::hiddenField('submit', '1');
    echo CHtml::hiddenField('btn-submitted', 'go', array('id' => 'fromActionId'));
    ?>

    <?php
    $general = $this->renderPartial('_form_tab_main', array('modelTexts' => $modelTexts, 'model' => $model, 'newsCategories' => $newsCategories), true);
    $meta = $this->widget('multiLangsTabs', array(
        'modelTexts' => $modelTexts,
        'view' => 'application.modules.news.views.records.langs._form_multi_meta'), true);
    $imgTab = $uploadedImages . $images;
    $this->widget('bootstrap.widgets.TbTabs', array(
        'type' => 'tabs', // 'tabs' or 'pills'
        'tabs' => array(
            array('label' => Yii::t('main', 'General'), 'content' => $general, 'active' => true),
            array('label' => Yii::t('main', 'Meta'), 'content' => $meta),
            array('label' => Yii::t('main', 'Images'), 'content' => $imgTab),
        ),
    ));
    ?>

    <?php $this->renderPartial('//forms/save_btn_group', array('cancelUrl' => Yii::app()->createUrl(lang_url . 'news'), 'formId' => 'modalEditFormId')) ?>
    <?php
    echo CHtml::endForm();
    ?>
</div>