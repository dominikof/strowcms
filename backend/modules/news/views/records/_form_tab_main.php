<?php
/**
 * User: Aki
 * Date: 12.11.12
 * Time: 20:40
 */
?>
<div class="control-group">
    <label class="control-label" for="NewsVisibleId">
        <?php  echo Yii::t("modNews", "Visibility of the news");?>
    </label>
    <div class="controls">
        <?php
        echo CHtml::hiddenField('news[visible]', '0');
        echo CHtml::checkBox('news[visible]', $model->visible,array('id'=>'NewsVisibleId'));
        ?>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="NewsCategoryId">
        <?php echo Yii::t("modNews", "Category of the news"); ?>
    </label>
    <div class="controls">
        <?php
        $arrCategories=MYCHtml::listDataRelations($newsCategories, 'id', 'name','categoryTexts');
        echo  CHtml::dropDownList('news[category_id]', Yii::app()->request->getParam('category'), $arrCategories, array('id' => 'NewsCategoryId'));
        ?>
    </div>
</div>
<?php
    $this->widget('dateTimeStartEndFlds',array(
        'fld_start_name'=>'news[start_date]',
        'fld_end_name'=>'news[end_date]',
        'label_start'=>Yii::t('modNews','News start date'),
        'label_end'=>Yii::t('modNews','News end date'),
        'fld_start_value'=>$model->start_date,
        'fld_end_value'=>$model->end_date,
    ));
//    $this->widget('dateTimeFld',array('fld_name'=>'news[start_date]','label'=>Yii::t('modNews','News start date'),'fld_value'=>$model->start_date));
//    $this->widget('dateTimeFld',array('fld_name'=>'news[end_date]','label'=>Yii::t('modNews','News end date'),'fld_value'=>$model->end_date));
?>

<?php $this->widget('multiLangsTabs',array(
    'modelTexts'=>$modelTexts,
    'view'=>'application.modules.news.views.records.langs._form_milti_lang'
)); ?>
