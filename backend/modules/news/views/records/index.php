<?php
/* @var $this PagesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    Yii::t('modNews', 'News'),
);
$this->box_title = Yii::t('modNews', 'News');
$this->leftMenu = $this->renderPartial('/menu/left', array(), true);
?>

<?php
$htmlAfterRemoveBtn = '';
$this->widget('addRemoveBtns',
    array(
        'addUrl' => Yii::app()->createUrl('news/records/create/'),
        'removeUrl' => Yii::app()->createUrl('news/records/remove/'),
        'msgRemove' => Yii::t("modNews", "Do you really want to delete the selected news?"),
    ));
?>

<table class="table table-striped table-condensed sortable-table" data-order-url="<?php echo Yii::app()->createUrl('news/records/order/');?>">
    <thead>
    <tr>
        <th class="text-center "><?php echo CHtml::checkBox('selectAll', false, array('onclick' => "$('input[name*=\'check\']').attr('checked', this.checked);")) ?></th>
        <th class="span1">ID</th>
        <th class="span3" colspan="2"><?php echo Yii::t("modNews", 'New title / url'); ?></th>
        <th class="span3"><?php echo Yii::t("modNews", 'News category'); ?></th>
        <th class="span2"><?php echo Yii::t("modNews", 'Visibility'); ?></th>
        <th></th>
        <th class="span3"><?php echo Yii::t("main", 'Date'); ?></th>
        <th class="table-action-column span2"><?php echo Yii::t("main", 'Action'); ?></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td></td>
        <td>
            <div class="filter-container">
                <?php
                echo CHtml::hiddenField('filterUrl', Yii::app()->createUrl('news/records/filter'), array('id' => 'filterUrlId'));
                echo CHtml::textField('filter_id', Yii::app()->request->getParam('filter_id'), array('class' => 'filter'))
                ?>
            </div>
        </td>
        <td colspan="2">
            <div class="filter-container">
                <?php
                echo CHtml::textField('filter_name', Yii::app()->request->getParam('filter_name'), array('class' => 'filter'))
                ?>
            </div>
        </td>
        <td>
            <div class="filter-container">
                <?php
                $arrCategories=MYCHtml::listDataRelations($newsCategories, 'id', 'name','categoryTexts');
                $static=array('0'=>Yii::t('texts','All categories'));
                echo  CHtml::dropDownList('filter_category', Yii::app()->request->getParam('filter_category'), $static+$arrCategories, array('class' => 'filter filter-select'));
                ?>
            </div>
        </td>

        <td>
            <div class="filter-container">
                <?php
                echo CHtml::dropDownList('filter_status', Yii::app()->request->getParam('filter_status','-1'), array(
                    '0' => Yii::t('modNews', 'All'),
                    'true' => Yii::t('modNews', 'Visible'),
                    'false' => Yii::t('modNews', 'Not visible'),
                ), array('class' => 'filter-select filter'));
                ?>
            </div>
        </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <?php
    $data = $dataProvider->getData();
    foreach ($data as $key => $newsRecord):
        ?>
    <tr class='tr-sortable'>
        <td class="text-center">
            <!--            <div class="move_zone">-->
            <!--                <i class="icon-list"></i>-->
            <!--            </div>-->
            <?php
//            echo Chtml::hiddenField("order[$newsRecord->order]", $newsRecord->id);
            echo Chtml::checkBox('check[]', false, array('value' => $newsRecord->id));

            ?>

        </td>
        <td class="id-column">
            <?php echo  $newsRecord->id;?>
        </td>
        <td >
            <?php

            echo $newsRecord->texts['name'];
            ?>
            <br/>
            Url:
            <?php
            echo $newsRecord->url['translit'];
            ?>

        </td>
        <td>
            <?php
            if (isset($newsRecord->images[0]->source)):
                ?>
                <img class="thumbnail" src="<?php echo ImageResizer::resize($newsRecord->images[0]->source, 75, 75);?>"/>
                <?php
            endif;
            ?>
        </td>
        <td>
            <?php
            echo $newsRecord->category->categoryTexts['name'];
            ?>
        </td>

        <td>
            <i class="<?php echo ($newsRecord->visible) ? 'icon-ok' : 'icon-remove';?>"></i>
        </td>
        <td class="align-right">
            <b>
                <?php echo Yii::t('modNews', 'Begin')?>:
            </b>
            <br/>
            <b>
                <?php echo Yii::t('main', 'End')?>:
            </b>
        </td>
        <td>

            <?php
            echo $newsRecord->start_date;
            ?>
            <br/>
            <?php
            echo $newsRecord->end_date;
            ?>
        </td>

        <td class="table-action-column">
            <?php
            $this->widget('editRemoveBtnGroup', array(
                'editUrl' => Yii::app()->createUrl('news/records/edit/' . $newsRecord->id),
                'delUrl' => Yii::app()->createUrl('news/records/delete/' . $newsRecord->id),
                'delQuestion' => Yii::t("modNews", "Do you really want to delete the selected news?"),
            ));
            ?>
        </td>
    </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$this->widget('pagination', array('count' => $dataProvider->getTotalItemCount(), 'perPage' => $this->perPage, 'url' => Yii::app()->createUrl('news')))
?>