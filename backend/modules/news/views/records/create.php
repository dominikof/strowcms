<?php
/* @var $this PageController */
/* @var $model ModPages */
/* @var $model ModPagesTexts */

$this->breadcrumbs=array(
	Yii::t('modNews','News')=>Yii::app()->createUrl('news'),
	Yii::t('main','New'),
);
$this->leftMenu= $this->renderPartial('/menu/left',array(),true);

$this->box_title=Yii::t('modNews','Create a news');
$this->form_title=Yii::t('modNews','Creating a new news');
?>


<?php
    $content= $this->renderPartial('_form',
        array(
            'model'=>$model,
            'modelTexts' => $modelTexts,
            'newsCategories' => $newsCategories,
            'images'=>$images,
            'uploadedImages'=>'',
            'submitUrl'=>Yii::app()->createUrl('news/records/create'),
        ),true);
    $this->widget('box',array('boxTitle'=>$this->box_title,'boxContent'=>$content))
?>