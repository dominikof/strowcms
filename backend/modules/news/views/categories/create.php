<?php
/* @var $this CategoriesController */
/* @var $model ModNewsCategory */
/* @var $model ModNewsCategoryTexts */
$this->breadcrumbs=array(
    Yii::t('modUsers','Users')=>Yii::app()->createUrl('users'),
	Yii::t('main','New user'),
);
$this->leftMenu= $this->renderPartial('/menu/left',array(),true);

$this->box_title=Yii::t('main','Create');
$this->form_title=Yii::t('modNews','Create new news category');
?>


<?php
    $content= $this->renderPartial('_form',
        array(
            'model'=>$model,
            'modelTexts' => $modelTexts,
            'submitUrl'=>Yii::app()->createUrl('news/categories/create'),
        ),true);
    $this->widget('box',array('boxTitle'=>$this->box_title,'boxContent'=>$content))
?>