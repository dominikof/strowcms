<?php
/* @var $this PagesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('modNews','News categories'),
);
$this->box_title=Yii::t('ModNews','News categories');
$this->leftMenu= $this->renderPartial('/menu/left',array(),true);
?>

<?php
$htmlAfterRemoveBtn='';
$this->widget('addRemoveBtns',
    array(
        'addUrl'=>Yii::app()->createUrl('news/categories/create/'),
        'removeUrl'=>Yii::app()->createUrl('news/categories/remove/'),
        'msgRemove'=>Yii::t("modNews","Do you really want to delete the selected news category?"),
    ));
?>

<table class="table table-striped table-condensed sortable-table" data-order-url="<?php echo Yii::app()->createUrl('news/categories/order/');?>">
    <thead>
    <tr>
        <th class="text-center table-check-column"><?php echo CHtml::checkBox('selectAll', false, array('onclick' => "$('input[name*=\'check\']').attr('checked', this.checked);")) ?></th>
        <th>ID</th>
        <th><?php echo Yii::t("modNews",'Category name'); ?></th>
        <th><?php echo Yii::t("modNews",'Category url'); ?></th>
        <th class="table-action-column"><?php echo Yii::t("main",'Action'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $data=$dataProvider->getData();
    foreach ($data as $key => $category):
        ?>
    <tr class='tr-sortable'>
        <td>
            <div class="move_zone">
                <i class="icon-list"></i>
            </div>
            <?php
            echo Chtml::hiddenField("order[$category->order]", $category->id);
            echo Chtml::checkBox('check[]', false,array('value'=>$category->id));

            ?>

        </td>
        <td class="id-column">
            <?php echo  $category->id;?>
        </td>
        <td>
            <?php
                echo $category->categoryTexts['name'];
            ?>
        </td>
        <td>
            <?php
            echo $category->url['translit'];
            ?>
        </td>
        <td class="table-action-column">
            <?php
            $this->widget('editRemoveBtnGroup',array(
                'editUrl'=>Yii::app()->createUrl('news/categories/edit/'.$category->id),
                'delUrl'=>Yii::app()->createUrl('news/categories/delete/'.$category->id),
                'delQuestion'=>Yii::t("modNews","Do you really want to delete the selected news category"),
            ));
            ?>
        </td>
    </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
    $this->widget('pagination',array('count'=>$dataProvider->getTotalItemCount(),'perPage'=>Yii::app()->getModule('news')->categoriesPerPage,'url'=>Yii::app()->createUrl('news/categories')))
?>