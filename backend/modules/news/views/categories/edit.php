<?php
/* @var $this PageController */
/* @var $model ModPages */
/* @var $model ModPagesTexts */

$this->breadcrumbs=array(
    Yii::t('modNews','News categories')=>Yii::app()->createUrl('news/categories'),
    'Edit',
);
$this->leftMenu= $this->renderPartial('/menu/left',array(),true);

$this->box_title=Yii::t('main','Edit');
$this->form_title=Yii::t('modNews','Edit news category ID:'.$model->id);
?>


<?php
$content= $this->renderPartial('_form',
    array(
        'model'=>$model,
        'modelTexts' => $modelTexts,
        'submitUrl'=>Yii::app()->createUrl('/news/categories/edit/'.$model->id),
    ),true);
$this->widget('box',array('boxTitle'=>$this->box_title,'boxContent'=>$content))
?>