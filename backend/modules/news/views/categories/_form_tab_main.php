<?php
/* @var $this CategoriesController */
/* @var $model ModNewsCategory */
/* @var $model ModNewsCategoryTexts */
?>
<input type="hidden" name='page[level]' value="0">

<div class="control-group">
    <label class="control-label" for="NewsVisibleId">
        <?php  echo Yii::t("modNews", "Visibility of the category");?>
    </label>
    <div class="controls">
        <?php
            echo CHtml::hiddenField('newsCategs[visible]',0);
            echo CHtml::checkBox('newsCategs[visible]', $model->visible,array('id'=>'NewsVisibleId'));
        ?>
    </div>
</div>

<?php $this->widget('multiLangsTabs',array(
    'modelTexts'=>$modelTexts,
    'view'=>'application.modules.news.views.categories.langs._form_milti_lang'
)); ?>
