<?php
/**
 * User: Panargin S.S.
 * Date: 17.11.12
 * Time: 16:27
 */

?>
<!-- start: Main Menu -->
<div class="span2 main-menu-span">
    <div class="nav-collapse sidebar-nav">
        <ul class="nav nav-tabs nav-stacked main-menu">
            <li><a href="<?php echo Yii::app()->createUrl('news/records/create') ?> "><i class="icon-plus icon-white"></i><span class="hidden-tablet"> New news</span></a></li>
            <li><a href="<?php echo Yii::app()->createUrl('news/records') ?> "><i class="icon-list-alt icon-white"></i><span class="hidden-tablet"> News</span></a></li>
            <li><a href="<?php echo Yii::app()->createUrl('news/categories') ?> "><i class="icon-list-alt icon-white"></i><span class="hidden-tablet"> Categories</span></a></li>
            <li><a href="index.html"><i class="icon-home icon-white"></i><span class="hidden-tablet"> Dashboard</span></a></li>
        </ul>
    </div><!--/.well -->
</div><!--/span-->
<!-- end: Main Menu -->