<?php

class NewsModule extends CWebModule
{

    public $defaultController="records";
    public $perPage=10;
    public $categoriesPerPage=10;
    public $moduleId;

	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'news.models.*',
			'news.components.*',
            'common.modules.news.models.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{

            $this->moduleId=Modules::model()->getModuleIdByName('news');
			return true;
		}
		else
			return false;
	}
}
