<?php
/**
 * User: Panarin S.S.
 * Date: 23.12.12
 * Time: 14:05
 */
class ModNewsAdmin extends ModNews
{

    private $moduleId;
    private $perPage;
    private $SUrlManagerObj;
    private $ModUploaderObj;

    public function behaviors()
    {
        $moduleObj=Yii::app()->getModule('news');
        $this->perPage=$moduleObj->perPage;
        $this->moduleId=$moduleObj->moduleId;
        
        return array(
            'AdminBehaviors'=>array(
                'class'=>'ext.behaviors.AdminBehaviors',
                'tableName'=>$this->tableName(),
            ),
        );
    }


    public function getNewsByPages()
    {
        $page = Yii::app()->request->getParam('page', 1);
        $dataProvider = new CActiveDataProvider('ModNews', array(
            'criteria' => array(
                'order' => '"t"."start_date" DESC',
                'with' => array(
                    'category' => array(
                        'joinType' => 'LEFT JOIN',
                        'with'=>array(
                            'categoryTexts' => array(
                                'joinType' => 'LEFT JOIN',
                                'on' => '"categoryTexts"."lang_id"='.lang_id.'',
                            ),
                        ),
                    ),
                    'url' => array(
                        'joinType' => 'LEFT JOIN',
                        'on' => '"url"."lang_id"='.lang_id.' AND "category"=false AND "url"."module_id"='.$this->moduleId.'',
                    ),
                    'texts' => array(
                        'joinType' => 'LEFT JOIN',
                        'on' => '"texts"."lang_id"='.lang_id.'',
                    ),
                    'images' => array(
                        'joinType' => 'LEFT JOIN',
                        'on' => '"images"."module_id"='.$this->moduleId.'',
                    ),
                ),
                'together' => false,
            ),
            'pagination' => array(
                'pageSize' => $this->perPage,
                'currentPage' => $page - 1
            ),
        ));

        $this->addSearchCriteria($dataProvider);

        return $dataProvider;
    }

    private function addSearchCriteria(CActiveDataProvider &$activeDataProvider){
        $filter_id=Yii::app()->request->getParam('filter_id');
        if(!empty($filter_id))
            $activeDataProvider->criteria->compare('t."id"',$filter_id);

        $filter_name=Yii::app()->request->getParam('filter_name');
        if(!empty($filter_name))
            $activeDataProvider->criteria->compare('"texts"."name"',$filter_name,true);

        $filter_category=Yii::app()->request->getParam('filter_category');
        if(!empty($filter_category))
            $activeDataProvider->criteria->compare('"category"."id"',$filter_category);

        $filter_status=Yii::app()->request->getParam('filter_status');
        if(!empty($filter_status))
            $activeDataProvider->criteria->compare('t."visible"',$filter_status);


    }

    public function getAllCategories(){
        $condition='';
        $dataProvider = new CActiveDataProvider('ModNewsCategory', array(
            'criteria' => array(
                'order' => '"order"',
                'condition' => $condition,
                'with' => array(
                    'categoryTexts' => array(
                        'joinType' => 'LEFT JOIN',
                        'condition' => '"categoryTexts"."lang_id"='.lang_id.'',
                    ),

                ),
            ),
        ));
        return $dataProvider->getData();
    }

    public function deleteNews($id)
    {
        $this->ModUploaderObj=new ModUploaderAdmin;
        $this->SUrlManagerObj=new MultiLanguageUrlManager();

        if(Yii::app()->db->currentTransaction===NULL)
            $transaction = Yii::app()->db->beginTransaction();

        if (!empty($id)) {
            if (is_array($id)) {
                $this->deleteSetOfNews($id);
            } elseif(is_numeric($id)) {
                $this->deleteSingleNews($id);
            }
        }

        if(isset($transaction))
            $transaction->commit();
    }

    private function deleteSingleNews($id){
        $this->findByPk($id)->delete();
        /**remove urls*/
        $this->SUrlManagerObj->deleteUrlNode($this->moduleId,$id);
        /**remove images*/
        $this->ModUploaderObj->deleteForModelAndPosition($this->moduleId,$id);
    }

    private function deleteSetOfNews($arr_id){
        $str_ids=implode(',',$arr_id);
        $this->deleteAll('id IN (' . $str_ids . ')');
        /**remove urls*/
        $this->SUrlManagerObj->deleteUrlNode($this->moduleId,$arr_id);
        foreach ($arr_id as $id) {
            $this->ModUploaderObj->deleteForModelAndPosition($this->moduleId,$id);
        }
    }

}
