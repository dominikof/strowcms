<?php
/**
 * User: Panarin S.S.
 * Date: 23.12.12
 * Time: 14:06
 */
class ModNewsCategoryAdmin extends ModNewsCategory
{
    private $moduleId;
    private $categoriesPerPage;
    private $SUrlManagerObj;
    private $newsAdmin;

    public function behaviors()
    {
        $moduleObj=Yii::app()->getModule('news');
        $this->categoriesPerPage=$moduleObj->categoriesPerPage;
        $this->moduleId=$moduleObj->moduleId;
        
        return array(
            'AdminBehaviors'=>array(
                'class'=>'ext.behaviors.AdminBehaviors',
                'tableName'=>$this->tableName(),
            ),
        );
    }

    public function getCategoriesBypages()
    {
        $page = Yii::app()->request->getParam('page', 1);
        $dataProvider = new CActiveDataProvider('ModNewsCategory', array(
            'criteria' => array(
                'order' => '"order"',
                'with' => array(
                    'categoryTexts' => array(
                        'joinType' => 'LEFT JOIN',
                        'on' => '"categoryTexts"."lang_id"='.lang_id.'',
                    ),
                    'url' => array(
                        'joinType' => 'LEFT JOIN',
                        'on' => '"url"."lang_id"='.lang_id.' AND "category"=true AND "url"."module_id"='.$this->moduleId.'',
                    ),

                ),
                'together' => false,
            ),
            'pagination' => array(
                'pageSize' => $this->categoriesPerPage,
                'currentPage' => $page - 1
            ),
        ));
        return $dataProvider;
    }

    public function deleteCategory($id)
    {
        $this->SUrlManagerObj=new MultiLanguageUrlManager();
        $transaction = Yii::app()->db->beginTransaction();
        if (!empty($id)) {
            if (is_array($id)) {
                $this->deleteSetOfCategories($id);
            } elseif(is_numeric($id)) {
                $this->deleteSingleCategory($id);
            }
        }
        $transaction->commit();
    }

    /**
     * @param $category_id
     */
    private function deleteSingleCategory($category_id){
        $this->findByPk($category_id)->delete();
        /**remove urls*/
        $this->SUrlManagerObj->deleteUrlNode($this->moduleId,$category_id,true);
        /**remove subpages*/
        $this->deleteNewsByCategoryId($category_id);
    }

    private function deleteSetOfCategories($arr_id){
        $str_ids=implode(',',$arr_id);
        $this->deleteAll('id IN (' . $str_ids . ')');
        /**remove urls*/
        $this->SUrlManagerObj->deleteUrlNode($this->moduleId,$arr_id,true);
        foreach ($arr_id as $category_id) {
            $this->deleteNewsByCategoryId($category_id);
        }
    }

    private function deleteNewsByCategoryId($category_id){
        if(!isset($this->newsAdmin) OR empty($this->newsAdmin))
            $this->newsAdmin=new ModNewsAdmin();
        $categsNewsDelete=$this->getNewsByCategoryId($category_id);
        $this->newsAdmin->deleteNews($categsNewsDelete);
    }

    private function getNewsByCategoryId($category_id){
        $rowsDel=ModNews::model()->findAll('"category_id"=:category_id',array(':category_id'=>$category_id));
        $str_del=array();
        foreach ($rowsDel as $row) {
            $str_del[]=$row->id;
        }
        return $str_del;
    }

}
