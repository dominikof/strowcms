<?php
/* @var $this UserController */
/* @var $model User */
/* @var $modelData UsersData */
$this->breadcrumbs=array(
    Yii::t('modUsers','Users')=>Yii::app()->createUrl('users'),
    Yii::t('modUsers','Edit user'),
);
$this->leftMenu= $this->renderPartial('/menu/left',array(),true);

$this->box_title=Yii::t('modUsers','Edit');
$this->form_title=Yii::t('modUsers','Edit a user ID:'.$model->id);
?>


<?php
$content= $this->renderPartial('_form',
    array(
        'model'=>$model,
        'modelData' => $modelData,
//        'groups' => $groups,
        'submitUrl'=>Yii::app()->createUrl('users/user/edit/'.$model->id),
    ),true);
$this->widget('box',array('boxTitle'=>$this->box_title,'boxContent'=>$content))
?>