<?php
/* @var $this CategoriesController */
/* @var $model User */
/* @var $model ModNewsTexts */
?>
<?php if (!empty($model->id)): ?>
<div class="control-group">
    <label class="control-label">
        <?php
        echo Yii::t("modUsers", "User rights");
        ?>
    </label>

    <div class="controls">
            <?php
            $this->widget('userPermissions', array('user_id' => $model->id));
        $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','label'=>Yii::t('modUsers','Edit user rights'),'type'=>'info','htmlOptions'=>array('rel'=>'tooltip','data-original-title'=>Yii::t('modUsers','Change user permissions')),'url'=>Yii::app()->createUrl('auth/assignment/view/'.$model->id),'icon'=>'ban-circle white'))
            ?>

    </div>
</div>
<?php endif; ?>
<div class="control-group">
    <label class="control-label" for="UsernameId">
        <?php  echo Yii::t("modUsers", "User name (login)");?>
    </label>

    <div class="controls">
        <div class="input-prepend">
            <span class="add-on"><i class="icon-user"></i></span>
            <?php
            echo CHtml::textField('user[username]', $model->username, array('id' => 'UsernameId'));
            ?>
        </div>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="EmailId">
        <?php  echo Yii::t("modUsers", "User email");?>
    </label>

    <div class="controls">
        <div class="input-prepend">
            <span class="add-on"><i class="icon-envelope"></i></span>
            <?php
            echo CHtml::textField('user[email]', $model->email, array('id' => 'EmailId'));
            ?>
        </div>
    </div>
</div>
<hr/>
<div class="control-group">
    <label class="control-label" for="PasswordId">
        <?php  echo Yii::t("modUsers", "Password");?>
    </label>

    <div class="controls">
        <?php
        echo CHtml::textField('user[newPassword]', '', array('id' => 'PasswordId'));
        ?>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="ConfirmPasswordId">
        <?php  echo Yii::t("modUsers", "Confirm password");?>
    </label>

    <div class="controls">
        <?php
        echo CHtml::textField('user[passwordConfirm]', '', array('id' => 'ConfirmPasswordId'));
        ?>
    </div>
</div>
<hr/>