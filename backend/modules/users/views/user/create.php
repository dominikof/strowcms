<?php
/* @var $this UserController */
/* @var $model User */
/* @var $modelData UsersData */
$this->breadcrumbs=array(
    Yii::t('modUsers','Users')=>Yii::app()->createUrl('users'),
    Yii::t('modUsers','New user'),
);
$this->leftMenu= $this->renderPartial('/menu/left',array(),true);

$this->box_title=Yii::t('modUsers','Create');
$this->form_title=Yii::t('modUsers','Creating a new user');
?>


<?php
$content= $this->renderPartial('_form',
    array(
        'model'=>$model,
        'modelData' => $modelData,
        'submitUrl'=>Yii::app()->createUrl('users/user/create'),
    ),true);
$this->widget('box',array('boxTitle'=>$this->box_title,'boxContent'=>$content))
?>