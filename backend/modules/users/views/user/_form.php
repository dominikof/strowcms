<?php
/* @var $this CategoriesController */
/* @var $model User */
/* @var $model ModNewsTexts */
?>
<legend>
    <?php
    echo $this->form_title;
    ?>
</legend>
<div class="inner-form-body">

    <?php echo CHtml::form($submitUrl,"POST",
    array('class' => 'form-horizontal ajax', 'id' => 'modalEditFormId', 'enctype' => 'multipart/form-data'));
    echo CHtml::hiddenField('submit', '1');
    echo CHtml::hiddenField('submit', '1');
    echo CHtml::hiddenField('btn-submitted', 'go',array('id'=>'fromActionId'));
    ?>
    <!--        <h2>--><?php //echo Yii::t('modPages','Creating new page') ?><!--</h2>-->

    <?php
    $general=$this->renderPartial('_form_tab_main',
        array(
            'modelData'=>$modelData,
            'model' => $model,
//            'groups' => $groups,
        ),true);
    $userDetails=$this->renderPartial('_form_tab_details',array('modelData'=>$modelData,'model' => $model),true);
    $this->widget('bootstrap.widgets.TbTabs', array(
        'type'=>'tabs', // 'tabs' or 'pills'
        'tabs'=>array(
            array('label'=>Yii::t('main','General'), 'content'=>$general, 'active'=>true,),
            array('label'=>Yii::t('modUsers','User details'), 'content'=>$userDetails,),
        ),
    ));
    ?>

    <?php $this->renderPartial('//forms/save_btn_group',array('cancelUrl'=>Yii::app()->createUrl('users'),'formId'=>'modalEditFormId')) ?>
    <?php
        echo CHtml::endForm();
    ?>
    <!--    </div>-->
</div>