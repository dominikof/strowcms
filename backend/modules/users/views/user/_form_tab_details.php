<div class="control-group">
    <label class="control-label" for="NameId">
        <?php  echo Yii::t("modUsers", "Name");?>
    </label>

    <div class="controls">
        <?php
        echo CHtml::textField('userData[name]', $modelData['name'], array('id' => 'NameId'));
        ?>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="SecondNameId">
        <?php  echo Yii::t("modUsers", "Second name");?>
    </label>

    <div class="controls">
        <?php
        echo CHtml::textField('userData[second_name]', $modelData['second_name'], array('id' => 'SecondNameId'));
        ?>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="AboutId">
        <?php  echo Yii::t("modUsers", "About");?>
    </label>

    <div class="controls">
        <?php
        echo CHtml::textArea('userData[about]', $modelData['about'], array('id' => 'AboutId'));
        ?>
    </div>
</div>