<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */
$this->breadcrumbs=array(
    Yii::t('modUsers','Users'),
);
$this->box_title=Yii::t('modUsers','Users');
$this->leftMenu= $this->renderPartial('/menu/left',array(),true);
?>

<?php
$this->widget('addRemoveBtns',
    array(
        'addUrl'=>Yii::app()->createUrl('users/user/create/'),
        'removeUrl'=>Yii::app()->createUrl('users/user/remove/'),
        'addBtnText'=>Yii::t("main","Add"),
        'removeBtnText'=>Yii::t("main","Remove"),
        'msgRemoveOk'=>Yii::t("main","Yes"),
        'msgRemoveNo'=>Yii::t("main","No"),
        'msgRemove'=>Yii::t("modUsers","Do you really want to delete the selected user"),
    ));
?>

<table class="table table-striped table-condensed sortable-table" data-order-url="<?php echo Yii::app()->createUrl('users/user/order/');?>">
    <thead>
    <tr>
        <th class="text-center table-check-column"><?php echo CHtml::checkBox('selectAll', false, array('onclick' => "$('input[name*=\'check\']').attr('checked', this.checked);")) ?></th>
        <th>ID</th>
        <th><?php echo Yii::t("modUsers",'Name'); ?></th>
        <th><?php echo Yii::t("modUsers",'Email'); ?></th>
        <th><?php echo Yii::t("modUsers",'Role'); ?></th>
        <th><?php echo Yii::t("modUsers",'Avatar'); ?></th>
        <th class="table-action-column"><?php echo Yii::t("main",'Action'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $data=$dataProvider->getData();
    foreach ($data as $key => $user):
        ?>
    <tr class='tr-sortable'>
        <td class="text-center">
<!--            <div class="move_zone">-->
<!--                <i class="icon-list"></i>-->
<!--            </div>-->
            <?php
            echo Chtml::checkBox('check[]', false,array('value'=>$user->id));

            ?>

        </td>
        <td class="id-column">
            <?php echo  $user->id;?>
        </td>
        <td>
            <?php
            echo $user['username'];
            ?>
        </td>
        <td>
            <?php
            echo $user['email'];
            ?>
        </td>
        <td>
            <?php
            $this->widget('userPermissions',array('user_id'=>$user->id))
            ?>
        </td>
        <td>

        </td>
        <td class="table-action-column">
           <?php
                $this->widget('editRemoveBtnGroup',array(
                    'editUrl'=>Yii::app()->createUrl('users/user/edit/'.$user->id),
                    'delUrl'=>Yii::app()->createUrl('users/user/delete/'.$user->id),
                    'delQuestion'=>Yii::t('modUsers','Do you really want to delete the selected user'),
                ));
                $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','type'=>'info','htmlOptions'=>array('rel'=>'tooltip','data-original-title'=>Yii::t('modUsers','Change user permissions')),'url'=>Yii::app()->createUrl('auth/assignment/view/'.$user->id),'icon'=>'ban-circle white'))
            ?>
        </td>
    </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$this->widget('pagination',array('count'=>$dataProvider->getTotalItemCount(),'perPage'=>Yii::app()->getModule('users')->perPage,'url'=>Yii::app()->createUrl('users')))
?>