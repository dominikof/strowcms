<?php
/**
 * User: Panargin S.S.
 * Date: 17.11.12
 * Time: 16:27
 */

?>
<!-- start: Main Menu -->
<div class="span2 main-menu-span">
    <div class="nav-collapse sidebar-nav">
        <ul class="nav nav-tabs nav-stacked main-menu">
            <li><a href="/admin/users/user/create"><i class="icon-plus icon-white"></i><span class="hidden-tablet"> New user</span></a></li>
            <li><a href="/admin/users/ "><i class="icon-list-alt icon-white"></i><span class="hidden-tablet"> Users all</span></a></li>
            <li><a href="index.html"><i class="icon-home icon-white"></i><span class="hidden-tablet"> Dashboard</span></a></li>
        </ul>
    </div><!--/.well -->
</div><!--/span-->
<!-- end: Main Menu -->