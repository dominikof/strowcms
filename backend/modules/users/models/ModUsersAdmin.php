<?php
/**
 * User: Panarin S.S.
 * Date: 19.01.13
 * Time: 16:15
 */
class ModUsersAdmin extends User
{

    private $moduleId;
    private $perPage;


    public function behaviors()
    {

        $moduleObj = Yii::app()->getModule('users');
        $this->perPage = $moduleObj->perPage;
        $this->moduleId = $moduleObj->moduleId;
        return array();
    }

    public function getUsersByPages()
    {
        $page = Yii::app()->request->getParam('page', 1);
        $dataProvider = new CActiveDataProvider('user', array(

            'pagination' => array(
                'pageSize' => $this->perPage,
                'currentPage' => $page - 1
            ),
        ));

        return $dataProvider;
    }


    public function deleteUser($user_id)
    {
        $transaction = Yii::app()->db->beginTransaction();
        if (!is_array($user_id)) {
            $this->deleteSingleUser($user_id);
        } else {
            $this->deleteSetOfUsers($user_id);
        }
        $transaction->commit();
    }

    private function deleteSingleUser($user_id)
    {
        $user = User::model()->findByPk($user_id);
        $this->revoke($user->id);
        $user->delete();
    }

    private function deleteSetOfUsers($user_arr)
    {
        $str_ids=implode(',',$user_arr);
        $this->deleteAll('id IN (' . $str_ids . ')');
        foreach ($user_arr as $user_id) {
            $this->revoke($user_id);
        }

    }

    private function revoke($user_id)
    {
        $am = Yii::app()->getAuthManager();
        $assignments = $am->getAuthAssignments($user_id);
        $permissions = $am->getItemsPermissions(array_keys($assignments));
        foreach ($permissions as $itemPermission) {
            $itemName = $itemPermission['item']->name;
            if ($am->isAssigned($itemName, $user_id)) {

                $am->revoke($itemName, $user_id);
                if ($am instanceof CPhpAuthManager)
                    $am->save();

                if ($am instanceof ICachedAuthManager)
                    $am->flushAccess($itemName, $user_id);
            }
        }
    }
}
