<?php

class UsersModule extends CWebModule
{

    public $defaultController="user";
    public $perPage=20;
    public $moduleId;

	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'users.models.*',
			'users.components.*',
			'users.widgets.*',
			'common.modules.users.models.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
            $this->moduleId=Modules::model()->getModuleIdByName('users');
			return true;
		}
		else
			return false;
	}
}
