<?php

class UserController extends Controller
{
    protected $perPage;
    protected $moduleId;


    protected function beforeAction($action)
    {
        $moduleObj = Yii::app()->getModule('users');
        $this->perPage = $moduleObj->perPage;
        $this->moduleId = $moduleObj->moduleId;

        return parent::beforeAction($action);
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('delete', 'index', 'view', 'create', 'edit', 'remove', 'edit', 'order'),
                'users' => array('root'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Lists all models.
     */
    public function actionIndex()
    {

        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/cms/modules/show.js');
        $model = new ModUsersAdmin();
        $dataProvider = $model->getUsersByPages();
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {

        $model = new User();
        $modelData = new UsersData();


        $transaction = Yii::app()->db->beginTransaction();
        if (isset($_POST['user']) AND isset($_POST['userData'])) {

            $model->attributes = $_POST['user'];
            if(!empty($_POST['user']['newPassword'])){
                $model->verifyPassword("newPassword");
                $model->password=$model->newPassword;
            }
            if (!$model->validate() OR !$model->save()) {
                $transaction->rollBack();
                $this->throwError($model->errors);
            }

            $modelData->attributes = $_POST['userData'];
            $modelData->user_id=$model->id;
            if (!$modelData->validate() OR !$modelData->save()) {
                $transaction->rollBack();
                $this->throwError($modelData->errors);
            }


            $transaction->commit();
            if ($_POST['btn-submitted'] == 'return')
                $url = Yii::app()->createUrl(lang_url . 'users/user/edit/' . $model->id);
            else $url = Yii::app()->createUrl(lang_url . 'users');
            $this->throwSuccess(array('url' => $url), 'redirect');
            Yii::app()->end();
        }


        $this->render('create', array(
            'model' => $model,
            'modelData' => $modelData,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdit($id)
    {

        $model = $this->loadModel($id);
        $modelData = UsersData::model()->findByPk($id);

        $transaction = Yii::app()->db->beginTransaction();
        if (isset($_POST['user']) AND isset($_POST['userData'])) {

            $model->attributes = $_POST['user'];
            $model->update_id=$id;
            if(!empty($_POST['user']['newPassword'])){
                $model->verifyPassword("newPassword");
                $model->password=$model->newPassword;
            }
            if (!$model->validate() OR !$model->save()) {
                $transaction->rollBack();
                $this->throwError($model->errors);
            }

            $modelData->attributes = $_POST['userData'];

            if (!$modelData->validate() OR !$modelData->save()) {
                $transaction->rollBack();
                $this->throwError($modelData->errors);
            }


            $transaction->commit();
            if ($_POST['btn-submitted'] == 'return')
                $url = Yii::app()->createUrl(lang_url . 'users/user/edit/' . $model->id);
            else $url = Yii::app()->createUrl(lang_url . 'users');
            $this->throwSuccess(array('url' => $url), 'redirect');
            Yii::app()->end();
        }

//        $groups=$auth->getAuthItems();
//        $groups=UserGroups::model()->findAll();
        $this->render('edit', array(
            'model' => $model,
            'modelData' => $modelData,
//            'groups'=>$groups,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $modelUsersAdmin = new ModUsersAdmin();
        $modelUsersAdmin->deleteUser($id);
        $this->ajaxThrowContent();
    }

    /**
     * Remove set of items
     */
    public function actionRemove()
    {
        $usersToDelete = Yii::app()->request->getParam('check', array());
        if (count($usersToDelete) > 0) {
            $modelUsersAdmin = new ModUsersAdmin();
            $modelUsersAdmin->deleteUser($usersToDelete);
            $this->ajaxThrowContent();
        }

    }




    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = ModUsersAdmin::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }


    private function ajaxThrowContent()
    {
        $model = new ModUsersAdmin();
        $dataProvider = $model->getUsersByPages();

        $render = $this->renderPartial('index', array(
            'dataProvider' => $dataProvider,
        ), true);
        $this->throwSuccess(array('content' => $render, 'div' => 'mainContentID'));
    }


}
