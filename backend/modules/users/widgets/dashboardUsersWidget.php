<?php
/**
 * User: Panarin S.S.
 * Date: 09.02.13
 * Time: 18:39
 */
class dashboardUsersWidget extends CWidget
{

    public function run(){
        Yii::import('common.modules.users.models.*');
        $dataProvider=$this->getData();
        CWidget::render('dashboardUsersWidget',array('data'=>$dataProvider));
    }

    private function getData(){
        $dataProvider = new CActiveDataProvider('User', array(
            'pagination'=>false,
            'criteria'=>array(
                'with'=>array(
                    'data'=>array(
                        'joinType' => 'LEFT JOIN',
                    ),
                ),
                'order'=>'id',
                'limit'=>3,
            ),
        ));
        return $dataProvider->getData();
    }

}
