<?php
/**
 * User: Panarin S.S.
 * Date: 03.02.13
 * Time: 14:33
 */
class userPermissions extends CWidget
{

    public $user_id;

    public function run(){
        $am = Yii::app()->getAuthManager();
        $assignments = $am->getAuthAssignments($this->user_id);
        $permissions = $am->getItemsPermissions(array_keys($assignments));
        foreach ($permissions as $itemPermission)
        {
            echo $itemPermission['item']->name.' <small>('.$itemPermission['item']->description.')</small><br />';
        }
    }
}
