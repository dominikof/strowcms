
<div class="box span4 noMargin" ontablet="span12" ondesktop="span4">
    <div class="box-header">
        <h2><i class="icon-list"></i><span class="break"></span><?php echo Yii::t('modUsers','Last users')?></h2>
        <div class="box-icon">
            <a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
<!--            <a href="#" class="btn-close"><i class="icon-remove"></i></a>-->
        </div>
    </div>
    <div class="box-content" style="display: block;">

        <ul class="tickets">
            <?php foreach ($data as $user) :  ?>
                    <li>
<!--                        <a href="#">-->
<!--                            <img class="avatar" alt="Lucas" src="img/avatar.jpg">-->
<!--                        </a>-->
                        <strong><?php echo Yii::t('modUsers','Name') ?>:</strong> <a href="<?php echo Yii::app()->createUrl('users/user/edit/'.$user->id) ?>"><?php echo (!empty($user->data['name']) OR !empty($user->data['second_name']))? $user->data['name'].' '.$user->data['second_name']:$user->username ?></a><br>
                        <strong><?php echo Yii::t('modUsers','Since') ?>:</strong> <?php echo $user->register_date ?><br>
<!--                        <strong>Status:</strong> <span class="label label-success">Approved</span>-->
                    </li>

            <?php endforeach;?>
        </ul>

        <a class="blue" href="<?php echo Yii::app()->createUrl('users') ?>" title="<?php echo Yii::t('modUsers','all users') ?>"><?php echo Yii::t('modUsers','all users') ?> ></a>
    </div>
</div>