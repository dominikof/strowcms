<?php
/**
 * SiteController.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/24/12
 * Time: 1:21 AM
 */

class SiteController extends Controller
{



	/**
	 * Renders index page
	 */
	public function actionIndex()
	{

		$this->render('index');
	}

	/**
	 * Action to render the error
	 * todo: design proper error page
	 */
	public function actionError()
	{
		if ($error = app()->errorHandler->error)
		{
			if (app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}




}