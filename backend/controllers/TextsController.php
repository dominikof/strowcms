<?php

class TextsController extends Controller
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('delete', 'index', 'view', 'create', 'update', 'remove', 'edit','filter'),
                'users' => array('root'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
	}


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Sourcemessage;
        $translates=new Message();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Sourcemessage']) AND isset($_POST['Message']))
		{
            $transaction=Yii::app()->db->beginTransaction();

			$tmp=$_POST['Sourcemessage'];
            $tmp['category']=(!empty($tmp['category_new']))?$tmp['category_new']:$tmp['category'];
            $model->attributes=$tmp;
			if(!$model->validate() OR !$model->save()){
                $transaction->rollback();
                $this->throwError($model->errors);
            }

            $translates->attributesM = $_POST['Message'];

            if(!$translates->multiSave($model->id,'id')){
                $transaction->rollBack();
                $this->throwError($translates->errors);
            }

            $transaction->commit();
            $this->ajaxThrowContent();
		}
        $translateCategories=TextsAdmin::factory()->getTextsCategories();

		$render=$this->renderPartial('create',array(
			'model'=>$model,
			'translateCategories'=>$translateCategories,
			'translates'=>$translates,
		),true);
        $this->throwSuccess(array('content' => $render),'modal_form');
	}

    /**
     *filter for texts
     */
    public function actionFilter(){
        $this->ajaxThrowContent();
    }

    private function ajaxThrowContent()
    {
        $TextsAdmin=new TextsAdmin;
        $dataProvider=$TextsAdmin->getTextsByPages();
        $translateCategories=$TextsAdmin->getTextsCategories();
        $getQuery=MYCHtml::makeGetQuery(array('filter_category','filter_text','filter_translate'));
        $render=$this->renderPartial('index',array(
            'dataProvider'=>$dataProvider,
            'getQuery'=>$getQuery,
            'translateCategories'=>$translateCategories,
        ),true);
        $this->throwSuccess(array('content' => $render, 'div' => 'mainContentID'));
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionEdit($id)
	{
		$model=$this->loadModel($id);
        $messageModel=Message::model();
        $translates=$messageModel->getDataForEdit($id,'id');
        if(isset($_POST['Sourcemessage']) AND isset($_POST['Message']))
        {
            $transaction=Yii::app()->db->beginTransaction();

            $tmp=$_POST['Sourcemessage'];
            $tmp['category']=(!empty($tmp['category_new']))?$tmp['category_new']:$tmp['category'];
            $model->attributes=$tmp;
            if(!$model->validate() OR !$model->save()){
                $transaction->rollback();
                $this->throwError($model->errors);
            }

            $messageModel->attributesM = $_POST['Message'];

            if(!$messageModel->multiSave($model->id,'id')){
                $transaction->rollBack();
                $this->throwError($messageModel->errors);
            }

            $transaction->commit();
            $this->ajaxThrowContent();
        }
        $translateCategories=TextsAdmin::factory()->getTextsCategories();

        $render=$this->renderPartial('edit',array(
            'model'=>$model,
            'translateCategories'=>$translateCategories,
            'translates'=>$translates,
        ),true);
        $this->throwSuccess(array('content' => $render),'modal_form');
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		$this->ajaxThrowContent();
	}

    public function actionRemove(){
        $textsToDelete=Yii::app()->request->getParam('check',array());
        $strId=implode(',', $textsToDelete);
        $model=new Sourcemessage();
        $model->deleteAll('id IN (' . $strId . ')');
        $this->ajaxThrowContent();
    }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $TextsAdmin=new TextsAdmin;
		$dataProvider=$TextsAdmin->getTextsByPages();
        $translateCategories=$TextsAdmin->getTextsCategories();
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/cms/modules/show.js',CClientScript::POS_END);
        $getQuery=MYCHtml::makeGetQuery(array('filter_category','filter_text','filter_translate'));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'getQuery'=>$getQuery,
			'translateCategories'=>$translateCategories,
		));
	}



	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Sourcemessage::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

}
