<?php

class LangsController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
//	public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('delete', 'index', 'view', 'create', 'update', 'remove', 'edit'),
                'users' => array('root'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new LangsAdmin;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Langs'])) {
            $model->attributes = $_POST['Langs'];

            if ($model->validate() AND $model->checkLangLeft()) {
                $model->rewriteDefLang();
                $model->save();
                Simage::factory('/media/images/uploads/langs/',$model,'img')->setResize(16)->saveImage();
                $this->ajaxThrowContent();
            } else {
                $this->throwError($model->errors);
            }
        }
        $render = $this->renderPartial('create', array(
            'model' => $model,
        ), true);
        $this->throwSuccess(array('content' => $render), 'modal_form');

    }

    private function ajaxThrowContent()
    {
        $dataProvider = new CActiveDataProvider('Langs',array(
            'sort'=>array(
                'defaultOrder'=>'"order" ASC',
            )
        ));
        $render = $this->renderPartial('index', array(
            'dataProvider' => $dataProvider->getData(),
        ), true);
        $this->throwSuccess(array('content' => $render, 'div' => 'mainContentID'));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdit($id)
    {
        $model = $this->loadModel($id);

        if (isset($_POST['Langs'])) {
            $transaction = Yii::app()->db->beginTransaction();
            $model->attributes = $_POST['Langs'];
            if ($model->validate()) {
                $model->rewriteDefLang();
                $model->save();
                if ($model->checkLangLeft() AND Simage::factory('/media/images/uploads/langs/',$model,'img')->setResize(16)->saveImage()) {
                    $transaction->commit();
                    $this->ajaxThrowContent();
                } else {
                    $transaction->rollBack();
                    $this->throwError($model->errors);
                }
            } else {
                $this->throwError($model->errors);
            }

        }

        $render = $this->renderPartial('update', array(
            'model' => $model,
        ), true);

        $this->throwSuccess(array('content' => $render), 'modal_form');
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $transaction = Yii::app()->db->beginTransaction();

        $model=$this->loadModel($id);
        $img=$model->img;
        $model->delete();

        if ($model->checkLangLeft()) {
            Simage::remove("/media/images/uploads/langs/".$img);
            $transaction->commit();
            $this->ajaxThrowContent();
        } else {
            $transaction->rollBack();
            $this->throwError($model->errors);
        }
    }


    /**
     * Remove set of items
     */
    public function actionRemove()
    {
        $transaction = Yii::app()->db->beginTransaction();
        $model = new LangsAdmin();

        $langsToDelete=$model->findLangsForDelete($_POST['check']);
        $model->deleteAll('id IN (' . implode(',', $_POST['check']) . ')');
        if ($model->checkLangLeft()) {
            $model->deleteImgInRecords($langsToDelete);
            $transaction->commit();
            $this->ajaxThrowContent();
        } else {
            $transaction->rollBack();
            $this->throwError($model->errors);
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        if (isset($_POST['order'])) {
            $model = new LangsAdmin();
            $model->saveOrder($_POST['order']);
        } else {
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/cms/modules/show.js',CClientScript::POS_END);
            $dataProvider = new CActiveDataProvider('Langs',array(
                'sort'=>array(
                    'defaultOrder'=>'"order" ASC',
                )
            ));

            $this->render('index', array(
                'dataProvider' => $dataProvider->getData(),
            ));
        }
    }


    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = new LangsAdmin();
        $model = $model->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'langs-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
