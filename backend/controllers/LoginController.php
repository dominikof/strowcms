<?php

class LoginController extends CController
{
	public function actionIndex()
	{
//        $user = User::model()->findByPK(4);
//        //$2a$14$gq3SqL.T/nZbF6CqglH9HO7Zv9fIfEzSx07Y9IE6WtbiAVH1qRmly
//        $user->password='sapogi';
//        $user->verifyPassword("password");
//        $user->update();

        $auth=Yii::app()->authManager;
        if(Yii::app()->user->checkAccess('root'))
            $this->redirect(Yii::app()->baseUrl);
//        $role = $auth->createRole($userGroup->name);
//        $auth->save();
//        $op = $auth->createOperation('admin_login','Login to admin panel');

        $this->layout='/login/index';
        $model = new LoginForm();

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form')
        {
            $model->attributes = $_POST['LoginForm'];
//            $validRespons= CActiveForm::validate($model, array('username', 'password'));
            $model->validate(array('username', 'password'));
            if(count($model->errors)>0)
                $this->throwError($model->errors,'active_form_validation');
            else
                $this->throwSuccess('','make_login');

        }

        if (isset($_POST['LoginForm']))
        {
            $model->attributes = $_POST['LoginForm'];
            if ($model->validate(array('username', 'password')) && $model->login())
                $this->redirect(user()->returnUrl);
        }

        $sent = r()->getParam('sent', 0);
        $this->render('form', array(
            'model' => $model,
            'sent' => $sent,
        ));
	}

    /**
     * Выбросить ошибку и прекратить выполнение
     * @param string $params текст ошибки или масив с параметрами для обработки на фронтенде
     */
    protected function throwError($params, $action = 'noty_error')
    {
        if (is_array($params))
            $dat = array('result' => false, 'action'=>$action, 'params' =>  $params);
        else
            $dat = array('result' => false, 'action'=>$action, 'params' => array($params));

//        header('Content-type: text/x-json');
        echo json_encode($dat);
        Yii::app()->end();
    }

    /**
     * Завершить аджакс запрос успешно
     * @param string|array $params что то вернуть?
     * Если $info строка то в результирующий json будет добавлен info
     * Если $info массив или объект то он будет конвертироваться в json как есть
     */
    protected function throwSuccess($params = '',$action='noty_success')
    {
        $dat = array('result' => true, 'action'=>$action, 'params' => array());
        if (is_array($params)) {
            $dat['params'] = $params;
//            die(json_encode($dat));
        }
        if (is_string($params) && strlen($params))
            $dat['params'] = $params;

//        header('Content-type: text/x-json');
        echo json_encode($dat);
        Yii::app()->end();
    }


    /**
     * This is the action that handles user's logout
     */
    public function actionLogout()
    {
        user()->logout();
        $this->redirect(app()->homeUrl);
    }
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}