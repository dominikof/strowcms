<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;
?>

<?php
$this->leftMenu=$this->renderPartial('/menu/left',array(),true);
?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<?php
    if(isset(Yii::app()->params['widgets']['news']))
        $this->widget(Yii::app()->params['widgets']['news']);
    if(isset(Yii::app()->params['widgets']['users']))
        $this->widget(Yii::app()->params['widgets']['users']);
?>
