<?php
/* @var $this LangsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    Yii::t('langs','Langs') ,
);
$this->box_title=Yii::t('langs','Langs');
?>
<?php
$this->widget('addRemoveBtns',
    array(
        'addUrl' => Yii::app()->createUrl(lang_url . 'langs/create/'),
        'removeUrl' => Yii::app()->createUrl(lang_url . 'langs/remove/'),
        'msgRemove' => Yii::t("langs", "Do you really want to delete the selected languages"),
        'modal' => true,
    ));
?>
<?php echo CHtml::beginForm('/admin/langs/','post', array('id' => 'listFormId')); ?>
<table class="table table-striped table-condensed sortable-table">
    <thead>
    <tr>
        <th class="text-center table-check-column"><?php echo CHtml::checkBox('selectAll', false, array('onclick' => "$('input[name*=\'check\']').attr('checked', this.checked);")) ?></th>
        <th>ID</th>
        <th><?php echo Yii::t("langs",'Short') ?></th>
        <th><?php echo Yii::t("langs",'Name'); ?></th>
        <th><?php echo Yii::t("langs",'Image'); ?></th>
        <th><?php echo Yii::t("langs",'Use in frontend'); ?></th>
        <th><?php echo Yii::t("langs",'Use in backend'); ?></th>
        <th><?php echo Yii::t("langs",'Use in frontend by default'); ?></th>
        <th><?php echo Yii::t("langs",'Use in backend by default'); ?></th>
        <th class="table-action-column"><?php echo Yii::t("main",'Action'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($dataProvider as $key => $lang_arr): ?>
    <tr class='tr-sortable'>
        <td>
            <div class="move_zone">
                <i class="icon-list"></i>
            </div>
            <?php
            echo Chtml::hiddenField('order[]', $lang_arr['id']);
            echo Chtml::checkBox('check[]', false,array('value'=>$lang_arr['id']));

            ?>

        </td>
        <td class="id-column">
            <?php echo  $lang_arr['id'];?>
        </td>
        <td>
            <?php echo $lang_arr['short']; ?>
        </td>
        <td>
            <?php echo $lang_arr['name']; ?>
        </td>
        <td>
            <?php
            if (isset($lang_arr['img']) AND !empty($lang_arr['img'])): ?>
                <img src="<?php echo "/media/images/uploads/langs/" . $lang_arr['img']?>" alt="<?php echo $lang_arr['short']; ?>"
                     title="<?php echo $lang_arr['short']; ?>"/>
                <?php else: ?>
                <i class="icon-remove"></i>
                <?php endif;  ?>
        </td>
        <td>
            <?php if ($lang_arr['front']): ?>
            <i class="icon-ok"></i>
            <?php else: ?>
            <i class="icon-remove"></i>
            <?php endif; ?>
        </td>
        <td>
            <?php if ($lang_arr['back']): ?>
            <i class="icon-ok"></i>
            <?php else: ?>
            <i class="icon-remove"></i>
            <?php endif; ?>
        </td>
        <td>
            <?php if ($lang_arr['front_def']): ?>
            <i class="icon-ok"></i>
            <?php else: ?>
            <i class="icon-remove"></i>
            <?php endif; ?>
        </td>
        <td>
            <?php if ($lang_arr['back_def']): ?>
            <i class="icon-ok"></i>
            <?php else: ?>
            <i class="icon-remove"></i>
            <?php endif; ?>
        </td>
        <td class="table-action-column">
            <?php
            $this->widget('editRemoveBtnGroup',array(
                'editUrl'=>Yii::app()->createUrl('langs/edit/'.$lang_arr['id']),
                'delUrl'=>Yii::app()->createUrl('langs/delete/'.$lang_arr['id']),
                'delQuestion'=>Yii::t("langs","Do you really want to delete the selected language"),
                'modal'=>true,
            ));
            ?>
        </td>
    </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php echo CHtml::endForm() ?>