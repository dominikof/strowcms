<?php
/* @var $this LangsController */
/* @var $model Langs */
/* @var $form CActiveForm */
?>


<div class="modal-body">
    <?php echo CHtml::form('#',"POST",
    array('class' => 'form-horizontal', 'id' => 'modalEditFormId', 'enctype' => 'multipart/form-data'));
    echo CHtml::hiddenField('submit', '1');
    echo CHtml::hiddenField('langkey') ?>
    <div class="control-group">
        <?php echo CHtml::label(Yii::t("langs","Language short"),
        Yii::t("langs","Short"), array('class' => 'control-label'));

        ?>
        <div class="controls">
            <?php
            echo CHtml::textField('Langs[short]', $model->short,
                array("class" => "input-medium", "id" => "shortId"))
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label(Yii::t("langs","Name"),
        Yii::t("langs","Name"), array('class' => 'control-label'));

        ?>
        <div class="controls">
            <?php
            echo CHtml::textField('Langs[name]', $model->name,
                array("class" => "input-medium", "id" => "nameId"))

            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label(Yii::t("main","Parameters"),
        '', array('class' => 'control-label'));

        ?>
        <div class="controls">
            <label class="checkbox">
                <?php
                echo CHtml::hiddenField('Langs[front]', false);
                echo CHtml::checkBox('Langs[front]', $model->front);
                echo Yii::t("langs","Use in frontend");
                ?>
            </label>
            <label class="checkbox">
                <?php
                echo CHtml::hiddenField('Langs[back]', false);
                echo CHtml::checkBox('Langs[back]', $model->back);
                echo Yii::t("langs","Use in backend");
                ?>
            </label>
            <label class="checkbox">
                <?php
                echo CHtml::hiddenField('Langs[front_def]', false);
                echo CHtml::checkBox('Langs[front_def]', $model->front_def);
                echo Yii::t("langs","Use in frontend by default");
                ?>
            </label>
            <label class="checkbox">
                <?php
                echo CHtml::hiddenField('Langs[back_def]', false);
                echo CHtml::checkBox('Langs[back_def]', $model->back_def);
                echo Yii::t("langs","Use in backend by default");
                ?>
            </label>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label(Yii::t("langs","Image"),
        'imageLangId', array('class' => 'control-label'));

        ?>
        <div class="controls">
            <?php
            echo CHtml::fileField('img','', array('id' => 'imageLangId'));
            ?>
        </div>
    </div>
    <?php echo CHtml::endForm(); ?>
</div><!-- form -->