<?php
/* @var $this LangsController */
/* @var $model Langs */


?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3><?php echo Yii::t("langs","Create new language") ?></h3>

</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal"><i class="icon-remove "></i> <?php echo  Yii::t("main",'Close')?></a>
    <a href="<?php echo Yii::app()->createUrl('langs/create')?>" id='modalSubmitBtnId' class="btn btn-success"
       title="<?php echo Yii::t("main",'Save')?>"><i class=" icon-ok icon-white"></i> <?php echo Yii::t("main",'Save');?></a>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/js/cms/modules/editmodal.js"></script>

