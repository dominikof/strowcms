<?php
/* @var $this LangsController */
/* @var $model Langs */


?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3><?php echo Yii::t("langs","Редактирование языка ':lang_shrt' (ID: :id)",array(':lang_shrt'=>$model->name,':id'=>$model->id)) ?></h3>

</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal"><i class="icon-remove "></i> <?php echo  Yii::t("main",'Закрыть')?></a>
    <a href="/admin/langs/edit/<?php echo $model->id?>" id='modalSubmitBtnId' class="btn btn-primary"
       title="<?php echo Yii::t("main",'Сохранить')?>"><i class=" icon-ok icon-white"></i> <?php echo Yii::t("main",'Сохранить');?></a>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/js/cms/modules/editmodal.js"></script>

