<?php
$this->widget('bootstrap.widgets.TbNavbar', array(
    'type' => 'null', // null or 'inverse'
    'brand' => '<span>Strawberry cms</span>',
    'brandUrl' => Yii::app()->createUrl(lang_url),
    'fixed' => '',

    'collapse' => false, // requires bootstrap-responsive.css
    'items' => array(
        array(
            'class' => 'bootstrap.widgets.TbMenu',
            'items' => array(
                array('label' => Yii::t('mainMenu','Home'), 'url' => Yii::app()->createUrl(lang_url)),
                array(
                    'class' => 'bootstrap.widgets.TbMenu',
                    'label' => 'Content',
                    'items' => array(
                        array(
                            'label' => Yii::t('mainMenu','Pages'),
                            'url' => Yii::app()->createUrl('pages'),
                        ),
                        array(
                            'label' => Yii::t('mainMenu','News'),
                            'url' => Yii::app()->createUrl('news'),
                        ),
                    ),
                ),
                array(
                    'class' => 'bootstrap.widgets.TbMenu',
                    'htmlOptions' => array('class' => ''),
                    'label' => 'CMS', 'url' => '#',
                    'items' => array(
                        array('label' => 'Langs', 'url' => Yii::app()->createUrl('langs')),
                        array('label' => 'Multilanguage messages', 'url' => Yii::app()->createUrl('texts')),
                        array('label' => 'Users', 'url' => Yii::app()->createUrl('users')),
                    ),
                ),
            ),
        ),

        array(
            'class' => 'bootstrap.widgets.TbMenu',
            'htmlOptions' => array('class' => 'pull-right'),
            'encodeLabel' => false,
            'items' => array(
                array('label' => "<i class='icon-user icon-white'></i> " . Yii::app()->user->name,  'url' => '#',
                    'items' => array(
                        array('label' => "<i class='icon-user'></i> ".Yii::t('mainMenu', 'Profile'),'encodeLabel' => false, 'url' => '/admin/users/user/edit/'.Yii::app()->user->id),
                        array('label' => "<i class='icon-off'></i> ".Yii::t('mainMenu', 'Logout'),'encodeLabel' => false, 'url' => '/admin/login/logout/'),
                    )),
            ),
        ),
    ),
)); ?>