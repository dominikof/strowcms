<?php
/**
 * User: Panargin S.S.
 * Date: 17.11.12
 * Time: 16:27
 */
?>

<!-- start: Main Menu -->
<div class="span2 main-menu-span">
    <div class="nav-collapse sidebar-nav">
        <ul class="nav nav-tabs nav-stacked main-menu">
            <?php

            if(Yii::app()->params['dashboardMenu']['pages']): ?>
            <li>
                <a href="<?php echo Yii::app()->createUrl('pages/page')?>">
                    <i class="fa-icon-file icon-white"></i>
                    <span class="hidden-tablet"> <?php echo Yii::t('modPages','Pages')  ?></span>
                </a>
            </li>
            <?php endif; ?>
            <?php
            if(Yii::app()->params['dashboardMenu']['news']): ?>
            <li>
                <a href="<?php echo Yii::app()->createUrl('news')?>">
                    <i class="fa-icon-file icon-white"></i>
                    <span class="hidden-tablet"> <?php echo Yii::t('modNews','News')  ?></span>
                </a>
            </li>
            <?php endif; ?>
            <li>
                <a href="<?php echo Yii::app()->createUrl('/')?>">
                    <i class="icon-home icon-white"></i>
                    <span class="hidden-tablet"> Dashboard</span>
                </a>
            </li>
        </ul>
    </div><!--/.well -->
</div><!--/span-->
<!-- end: Main Menu -->