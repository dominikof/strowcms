<?php
/**
 * User: Panarin S.S.
 * Date: 18.11.12
 * Time: 20:49
 */
?>
<div class="form-actions">
    <button type="button" class="btn btn-primary" onclick="$('#<?php echo $formId;?>').submit();"><?php echo Yii::t('main','Save changes')?></button>
    <button type="button" class="btn btn-success" onclick="$('#fromActionId').val('return');$('#<?php echo $formId;?>').submit();"><?php echo Yii::t('main','Save changes and return')?></button>
    <a href="<?php echo $cancelUrl  ?>" class="btn" title="<?php echo Yii::t('main','Cancel')?>"><?php echo Yii::t('main','Cancel')?></a>
</div>
