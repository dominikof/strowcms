<?php
/* @var $this TextsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Multi language texts and translates',
);

$this->box_title = Yii::t('admin', 'Multi language texts and translates');
$this->leftMenu = $this->renderPartial('/texts/menu/left', array(), true);
?>
<?php
echo CHtml::form(Yii::app()->createUrl('texts'), 'get');
?>
<?php
echo CHtml::endForm();
?>
<?php
$this->widget('addRemoveBtns',
    array(
        'addUrl' => Yii::app()->createUrl(lang_url . 'texts/create/'),
        'removeUrl' => Yii::app()->createUrl(lang_url . 'texts/remove/'),
        'msgRemove' => Yii::t("texts", "Do you really want to delete the selected texts"),
        'modal' => true,
    ));
?>

<table class="table table-striped table-condensed">
    <thead>
    <th class="text-center table-check-column"><?php echo CHtml::checkBox('selectAll', false, array('onclick' => "$('input[name*=\'check\']').attr('checked', this.checked);")) ?></th>
    <th class="span4"><?php echo Yii::t('texts', 'Original text')?></th>
    <th class="span3"><?php echo Yii::t('texts', 'Dictionary')?></th>
    <th class="span4"><?php echo Yii::t('texts', 'Translate to current language')?></th>
    <th class="table-action-column"><?php echo Yii::t("main", 'Action'); ?></th>
    </thead>
    <tbody>
    <tr>
        <td></td>
        <td>
            <div class="filter-container ">
                <?php
                    echo CHtml::hiddenField('filterUrl',Yii::app()->createUrl('texts/filter'),array('id'=>'filterUrlId'));
                    echo CHtml::textField('filter_text',Yii::app()->request->getParam('filter_text'),array('class'=>'filter'))
                ?>
            </div>
        </td>
        <td>
            <div class="filter-container ">
                <?php
                $arrSearch=CHtml::listData($translateCategories, 'category', 'category');
                $static=array('0'=>Yii::t('texts','Select dictionary'));
                echo  CHtml::dropDownList('filter_category', Yii::app()->request->getParam('filter_category'),$static+ $arrSearch, array('class'=>'filter-select filter'));
                ?>
            </div>
        </td>
        <td>
            <div class="filter-container ">
                <?php
                echo CHtml::textField('filter_translate',Yii::app()->request->getParam('filter_translate'),array('class'=>'filter'))
                ?>
            </div>
        </td>
        <td></td>
    </tr>
    <?php
    $data = $dataProvider->getData();
    foreach ($data as $key => $text):
        ?>
    <tr>
        <td class="text-center">
            <?php
            echo Chtml::checkBox('check[]', false, array('value' => $text->id));
            ?>
        </td>
        <td><?php echo $text->message?></td>
        <td><?php echo $text->category?></td>
        <td>
            <?php
            foreach ($text->messages as $translate) {
                echo $translate->language . ': ' . $translate->translation . "<br>";
            }
            ?>
        </td>
        <td class="table-action-column">
            <?php
            $this->widget('editRemoveBtnGroup', array(
                'editUrl' => Yii::app()->createUrl('texts/edit/' . $text->id . $getQuery),
                'delUrl' => Yii::app()->createUrl('texts/delete/' . $text->id . $getQuery),
                'delQuestion' => Yii::t("texts", "Do you really want to delete the selected text"),
                'modal' => true,
            ));
            ?>
        </td>
    </tr>
        <?php endforeach; ?>

    </tbody>
</table>
<?php
$this->widget('pagination', array('count' => $dataProvider->getTotalItemCount(), 'perPage' => 15, 'url' => Yii::app()->createUrl(lang_url . 'texts' . $getQuery)));
?>