<?php
/* @var $this TextsController */
/* @var $model Sourcemessage */
/* @var $form CActiveForm */
?>

<div class="modal-body">

    <?php echo CHtml::form('#', "POST",
    array('class' => 'form-horizontal', 'id' => 'modalEditFormId', 'enctype' => 'multipart/form-data'));
    echo CHtml::hiddenField('submit', '1');
    ?>
    <div class="control-group" >
        <label class="control-label" for="textsMessageId"><?php echo Yii::t('texts', 'Category');?></label>

        <div class="controls" id="textsCategoryTabs">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#textsCategory"> <?php echo Yii::t('texts', 'Choose') ?></a></li>
                <li><a href="#textsNewCategory"> <?php echo Yii::t('texts', 'New category') ?></a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="textsCategory">
                    <?php
                    echo  CHtml::dropDownList('Sourcemessage[category]', $model->category, CHtml::listData($translateCategories, 'category', 'category'), array('id' => 'categoryList'));
                    ?>
                </div>
                <div class="tab-pane" id="textsNewCategory">
                    <?php
                    echo CHtml::textField('Sourcemessage[category_new]', '', array('id' => 'categoryNew'));
                    ?>
                </div>
            </div>
        </div>

    </div>
    <div class="control-group">
        <label class="control-label" for="textsMessageId"><?php echo Yii::t('texts', 'Source text');?></label>

        <div class="controls">
            <?php
            echo CHtml::textField('Sourcemessage[message]', $model->message, array('textsMessageId'))
            ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for=""><?php echo Yii::t('texts', 'Translates');?></label>

        <div class="controls">
            <?php
            $this->widget('multiLangsTabs',array(
                'modelTexts'=>$translates,
                'view'=>'application.views.texts.langs._translates'));
            ?>
        </div>
    </div>

    <script>
        $(function () {
            $('#textsCategoryTabs a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            })
        })
    </script>
    <?php echo CHtml::endForm(); ?>

</div><!-- form -->