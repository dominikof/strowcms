<?php
/* @var $this TextsController */
/* @var $model Sourcemessage */
?>
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">×</button>
<h3><?php echo Yii::t("texts","Edit multilanguage text translate") ?></h3>

</div>

<?php $this->renderPartial('_form', array('model'=>$model,'translateCategories'=>$translateCategories,'translates'=>$translates)); ?>

<div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal"><i class="icon-remove "></i> <?php echo  Yii::t("main",'Close')?></a>
    <a href="<?php echo Yii::app()->createUrl('texts/edit/'.$model->id)?>" id='modalSubmitBtnId' class="btn btn-success"
       title="<?php echo Yii::t("main",'Save')?>"><i class=" icon-ok icon-white"></i> <?php echo Yii::t("main",'Save');?></a>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/js/cms/modules/editmodal.js"></script>
