<?php
$this->pageTitle = Yii::app()->name . ' - Login';
?>


<div class="login-box">
    <div class="icons">
        <a href="/"><i class="icon-home"></i></a>
    </div>
    <h2>Strawbarry cms admin</h2>
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'login-form',
    'enableClientValidation' => true,
    'htmlOptions' => array('class' => 'login-form-class'),
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
)); ?>
    <fieldset>
        <div class="input-prepend" title="Username">
            <span class="add-on"><i class="icon-user"></i></span>
            <input class="input-large span10" name="LoginForm[username]" id="LoginForm_username" type="text"
                   placeholder="<?php echo Yii::t('main', 'user name');?>"/>


<!--            --><?php //echo $form->textFieldRow($model, 'username', array('class' => 'span3', 'prepend' => '<i class="icon-user"></i>'));?>
<!--            --><?php //echo $form->passwordFieldRow($model, 'password', array('class' => 'span3', 'prepend' => '<i class="icon-lock"></i>'));?>
        </div>
        <div class="clearfix"></div>
        <div class="input-prepend" title="Password">
            <span class="add-on"><i class="icon-lock"></i></span>
            <input class="input-large span10" name="LoginForm[password]" id="LoginForm_password" type="password" placeholder="<?php echo Yii::t('main','password')?>"/>
        </div>
        <div class="clearfix"></div>
        <label class="remember" for="LoginForm_rememberMe"><input type="checkbox" id="LoginForm_rememberMe" name="LoginForm[rememberMe]" />Remember me</label>
        <div class="btn-group button-login">
            <button type="submit" class="btn btn-primary"><i class="icon-off icon-white"></i></button>
            <button type="submit" class="btn btn-primary"><?php echo Yii::t('main','Login')?></button>
        </div>
        <div class="clearfix"></div>
<!--        <div class="form-actions">-->
<!--            --><?php //$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Submit'));?>
<!--            --><?php //echo $form->checkBoxRow($model, 'rememberMe');?>
<!--        </div>-->
    </fieldset>
    <?php $this->endWidget(); ?>
</div><!-- form -->
