<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!-- start: Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- end: Mobile Specific -->

    <link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon"/>
    <!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css"
          media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css"
          media="print"/>
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css"
          media="screen, projection"/>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin.css"/>

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <?php Yii::app()->bootstrap->register(); ?>
    <style type="text/css">
        body {
            background: url('<?php echo Yii::app()->request->baseUrl; ?>/themes/perfectum_dashbord/img/bg-login.jpg') !important;
        }
    </style>
</head>

<body>
<div id="cmsAjaxLoaderId" class="cms-ajax-loader btn-warning">
    <?php echo Yii::t('main','Loading'); ?>
</div>
<!--<div class="container login-container" id="page">-->
<div class="container-fluid">
    <div class="row-fluid">

        <div class="row-fluid">

            <?php echo $content;?>

        </div>
    </div>
</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/cms/login/init.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/perfectum_dashbord/js/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/cms/admin_modal.js'?>"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/cms/admin_notification.js'?>"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/libs/jquery-ui-1.9.1.custom.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/admin_js_lib.js'?>"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/plugins/bootbox.js'?>"></script>
<script type="text/javascript" src="<?php echo '/media/js/libs/noty/jquery.noty.js'?>"></script>
<script type="text/javascript" src="<?php echo '/media/js/libs/noty/layouts/top.js'?>"></script>
<script type="text/javascript" src="<?php echo '/media/js/libs/noty/layouts/topRight.js'?>"></script>
<script type="text/javascript" src="<?php echo '/media/js/libs/noty/themes/default.js'?>"></script>
<!------------------------------theme begin-------------------------------------->
<!-- start: CSS -->
<link id="base-style" href="<?php echo Yii::app()->baseUrl . '/themes/perfectum_dashbord/css/style.css'?>"
      rel="stylesheet">
<link id="base-style-responsive"
      href="<?php echo Yii::app()->baseUrl . '/themes/perfectum_dashbord/css/style-responsive.css'?>" rel="stylesheet">

<!--[if lt IE 7 ]>
<link id="ie-style" href="<?php echo Yii::app()->baseUrl . '/themes/perfectum_dashbord/css/style-ie.css'?>"
      rel="stylesheet">
<![endif]-->
<!--[if IE 8 ]>
<link id="ie-style" href="<?php echo Yii::app()->baseUrl . '/themes/perfectum_dashbord/css/style-ie.css'?>"
      rel="stylesheet">
<![endif]-->

<!-- end: CSS -->
<!------------------------------theme end-------------------------------------->

<!--</div>-->
<!-- page -->
</body>
</html>