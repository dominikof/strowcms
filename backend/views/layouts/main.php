<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!-- start: Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- end: Mobile Specific -->
    <link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico" type="image/x-icon"/>
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css"
          media="screen, projection"/>
    <![endif]-->

    <?php Yii::app()->bootstrap->register(); ?>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<div id="cmsAjaxLoaderId" class="cms-ajax-loader btn-warning">
    <?php echo Yii::t('main','Loading'); ?>
</div>
<?php echo $this->renderPartial('//menu/main_menu'); ?>
<div class="container-fluid" id="page">
    <div class="row-fluid">
        <?php
        if (isset($this->leftMenu) ) echo $this->leftMenu; ?>
        <div id='content' class="span10">
            <?php if (isset($this->breadcrumbs) AND !empty($this->breadcrumbs)): ?>
            <div>
                <hr>
                    <?php
                        $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
                            'links' => $this->breadcrumbs,
                        ));
                    ?><!-- breadcrumbs -->
            </div>
            <hr>
            <?php endif?>
            <div id="mainContentID">
                <?php
                echo $content;
                ?>
            </div>

            <!-- footer -->
        </div>
    </div>
    <footer id="footer">
        Copyright &copy; <?php echo date('Y'); ?> by Strawberry hills.<br/>
        All Rights Reserved.<br/>
    </footer>
</div>
<!------------------------------theme begin-------------------------------------->
<!-- start: CSS -->
<!--<link id="base-style" href="--><?php //echo Yii::app()->baseUrl . '/themes/bootstrap/css/bootstrap-responsive.min.css'?><!--"-->
<!--      rel="stylesheet">-->
<link id="base-style" href="<?php echo Yii::app()->baseUrl . '/themes/perfectum_dashbord/css/style.css'?>"
      rel="stylesheet">
<link id="base-style-responsive"
      href="<?php echo Yii::app()->baseUrl . '/themes/perfectum_dashbord/css/style-responsive.css'?>" rel="stylesheet">

<!--[if lt IE 7 ]>
<link id="ie-style" href="<?php echo Yii::app()->baseUrl . '/themes/perfectum_dashbord/css/style-ie.css'?>"
      rel="stylesheet">
<![endif]-->
<!--[if IE 8 ]>
<link id="ie-style" href="<?php echo Yii::app()->baseUrl . '/themes/perfectum_dashbord/css/style-ie.css'?>"
      rel="stylesheet">
<![endif]-->
<!-- end: CSS -->
<!--<!------------------------------theme end-------------------------------------->
</body>
</html>