<?php
return array(
    'dashboardMenu'=>array(
        'pages'=>true,
        'news'=>true,
    ),
    'widgets'=>array(
        'users'=>'application.modules.users.widgets.dashboardUsersWidget',
        'news'=>'application.modules.news.widgets.dashboardLastNewsW',
    ),

);