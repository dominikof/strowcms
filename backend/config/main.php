<?php
/**
 * main.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/22/12
 * Time: 5:48 PM
 *
 * This file holds the configuration settings of your backend application.
 **/
$backendConfigDir = dirname(__FILE__);

$root = $backendConfigDir . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..';

$params = require_once($backendConfigDir . DIRECTORY_SEPARATOR . 'params.php');
$dashboard = include $backendConfigDir . DIRECTORY_SEPARATOR . 'dashboard.php';
$params = CMap::mergeArray($params, $dashboard);
// Setup some default path aliases. These alias may vary from projects.
Yii::setPathOfAlias('root', $root);
Yii::setPathOfAlias('common', $root . DIRECTORY_SEPARATOR . 'common');
Yii::setPathOfAlias('backend', $root . DIRECTORY_SEPARATOR . 'backend');
Yii::setPathOfAlias('www', $root . DIRECTORY_SEPARATOR . 'backend' . DIRECTORY_SEPARATOR . 'www');
Yii::setPathOfAlias('bootstrap', $root . DIRECTORY_SEPARATOR . 'common' . DIRECTORY_SEPARATOR . 'extensions' . DIRECTORY_SEPARATOR . 'bootstrap');

/* uncomment if you need to use frontend folders */
/* Yii::setPathOfAlias('frontend', $root . DIRECTORY_SEPARATOR . 'frontend'); */


$mainLocalFile = $backendConfigDir . DIRECTORY_SEPARATOR . 'main-local.php';
$mainLocalConfiguration = file_exists($mainLocalFile) ? require($mainLocalFile) : array();

$mainEnvFile = $backendConfigDir . DIRECTORY_SEPARATOR . 'main-env.php';
$mainEnvConfiguration = file_exists($mainEnvFile) ? require($mainEnvFile) : array();
return CMap::mergeArray(
    array(
        'name' => 'Strawberry hills admin panel',
        // @see http://www.yiiframework.com/doc/api/1.1/CApplication#basePath-detail
        'basePath' => 'backend',
        // set parameters
        'params' => $params,

        // preload components required before running applications
        // @see http://www.yiiframework.com/doc/api/1.1/CModule#preload-detail
        'preload' => array('bootstrap', 'log'),
        // @see http://www.yiiframework.com/doc/api/1.1/CApplication#language-detail
        'language' => 'en',
        // using bootstrap theme ? not needed with extension
        'theme' => 'perfectum_dashbord',


        // setup import paths aliases
        // @see http://www.yiiframework.com/doc/api/1.1/YiiBase#import-detail

        'import' => array(
            'common.components.*',
            'common.extensions.*',

            /* uncomment if required */
            /* 'common.extensions.behaviors.*', */
            /* 'common.extensions.validators.*', */
            'common.models.*',

            'common.helpers.*',
            // uncomment if behaviors are required
            // you can also import a specific one
            /* 'common.extensions.behaviors.*', */
            // uncomment if validators on common folder are required
            /* 'common.extensions.validators.*', */
            'application.components.*',
            'application.controllers.*',

            'application.models.*',
            'application.helpers.*',
            'application.widgets.*',
            //uploader
            'common.modules.uploader.models.*',
            'application.modules.uploader.models.*',

        ),

        /* uncomment and set if required */
        // @see http://www.yiiframework.com/doc/api/1.1/CModule#setModules-detail
        'modules' => array(
            'gii' => array(
                'class' => 'system.gii.GiiModule',
                'password' => 'sapogi',
                'generatorPaths' => array(
                    'bootstrap.gii ',
                ),

            ),
            'pages',
            'uploader',
            'news',
            'users',
            'auth' => array(
                'strictMode' => true, // when enabled authorization items cannot be assigned children of the same type.
                'userClass' => 'User', // the name of the user model class.
                'userIdColumn' => 'id', // the name of the user id column.
                'userNameColumn' => 'username', // the name of the user name column.
//                'appLayout' => 'application.views.layouts.main', // the layout used by the module.
                'viewDir' => null, // the path to view files to use with this module.
            ),
        ),
        'behaviors' => array(//            'MultilingualApp',
            'AppStartConfigureBehavior'
        ),
        'controllerMap'=>array(
            'min'=>array(
                'class'=>'common.extensions.minScript.controllers.ExtMinScriptController',
            ),
        ),
        'components' => array(

            'clientScript' => array(
//                'class'=>'common.extensions.minScript.components.ExtMinScript',
                'scriptMap' => array(
                    'jquery.js' => '/media/js/libs/jquery.js',
                    'JqueryFormPlugin.js' => '/media/js/libs/JqueryFormPlugin.js',
                    'admin_modal.js' => '/backend/www/js/cms/admin_modal.js',
                    'admin.css' => '/backend/www/css/admin.css',
                    'admin_notification.js' => '/backend/www/js/cms/admin_notification.js',
                    'jquery-ui.min.js' => '/backend/www/js/libs/jquery-ui-1.10.1.custom.min.js',
                    'admin_js_lib.js' => '/backend/www/js/admin_js_lib.js',
                    'jquery.noty.js' => '/media/js/libs/noty/jquery.noty.js',
                    'top.js' => '/media/js/libs/noty/layouts/top.js',
                    'topRight.js' => '/media/js/libs/noty/layouts/topRight.js',
                    'default.js' => '/media/js/libs/noty/themes/default.js',
                    'jquery-ui.css' => '/backend/www/themes/perfectum_dashbord/css/jquery-ui-1.8.21.custom.css',
                ),
            ),

            'messages' => array(
                'class' => 'CDbMessageSource',

                // config for db message source here, see http://www.yiiframework.com/doc/api/CDbMessageSource
            ),
            'user' => array(
                'allowAutoLogin' => true,
                'class' => 'auth.components.AuthWebUser',
            ),

            'authManager' => array(
                'class' => 'CDbAuthManager',
//                'class'=>'auth.components.CachedDbAuthManager',
//                'cachingDuration'=>3600,
                'connectionID' => 'db',
                //auth module
                'behaviors' => array(
                    'auth' => array(
                        'class' => 'auth.components.AuthBehavior',
                        'admins' => array('root'), // users with full access
                    ),
                ),
            ),
            /* load bootstrap components */
            'bootstrap' => array(
                'class' => 'bootstrap.components.Bootstrap',
            ),
            'errorHandler' => array(
                // @see http://www.yiiframework.com/doc/api/1.1/CErrorHandler#errorAction-detail
                'errorAction' => 'site/error'
            ),
            'request' => array(
                'baseUrl' => '/admin',
            ),
            'db' => array(
                'connectionString' => $params['db.connectionString'],
                'username' => $params['db.username'],
                'password' => $params['db.password'],
                'schemaCachingDuration' => YII_DEBUG ? 0 : 86400000, // 1000 days
                'enableParamLogging' => YII_DEBUG,
                'charset' => 'utf8',
                'emulatePrepare' => false,
                'enableProfiling' => true
            ),
            'urlManager' => array(
                'urlFormat' => 'path',
                'class' => 'SUrlManager',
                'matchValue' => false,
                'showScriptName' => false,
                'rules' => $params['url.rules']
            ),

            'image' => array(
                'class' => 'common.extensions.image.CImageComponent',
                'application_dir' => 'common',
                // GD or ImageMagick
                'driver' => 'GD',
                // ImageMagick setup path
                'params' => array('directory' => '/opt/local/bin'),
            ),


            /**====================debug begin==========================*/
            'log' => array(
                'class' => 'CLogRouter',
                'routes' => array(
                    array(
                        'class' => 'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
                        // Access is restricted by default to the localhost
                        //'ipFilters'=>array('127.0.0.1','192.168.1.*', 88.23.23.0/24),
                    ),
                ),
            ),

            /*'log'=>array(
                'class'=>'CLogRouter',
                'routes'=>array(
                    array(
                        'class'=>'CWebLogRoute',
                        'levels'=>'error, warning, trace, profile, info',
                    ),
                ),
            ),*/
            /**====================debug begin==========================*/

            /* make sure you have your cache set correctly before uncommenting */
//             'cache' => $params['cache.core'],
//             'contentCache' => $params['cache.content']
        ),


    ),
    CMap::mergeArray($mainEnvConfiguration, $mainLocalConfiguration)
);