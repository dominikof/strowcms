<?php
/**
 * User: Panarin S.S.
 * Date: 06.01.13
 * Time: 12:03
 */
class AdminBehaviors extends CActiveRecordBehavior
{

    public $tableName;
    public $perPage;
    public $orderConditionDiapason=array();

    public function saveOrder($arr)
    {
        $keys = array_keys($arr);
        $connection = Yii::app()->db;
        $transaction = $connection->beginTransaction();
        $page = Yii::app()->request->getParam('page', 1);
        $coef = ($page-1) * $this->perPage;
        try {
            $q = "UPDATE \"" . $this->tableName . "\" SET \"order\"=:order WHERE \"id\"=:id";
            $command = Yii::app()->db->createCommand($q);
            foreach ($keys as $orderNew => $id) {
//                $orderOld = $arr[$id];
                $orderNew += $coef;
//                if ($orderOld != $orderNew) {
                    $command->bindParam(':order', $orderNew, PDO::PARAM_INT);
                    $command->bindParam(':id', $id, PDO::PARAM_INT);
                    $command->execute();
//                }
            }
            $transaction->commit();
        } catch (Exception $e) // в случае возникновения ошибки при выполнении одного из запросов выбрасывается исключение
        {
            $transaction->rollback();
        }

    }

    public function findMaxOrder($id_parent = NULL, $parent_fld = NULL)
    {
        $maxOrder = Yii::app()->db->createCommand()
            ->select('max("order") AS "maxOrder"')
            ->from($this->tableName);
        if (!empty($id_parent) AND !empty($parent_fld)) {
            $maxOrder->where('"' . $parent_fld . '"=:parent_id', array(':parent_id' => $id_parent));
        }
        $max = $maxOrder->queryScalar();
        return $max;
    }

    public function moveToOtherPage($pageToMove)
    {
        $checked = Yii::app()->request->getParam('check', array());
        $order = Yii::app()->request->getParam('order');
        $pageFrom=Yii::app()->request->getParam('pageFrom');
        $pageNew=Yii::app()->request->getParam('page');
        if (count($checked) > 0) {
            $coef = count($checked);
            if($pageNew>$pageFrom){
                $orderFrom = $order[$checked[0]];
                if(!isset($pageToMove[$coef-1])) return;
                $orderTo = $pageToMove[$coef-1]->order;
                $connection = Yii::app()->db;
                $transaction = $connection->beginTransaction();

                $q = 'UPDATE "'.$this->tableName.'" SET  "order"="order"-:coef WHERE';
                $q.=' "order">:orderFrom';
                $q.=' AND "order"<=:orderTo';

                $this->addOrderConditionDiapasonCriteria($q);
                $command = Yii::app()->db->createCommand($q);
                $command->execute(array(
                    ':coef'=>$coef,
                    ':orderFrom'=>$orderFrom,
                    ':orderTo'=>$orderTo,
                ));
            }else{
                $orderFrom =$pageToMove[0]->order;
                $orderTo =$order[$checked[$coef-1]];
                $notIn=implode(',',$checked);
                $connection = Yii::app()->db;
                $transaction = $connection->beginTransaction();

                $q = 'UPDATE "'.$this->tableName.'" SET  "order"="order"+:coef WHERE';
                $q.=' "order">=:orderFrom';
                $q.=' AND "order"<:orderTo';
                $q.=' AND "id" NOT IN ('.$notIn.')';

                $this->addOrderConditionDiapasonCriteria($q);
                $command = Yii::app()->db->createCommand($q);
                $command->execute(array(
                    ':coef'=>$coef,
                    ':orderFrom'=>$orderFrom,
                    ':orderTo'=>$orderTo,
                ));
            }
            $order=$pageToMove[0]->order;
            for ($i = 0; $i < $coef; $i++) {
                $q='UPDATE "'.$this->tableName.'" SET  "order"=:order WHERE "id"=:id';
                $command = Yii::app()->db->createCommand($q);
                $command->execute(array(
                    ':order'=>$order,
                    ':id'=>$checked[$i],
                ));
                $order++;
            }

            $transaction->commit();
        }
    }

    private function addOrderConditionDiapasonCriteria(&$q){
        foreach ($this->orderConditionDiapason as $key=>$cond) {
             if(isset($cond['value']) AND isset($cond['operator'])){
                 $q.=' AND "'.$key.'" '.$cond['operator'].' \''.$cond['value'].'\' ';
             }
        }
    }
}
