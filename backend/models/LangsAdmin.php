<?php
/**
 * User: Aki
 * Date: 04.11.12
 * Time: 23:11
 */
class LangsAdmin extends Langs
{

    public function behaviors()
    {
        return array(
            'AdminBehaviors'=>array(
                'class'=>'ext.behaviors.AdminBehaviors',
                'tableName'=>$this->tableName(),
            ),
        );
    }

    public function checkLangLeft()
    {

        if ($this->back_def  AND !$this->back) {
            $this->addErrors(array(Yii::t('langs', 'Если даный язык установлен как по-умолчанию на бекенде, то должно быть установлено, что даный язык исполюзуеться на бекенде!')));
            return false;
        }
        if ($this->front_def  AND !$this->front) {
            $this->addErrors(array(Yii::t('langs', 'Если даный язык установлен как по-умолчанию на фронтенде, то должно быть установлено, что даный язык исполюзуеться на фронтенде!')));
            return false;
        }

        $criteria = new CDbCriteria;
        $criteria->condition = "back=:dif";
        $criteria->params = array(":dif" => 'true');
        $res = Langs::model()->find($criteria);
        if (empty($res->id)) {
            $this->addErrors(array(Yii::t('langs', 'Должен остаться хотя бы один язык установленный для бекенда!')));
            return false;
        }
        $criteria->condition = "front=:dif";
        $criteria->params = array(":dif" => 'true');
        $res = Langs::model()->find($criteria);
        if (empty($res->id)) {
            $this->addErrors(array(Yii::t('langs', 'Должен остаться хотя бы один язык установленный для фронтенда!')));
            return false;
        }

        $criteria->condition = "back_def=:dif";
        $criteria->params = array(":dif" => 'true');
        $res = Langs::model()->find($criteria);
        if (empty($res->id)) {
            $this->addErrors(array(Yii::t('langs', 'Должен остаться хотя бы один язык установленный для бекенда по-умолчанию!')));
            return false;
        }
        $criteria->condition = "front_def=:dif";
        $criteria->params = array(":dif" => 'true');
        $res = Langs::model()->find($criteria);
        if (empty($res->id)) {
            $this->addErrors(array(Yii::t('langs', 'Должен остаться хотя бы один язык установленный для фронтенда по-умолчанию!')));
            return false;
        }

        return true;
    }

    public function findLangsForDelete($arr)
    {
        $result = Langs::model()->findAllByAttributes(
            array('id' => $arr)
        );
        return $result;
    }

    public function deleteImgInRecords($obj)
    {
            foreach ($obj as $item) {
                $img_path = $_SERVER['DOCUMENT_ROOT'] . "/media/images/uploads/langs/" . $item->img;
                if (is_file($img_path)) {
                    unlink($img_path);
                }
            }

    }


    public function rewriteDefLang()
    {
        $criteria = new CDbCriteria;
        if ($this->attributes['back_def']) {
            $criteria->condition = "back_def=:dif";
            $criteria->params = array(":dif" => 'true');
            $res = Langs::model()->find($criteria);
            if (isset($res->isNewRecord)) {
                $res->back_def = false;
                $res->save();
            }
        }
        if ($this->attributes['front_def']) {
            $criteria->condition = "front_def=:dif";
            $criteria->params = array(":dif" => 'true');
            $res = Langs::model()->find($criteria);
            if (isset($res->isNewRecord)) {
                $res->front_def = false;
                $res->save();
            }
        }
    }
}
