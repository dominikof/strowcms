<?php
/**
 * User: Panarin S.S.
 * Date: 19.12.12
 * Time: 12:57
 */
class TextsAdmin extends Sourcemessage
{

    public function getTextsByPages()
    {

        $page = Yii::app()->request->getParam('page', 1);
        $criteria = new CDbCriteria();
        $criteria->with = array(
            'messages',
        );
        $criteria->together = true;
        $dataProvider = new CActiveDataProvider('Sourcemessage', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 15,
                'currentPage' => $page - 1
            ),
        ));

        $this->addSearchCriteria($dataProvider);

        return $dataProvider;
    }

    private function addSearchCriteria(CActiveDataProvider &$activeDataProvider){
        $filter_text=Yii::app()->request->getParam('filter_text');
        if(!empty($filter_text))
            $activeDataProvider->criteria->compare('"message"',$filter_text,true);

        $filter_category=Yii::app()->request->getParam('filter_category');
        if(!empty($filter_category))
            $activeDataProvider->criteria->compare('"category"',$filter_category);

        $filter_translate=Yii::app()->request->getParam('filter_translate');
        if(!empty($filter_translate))
            $activeDataProvider->criteria->compare('"messages"."translation"',$filter_translate,true);
    }


    public function getTextsCategories()
    {
        $command = Yii::app()->db->createCommand();
        $command->select('category')
            ->from($this->tableName())
            ->group('category');
        return $command->queryAll();
    }

    public static function factory()
    {
        return new TextsAdmin();
    }
}
