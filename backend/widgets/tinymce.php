<?php
/**
 * User: Aki
 * Date: 12.11.12
 * Time: 23:09
 */
class tinymce extends CWidget
{

    private $pathToJqueryTinymce = "/admin/js/tinymce/jquery.tinymce.js";
    private $pathToTinymce = "/admin/js/tinymce/tiny_mce.js";
    public $defLang = 'en';
    public $lang = 'en';
    public $fld_name = 'tinymce';
    public $content = '';

    public function run(){
        $this->makeFld($this->fld_name,$this->lang);
    }

    private function makeFld($name, $lang)
    {
        Yii::app()->clientScript->registerScriptFile($this->pathToJqueryTinymce);
        $this->_render($name);
    }

    private function _render($name)
    {
        $symbols = '0123456789abcdefghijklmnopqrstuvwxyz';
        $id = md5(substr(str_shuffle($symbols), 0, 16) . microtime());
        CWidget::render('tinymce_fld',array('name'=>$name,'id'=>$id,'pathToTinymce'=>$this->pathToTinymce,'content'=>$this->content));
    }
}
