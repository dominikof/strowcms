<?php
/**
 * User: Panarin S.S.
 * Date: 07.01.13
 * Time: 9:27
 */
class dateTimeFld extends CWidget
{

    public $fld_name;
    public $label;
    public $fld_value;

    public function run(){
        $script="
            $('.time_picker').datetimepicker({
                addSliderAccess: true,
                dateFormat: 'yy-mm-dd',
                timeFormat:'hh:mm:ss',
                sliderAccessArgs: { touchonly: false }
            });
        ";
        Yii::app()->getClientScript()->registerScriptFile(CHtml::asset(__DIR__.'/js/timepicker/sliderAccess.js'), CClientScript::POS_END);
        Yii::app()->getClientScript()->registerScriptFile(CHtml::asset(__DIR__.'/js/timepicker/timepicker.js'), CClientScript::POS_END);
        Yii::app()->getClientScript()->registerScript('__', $script, CClientScript::POS_READY);
        CWidget::render('dateTimeFld',array('fld_name'=>$this->fld_name,'label'=>$this->label,'fld_value'=>$this->fld_value));
    }

}
