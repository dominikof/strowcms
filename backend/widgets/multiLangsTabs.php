<?php
/**
 * User: Panarin S.S.
 * Date: 18.11.12
 * Time: 15:03
 */
class multiLangsTabs extends CWidget
{

    public $view;
    public $modelTexts;


    public function run(){
        $model=Langs::model()->frontActiveLangs();
        CWidget::render('multiLangsTabs',array('view'=>$this->view,'model'=>$model,'modelTexts'=>$this->modelTexts));
    }
}
