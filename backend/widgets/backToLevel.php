<?php
/**
 * User: Panarin S.S.
 * Date: 10.12.12
 * Time: 11:35
 * back to top level widget
 */
class backToLevel extends CWidget
{
    public $id;
    public $name;
    public $url;

    public function run(){
        CWidget::render('backToLevel',array('id'=>$this->id,'name'=>$this->name,'url'=>$this->url));
    }
}
