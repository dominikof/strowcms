/**
 * User: Panarin S.S.
 * Date: 07.01.13
 * Time: 14:13
 */
(function(){
    var startDateTextBox = $('#timePickerBeginId');
    var endDateTextBox = $('#timePickerEndId');
    var dateFormat=date_format;
    var timeFormat=time_format;

    startDateTextBox.datetimepicker({
        addSliderAccess: true,
        dateFormat: dateFormat,
        timeFormat: timeFormat,
        sliderAccessArgs: { touchonly: false },
        onClose: function(dateText, inst) {
            if (endDateTextBox.val() != '') {
                var testStartDate = startDateTextBox.datetimepicker('getDate');
                var testEndDate = endDateTextBox.datetimepicker('getDate');
                if (testStartDate > testEndDate)
                    endDateTextBox.datetimepicker('setDate', testStartDate);
            }
            else {
                endDateTextBox.val(dateText);
            }
        },
        onSelect: function (selectedDateTime){
            endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
        }
    });
    endDateTextBox.datetimepicker({
        addSliderAccess: true,
        dateFormat: dateFormat,
        timeFormat: timeFormat,
        sliderAccessArgs: { touchonly: false },
        onClose: function(dateText, inst) {
            if (startDateTextBox.val() != '') {
                var testStartDate = startDateTextBox.datetimepicker('getDate');
                var testEndDate = endDateTextBox.datetimepicker('getDate');
                if (testStartDate > testEndDate)
                    startDateTextBox.datetimepicker('setDate', testEndDate);
            }
            else {
                startDateTextBox.val(dateText);
            }
        },
        onSelect: function (selectedDateTime){
            startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
        }
    });
})()