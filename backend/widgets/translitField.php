<?php
/**
 * User: Panarin S.S.
 * Date: 24.12.12
 * Time: 20:57
 */
class translitField extends CWidget
{

    public $translit='';
    public $lang_id='';

    public function run(){
        $script='var changeUrlTxt="'.Yii::t('main','Do you really want edit url?').'";';
        Yii::app()->getClientScript()->registerScript('_', $script, CClientScript::POS_HEAD);
        Yii::app()->getClientScript()->registerScriptFile(CHtml::asset(__DIR__.'/js/translit.js'), CClientScript::POS_END);
        CWidget::render('translitField',array('translit'=>$this->translit,'lang_id'=>$this->lang_id));
    }

}
