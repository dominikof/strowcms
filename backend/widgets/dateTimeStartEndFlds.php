<?php
/**
 * User: Panarin S.S.
 * Date: 07.01.13
 * Time: 9:27
 */
class dateTimeStartEndFlds extends CWidget
{

    public $fld_start_name;
    public $fld_end_name;
    public $label_start;
    public $label_end;
    public $fld_start_value;
    public $fld_end_value;

    public function run(){
        $date_arr=explode(' ',Yii::app()->params['dateTimeFormatTimePicker']);
        $script="
            var date_format='".$date_arr[0]."';
            var time_format='".$date_arr[1]."';
        ";
        Yii::app()->getClientScript()->registerScript('__', $script, CClientScript::POS_BEGIN);
        Yii::app()->getClientScript()->registerScriptFile(CHtml::asset(__DIR__.'/js/timepicker/sliderAccess.js'), CClientScript::POS_END);
        Yii::app()->getClientScript()->registerScriptFile(CHtml::asset(__DIR__.'/js/timepicker/timepicker.js'), CClientScript::POS_END);
        Yii::app()->getClientScript()->registerScriptFile(CHtml::asset(__DIR__.'/js/dateBeginEndFields.js'), CClientScript::POS_END);
//        Yii::app()->getClientScript()->registerScript('__', $script, CClientScript::POS_READY);
        if(empty($this->fld_start_value))
            $this->fld_start_value=date(Yii::app()->params['dateTimeFormat']);
        if(empty($this->fld_end_value)){
            $date=new DateTime();
            $date->modify('+1 month');
            $this->fld_end_value=$date->format(Yii::app()->params['dateTimeFormat']);
        }

        CWidget::render('dateTimeStartEndFlds',
            array(
                'fld_start_name'=>$this->fld_start_name,
                'fld_end_name'=>$this->fld_end_name,
                'label_start'=>$this->label_start,
                'label_end'=>$this->label_end,
                'fld_start_value'=>$this->fld_start_value,
                'fld_end_value'=>$this->fld_end_value,
            ));
    }

}
