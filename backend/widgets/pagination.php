<?php
/**
 * User: Panarin S.S.
 * Date: 07.12.12
 * Time: 12:48
 *
 * widget for pagination in admin
 */
class pagination extends CWidget
{
    public $count;
    public $perPage=10;
    public $url;

    public function run(){
        $page=Yii::app()->request->getParam('page',1);
        if($this->count>$this->perPage){
            $btnCount=ceil($this->count/$this->perPage);
            CWidget::render('pagination',array('btnCount'=>$btnCount,'page'=>$page,'count'=>$this->count,'url'=>$this->url,'perPage'=>$this->perPage));
        }
    }

}
