<?php
/**
 * User: Aki
 * Date: 12.11.12
 * Time: 23:45
 */
?>
<script type="text/javascript" charset="utf-8">
    $().ready(function () {
        var opts = {
            lang:'<?php echo $this->lang; ?>', // set your language
            styleWithCSS:false,
            height:400,
            toolbar:'maxi'
        };
        // create editor
        $('#editorFld<?php echo $id; ?>').elrte(opts);

        // or this way
        // var editor = new elRTE(document.getElementById('our-element'), opts);
    });
</script>
<textarea id="editorFld<?php echo $id; ?>" name="<?php echo $name; ?>"></textarea>