<?php
/**
 * User: Panarin S.S.
 * Date: 24.12.12
 * Time: 21:01
 */
?>

<div class="control-group">
    <label class="control-label">
        <?php echo Yii::t("main", "Url ");?> .../
    </label>
    <div class="controls">
        <?php echo CHtml::textField("translits[$lang_id][translit]",$this->translit,array('disabled'=>'disabled')); ?>
        <?php echo CHtml::button(Yii::t('main','Change url'),array('class'=>'btn translit-change'))?>
        <?php echo CHtml::hiddenField("translits[$lang_id][changeUrl]",0)?>
        <br/>
    </div>
    <div class="controls">
        <label class="checkbox">
        <?php echo CHtml::checkBox("translits[$lang_id][regenerate]")?>
        <?php echo Yii::t('main','Regenerate url from name')?>
        </label>
    </div>
</div>