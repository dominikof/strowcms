<?php
/**
 * User: Panargin S.S.
 * Date: 07.12.12
 * Time: 12:48
 */
?>

<div class="row-fluid">
    <div class="span12">
        <div class="dataTables_info" id="DataTables_Table_0_info">
            <?php echo Yii::t('main','Showing :fromR to :toR of :count entries',array(':fromR'=>($page-1)*$perPage+1,':toR'=>($page*$perPage>$count)?$count:$page*$perPage,':count'=>$count))?>
        </div>
    </div>
    <div class="span12 center">
        <div class="dataTables_paginate paging_bootstrap pagination">
            <ul>
                <?php
                $devider=MYCHtml::getQueryDeviderToParam($url);
                if($page!=1):
                    $urlPrev=(($page-1)==1)?$url:$url.$devider."page=".($page-1);
                ?>
                <li class="prev"><a href="<?php echo $urlPrev;?>"><?php echo Yii::t('main','Previous')?></a></li>
                <?php endif;?>
                <?php for ($i = 1; $i <= $btnCount; $i++) :
                    $urlO=($i==1)?$url:$url.$devider."page=".$i;
                ?>
                <li class="<?php echo ($page==$i)?"active":''?>">
                    <a id='paginationPageId<?php echo $i?>' data-page='<?php echo $i?>' class='<?php echo ($page==$i)?'':'item-dropable'?>' href="<?php echo $urlO;?>"><?php echo $i?></a>
                </li>
                <?php endfor; ?>
                <?php if($page!=$btnCount):
                $urlNext=$url.$devider."page=".($page+1);
                ?>
                <li class="next"><a href="<?php echo $urlNext;?>"><?php echo Yii::t('main','Next')?></a></li>
                <?php endif;?>
            </ul>
        </div>
    </div>
</div>