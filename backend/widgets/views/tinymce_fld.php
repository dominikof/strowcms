<?php
/**
 * User: Aki
 * Date: 12.11.12
 * Time: 23:45
 */
?>
<script type="text/javascript" charset="utf-8">
    $().ready(function () {
        $('#editorFld<?php echo $id;?>').tinymce({
            // Location of TinyMCE script
            script_url:'<?php echo $pathToTinymce;?>',

            // General options
            theme:"advanced",
            plugins:"pagebreak,style,layer,table,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

            // Theme options
            theme_advanced_buttons1:"code,save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
            theme_advanced_buttons2:"cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,|,insertdate,inserttime,preview,|,forecolor,backcolor",
            theme_advanced_buttons3:"tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
            theme_advanced_buttons4:"insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,imagemanager,filemanager",
            theme_advanced_toolbar_location:"top",
            theme_advanced_toolbar_align:"left",
            theme_advanced_statusbar_location:"bottom",
            theme_advanced_resizing:true,


            file_browser_callback:'elFinderBrowser',
            setup : function(ed) {
                // Display an alert onclick
                ed.onClick.add(function(ed) {
                    //ed.windowManager.alert('User clicked the editor.');
                });

                // Add a custom button
                ed.addButton('imagemanager', {
                    title : 'Image manager',
                    image : '/admin/js/tinymce/img/insertimage.gif',
                    onclick : function() {
                            elFinderBrowser('temp_elfinder_field','','image',window,'imageManager',ed);
                    }
                });
                ed.addButton('filemanager', {
                    title : 'File manager',
                    image : '/admin/js/tinymce/img/insertfile.gif',
                    onclick : function() {
                            elFinderBrowser('temp_elfinder_field','','image',window,'fileManager',ed);
                    }
                });
            }

        });
    });

    function elFinderBrowser (field_name, url, type, win,from,ed) {
        var elfinder_url = '/admin/js/elfinder/elfinder.html';    // use an absolute path!
        tinyMCE.activeEditor.windowManager.open({
            file: elfinder_url,
            title: 'elFinder 2.0',
            width: 900,
            height: 450,
            resizable: 'yes',
            inline: 'yes',    // This parameter only has an effect if you use the inlinepopups plugin!
            popup_css: false, // Disable TinyMCE's default popup CSS
            close_previous: 'no'
        }, {
            window: win,
            input: field_name,
            from: from,
            ed: ed
        });
        return false;
    }
</script>
<textarea id="editorFld<?php echo $id; ?>" name="<?php echo $name; ?>"><?php echo $content?></textarea>