<?php
/**
 * User: Panarin S.S.
 * Date: 16.12.12
 * Time: 21:41
 */
?>

<div class="show-btn-panel">
    <a href="<?php
    echo $this->addUrl;?>" title='<?php echo $this->addBtnText; ?>' class="btn btn-primary <?php echo ($this->modal)?'add-content':''; ?>"><i
            class='icon-plus-sign icon-white'></i> <?php echo $this->addBtnText; ?>
    </a>
    <a href="<?php echo $this->removeUrl;?>" oktext='<?php echo $this->msgRemoveOk ?>' notext='<?php echo $this->msgRemoveNo ?>'
       msgtext='<?php echo  $this->msgRemove; ?>' class="btn btn-danger del-btn"
       title="<?php echo $this->removeBtnText; ?>"><i class='icon-trash icon-white'></i> <?php echo $this->removeBtnText; ?>
    </a>
    <?php
    echo $this->htmlAfterRemoveBtn;
    ?>
</div>