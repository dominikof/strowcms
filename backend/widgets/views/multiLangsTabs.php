<?php
/**
 * User: Panarin S.S.
 * Date: 18.11.12
 * Time: 15:29
 */
$arr=array();

foreach ($model as $key=>$lang) {
    $img='';
    if(isset($lang['img']))
        $img='/media/images/uploads/langs/'.$lang['img'];
    $active=false;
    if($key==0) $active=true;
    $arr[]=array(
        'label'=>CHtml::image($img)." ".$lang['name'],
        'active'=>$active,
        'content'=>CWidget::render($this->view,array('lang'=>$lang->id,'short'=>$lang->short,'modelTexts'=>$this->modelTexts),true)
    );
}


$this->widget('bootstrap.widgets.TbTabs', array(
    'type'=>'tabs',
    'encodeLabel'=>false,
    'tabs'=>$arr,
));