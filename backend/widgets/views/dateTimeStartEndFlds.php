<div class="control-group">
    <label class="control-label">
        <?php echo $label_start;?>
    </label>
    <div class="controls">
        <?php echo CHtml::textField($fld_start_name,$fld_start_value,array('class'=>'time_picker ','id'=>'timePickerBeginId')) ?>
    </div>
</div>
<div class="control-group">
    <label class="control-label">
        <?php echo $label_end;?>
    </label>
    <div class="controls">
        <?php echo CHtml::textField($fld_end_name,$fld_end_value,array('class'=>'time_picker','id'=>'timePickerEndId')) ?>
    </div>
</div>