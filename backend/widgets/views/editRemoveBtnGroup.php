<a class="btn btn-success <?php echo ($this->modal)?'edit-content':''?>" rel="tooltip"
   href="<?php echo $this->editUrl;?>" title='<?php echo $this->editTitle;?>'><i
        class=' icon-pencil icon-white' rel="tooltip" ></i></a>
<a class="btn btn-danger del-btn" href="<?php
echo $this->delUrl ?>" oktext='<?php echo $this->delOkTxt ?>'
   notext='<?php echo $this->delNoTxt ?>'
   msgtext='<?php echo  $this->delQuestion; ?>'
   title="<?php echo $this->delTitle; ?>"
   rel="tooltip"  >
    <i class='icon-trash icon-white'></i>
</a>&nbsp