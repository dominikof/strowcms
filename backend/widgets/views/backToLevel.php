<?php
/**
 * User: Panarin S.S.
 * Date: 10.12.12
 * Time: 11:36
 */

?>

<?php if($id!=0):?>
<a href="<?php echo $url ?>?parent_id=<?php echo $id?>"
   title="<?php echo $name?>" rel="tooltip">
    <?php
    echo Yii::t('main', 'back to level ":level"',array(':level'=>$name))
    ?>
</a>
<?php else: ?>
<a href="<?php echo $url ?>"
   title="<?php echo Yii::t('main','Back to root level')?>" rel="tooltip">
    <?php echo Yii::t('main','Back to root level')?>
</a>
<?php endif; ?>