<?php
/**
 * User: Panargin S.S.
 * Date: 18.11.12
 * Time: 13:21
 */
?>
<div class="row-fluid">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="icon-edit"></i><span class="break"></span><?php echo $this->boxTitle?></h2>
        </div>
        <div class="box-content">
            <?php echo $this->boxContent?>
        </div>
    </div>
</div>
