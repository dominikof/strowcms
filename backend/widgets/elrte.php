<?php
/**
 * User: Aki
 * Date: 12.11.12
 * Time: 23:09
 */
class elrte extends CWidget
{

    var $pathToCss = "/admin/js/elrte/css/elrte.min.css";
    var $pathToElrteJs = "/admin/js/elrte/js/elrte.min.js";
    var $pathToLocalizations = "/admin/js/elrte/js/i18n/";
    var $pathToUICSS = "/admin/js/elrte/css/smoothness/jquery-ui-1.8.13.custom.css";
    var $defLang = 'en';
    var $lang = 'en';
    var $fld_name = 'elrte';

    public function run(){
        $this->makeFld($this->fld_name,$this->lang);
    }

    private function makeFld($name, $lang)
    {
        Yii::app()->clientScript->registerScriptFile($this->pathToElrteJs);
        Yii::app()->clientScript->registerCssFile($this->pathToCss);
//        Yii::app()->clientScript->registerCssFile($this->pathToUICSS);
        $pathLoLocalFile = $_SERVER['DOCUMENT_ROOT'] . $this->pathToLocalizations . 'elrte.' . $lang.".js";
        if (file_exists($pathLoLocalFile)){
            Yii::app()->clientScript->registerScriptFile($this->pathToLocalizations . 'elrte.' . $lang.".js");
            $this->lang=$lang;
        }else
            Yii::app()->clientScript->registerScriptFile($this->pathToLocalizations . 'elrte.' . $this->defLang.".js");
        $this->_render($name);
    }

    private function _render($name)
    {
        $symbols = '0123456789abcdefghijklmnopqrstuvwxyz';
        $id = md5(substr(str_shuffle($symbols), 0, 16) . microtime());
        CWidget::render('elrte_fld',array('name'=>$name,'id'=>$id));
    }
}
