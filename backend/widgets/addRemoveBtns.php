<?php
/**
 * User: Panarin S.S.
 * Date: 16.12.12
 * Time: 21:39
 *
 * button group panel to show and delete instance
 */
class addRemoveBtns extends CWidget
{

    public $addUrl='';
    public $removeUrl='';
    public $addBtnText='';
    public $removeBtnText='';

    public $msgRemoveOk='';
    public $msgRemoveNo='';
    public $msgRemove='';
    public $modal=false;

    public $htmlAfterRemoveBtn='';

    public function run(){
        if(empty($this->msgRemoveOk)){
            $this->msgRemoveOk = Yii::t("main", "Yes");
        }
        if(empty($this->msgRemoveNo)){
            $this->msgRemoveNo = Yii::t("main", "No");
        }
        if(empty($this->addBtnText)){
            $this->addBtnText = Yii::t("main", "Add");
        }
        if(empty($this->removeBtnText)){
            $this->removeBtnText = Yii::t("main", "Remove");
        }
        CWidget::render('addRemoveBtns');
    }

}
