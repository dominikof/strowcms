<?php
/**
 * User: Panargin S.S.
 * Date: 18.11.12
 * Time: 13:19
 */
class box extends CWidget
{

    public $boxTitle;
    public $boxContent;

    public function run(){
        CWidget::render('box',array('boxTitle'=>$this->boxTitle,'boxContent'=>$this->boxContent));
    }
}