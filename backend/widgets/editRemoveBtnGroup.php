<?php
/**
 * User: Panarin S.S.
 * Date: 20.01.13
 * Time: 11:08
 */
class editRemoveBtnGroup extends CWidget
{
    public $showEditBtn = true;
    public $showDelBtn = true;

    public $editUrl;
    public $delUrl;
    public $delOkTxt;
    public $delNoTxt;
    public $delQuestion;
    public $delTitle;
    public $editTitle;
    public $modal=false;


    public function run(){
        if(empty($this->delTitle)){
            $this->delTitle=Yii::t("main","Delete");
        }
        if(empty($this->delOkTxt)){
            $this->delOkTxt=Yii::t("main","Yes");
        }
        if(empty($this->editTitle)){
            $this->editTitle=Yii::t("main","Edit");
        }
        if(empty($this->delNoTxt)){
            $this->delNoTxt=Yii::t("main","No");
        }
        CWidget::render('editRemoveBtnGroup');
    }
}
